== BEGIN ==========================
Sat 05 Mar 2022 04:10:23 AM WET
 # note: 
 # tag: nasmg.m.autotiering
 # running nasmg (220305013155: Size m)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1536x1536x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         222.499 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.1746922003355E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           1536x1536x1280
 Iterations      =                       50
 Time in seconds =                  5965.69
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1468.01
 Mop/s/thread    =                    45.88
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              09 Feb 2022

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Sat Mar  5 04:10:23 2022


 Performance counter stats for 'system wide':

        151,976.97 Joules power/energy-ram/                                           
        883,578.46 Joules power/energy-pkg/                                           

    6192.617355574 seconds time elapsed

== DMESG ==========================
[57703.542758] oom_reaper: reaped process 35016 (bfs), now anon-rss:0kB, file-rss:0kB, shmem-rss:0kB
== END ============================
