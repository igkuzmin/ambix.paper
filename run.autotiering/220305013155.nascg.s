== BEGIN ==========================
Sat 05 Mar 2022 01:37:07 AM WET
 # note: 
 # tag: nascg.s.autotiering
 # running nascg (220305013155: Size s)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         171.184 seconds

   iteration           ||r||                 zeta
        1       0.21274464344931E-11   999.9996857982868
        2       0.13467133641236E-14    57.8675766650498
        3       0.13441923886632E-14    58.2941206350793
        4       0.13463944394759E-14    58.6783531949449
        5       0.13425923719989E-14    59.0239484906583
        6       0.13347874515157E-14    59.3343718346763
        7       0.13232132233012E-14    59.6128667120506
        8       0.13139066822096E-14    59.8624490951617
        9       0.13007944817252E-14    60.0859075092867
       10       0.12903100747816E-14    60.2858074951212
       11       0.12794691749373E-14    60.4644993187778
       12       0.12692215360163E-14    60.6241279736679
       13       0.12625140786194E-14    60.7666446959979
       14       0.12539672229399E-14    60.8938193721789
       15       0.12483870023476E-14    61.0072533517981
       16       0.12425080157844E-14    61.1083922942586
       17       0.12372661667769E-14    61.1985387720232
       18       0.12335154118248E-14    61.2788644311378
       19       0.12277143993188E-14    61.3504215716453
       20       0.12241554254976E-14    61.4141540599919
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                  3268.25
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   531.77
 Mop/s/thread    =                    16.62
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              09 Feb 2022

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Sat Mar  5 01:37:08 2022


 Performance counter stats for 'system wide':

         85,361.20 Joules power/energy-ram/                                           
        508,659.61 Joules power/energy-pkg/                                           

    3442.585437530 seconds time elapsed

== DMESG ==========================
[57703.542758] oom_reaper: reaped process 35016 (bfs), now anon-rss:0kB, file-rss:0kB, shmem-rss:0kB
== END ============================
