== BEGIN ==========================
Fri 04 Mar 2022 11:07:07 AM WET
 # note: 
 # tag: nascg.m.autotiering
 # running nascg (220304094711: Size m)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         153.618 seconds

   iteration           ||r||                 zeta
        1       0.21274504495772E-11   999.9996857982868
        2       0.13482641310938E-14    57.8675766650492
        3       0.13442260433202E-14    58.2941206350796
        4       0.13464639644777E-14    58.6783531949449
        5       0.13425703443070E-14    59.0239484906585
        6       0.13348132171865E-14    59.3343718346763
        7       0.13232107492065E-14    59.6128667120510
        8       0.13140147331867E-14    59.8624490951617
        9       0.13007634141664E-14    60.0859075092867
       10       0.12901712306055E-14    60.2858074951204
       11       0.12793941269372E-14    60.4644993187783
       12       0.12691499816720E-14    60.6241279736680
       13       0.12624878290357E-14    60.7666446959979
       14       0.12539483548705E-14    60.8938193721789
       15       0.12483451917857E-14    61.0072533517979
       16       0.12425221735786E-14    61.1083922942574
       17       0.12372251525924E-14    61.1985387720229
       18       0.12336224144878E-14    61.2788644311378
       19       0.12277192509671E-14    61.3504215716453
       20       0.12242379201332E-14    61.4141540599923
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                  2851.73
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   609.44
 Mop/s/thread    =                    19.05
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              09 Feb 2022

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Fri Mar  4 11:07:07 2022


 Performance counter stats for 'system wide':

         75,438.64 Joules power/energy-ram/                                           
        447,207.32 Joules power/energy-pkg/                                           

    3008.443364427 seconds time elapsed

== DMESG ==========================
[   57.795450] Policy zone: Normal
== END ============================
