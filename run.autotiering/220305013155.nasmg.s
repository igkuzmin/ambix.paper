== BEGIN ==========================
Sat 05 Mar 2022 02:36:00 AM WET
 # note: 
 # tag: nasmg.s.autotiering
 # running nasmg (220305013155: Size s)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1024x1024x1024  (class D)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:          34.066 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 VERIFICATION FAILED
 L2 Norm is              0.1590005169528E-09
 The correct L2 Norm is  0.1583275060440E-09


 MG Benchmark Completed.
 Class           =                        D
 Size            =           1024x1024x1024
 Iterations      =                       50
 Time in seconds =                   167.93
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                 18542.75
 Mop/s/thread    =                   579.46
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              09 Feb 2022

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Sat Mar  5 02:36:00 2022


 Performance counter stats for 'system wide':

          8,061.74 Joules power/energy-ram/                                           
         33,376.06 Joules power/energy-pkg/                                           

     203.727698895 seconds time elapsed

== DMESG ==========================
[57703.542758] oom_reaper: reaped process 35016 (bfs), now anon-rss:0kB, file-rss:0kB, shmem-rss:0kB
== END ============================
