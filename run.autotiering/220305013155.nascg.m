== BEGIN ==========================
Sat 05 Mar 2022 02:52:54 AM WET
 # note: 
 # tag: nascg.m.autotiering
 # running nascg (220305013155: Size m)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         187.577 seconds

   iteration           ||r||                 zeta
        1       0.21274493864208E-11   999.9996857982868
        2       0.13449939206057E-14    57.8675766650500
        3       0.13441882129259E-14    58.2941206350788
        4       0.13464297633573E-14    58.6783531949446
        5       0.13425683983843E-14    59.0239484906583
        6       0.13348235669070E-14    59.3343718346763
        7       0.13233041153135E-14    59.6128667120510
        8       0.13140337742335E-14    59.8624490951615
        9       0.13007819036497E-14    60.0859075092861
       10       0.12903004464353E-14    60.2858074951207
       11       0.12793857234278E-14    60.4644993187784
       12       0.12692029190307E-14    60.6241279736677
       13       0.12625184965590E-14    60.7666446959977
       14       0.12539979277664E-14    60.8938193721789
       15       0.12484238199535E-14    61.0072533517979
       16       0.12425122458740E-14    61.1083922942584
       17       0.12370620728723E-14    61.1985387720231
       18       0.12334758477468E-14    61.2788644311378
       19       0.12276766096629E-14    61.3504215716461
       20       0.12241890115555E-14    61.4141540599923
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                  3195.00
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   543.96
 Mop/s/thread    =                    17.00
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              09 Feb 2022

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Sat Mar  5 02:52:54 2022


 Performance counter stats for 'system wide':

         84,240.05 Joules power/energy-ram/                                           
        502,310.04 Joules power/energy-pkg/                                           

    3385.556284833 seconds time elapsed

== DMESG ==========================
[57703.542758] oom_reaper: reaped process 35016 (bfs), now anon-rss:0kB, file-rss:0kB, shmem-rss:0kB
== END ============================
