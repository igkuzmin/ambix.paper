== BEGIN ==========================
Fri 04 Mar 2022 03:21:15 PM WET
 # note: 
 # tag: nascg.l.autotiering
 # running nascg (220304094711: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     9000000
 Iterations:                      5
 Number of available threads:    32

bash: Killed
===================================
# started on Fri Mar  4 15:21:15 2022


 Performance counter stats for 'system wide':

         11,355.48 Joules power/energy-ram/                                           
         72,242.27 Joules power/energy-pkg/                                           

     447.795536586 seconds time elapsed

== DMESG ==========================
[   57.795450] Policy zone: Normal
[21507.577790] cg.l.x invoked oom-killer: gfp_mask=0x400dc0(GFP_KERNEL_ACCOUNT|__GFP_ZERO), order=0, oom_score_adj=0
[21507.577800] CPU: 7 PID: 44502 Comm: cg.l.x Tainted: G            E     5.9.6-vanila #1
[21507.577801] Hardware name: Supermicro SYS-6019P-WT/X11DDW-L, BIOS 3.2 10/16/2019
[21507.577804] Call Trace:
[21507.577816]  dump_stack+0x6d/0x88
[21507.577823]  dump_header+0x4a/0x1d8
[21507.577827]  oom_kill_process.cold.36+0xb/0x10
[21507.577842]  out_of_memory+0x1a8/0x4d0
[21507.577849]  __alloc_pages_slowpath.constprop.110+0xbd7/0xcc0
[21507.577852]  __alloc_pages_nodemask+0x2de/0x310
[21507.577856]  pte_alloc_one+0x13/0x40
[21507.577870]  __pte_alloc+0x16/0x100
[21507.577872]  handle_mm_fault+0x1524/0x1640
[21507.577877]  exc_page_fault+0x290/0x550
[21507.577882]  ? asm_exc_page_fault+0x8/0x30
[21507.577884]  asm_exc_page_fault+0x1e/0x30
[21507.577890] RIP: 0033:0x7f3e21fae6ff
[21507.577893] Code: 17 e0 c5 f8 77 c3 48 3b 15 96 3d 06 00 0f 83 25 01 00 00 48 39 f7 72 0f 74 12 4c 8d 0c 16 4c 39 cf 0f 82 c5 01 00 00 48 89 d1 <f3> a4 c3 80 fa 10 73 17 80 fa 08 73 27 80 fa 04 73 33 80 fa 01 77
[21507.577894] RSP: 002b:00007f1b4e8ab368 EFLAGS: 00010287
[21507.577895] RAX: 00007f256b5ff300 RBX: 000000014b708de1 RCX: 00000000000009b0
[21507.577896] RDX: 00000000000016b0 RSI: 00007f31922eb560 RDI: 00007f256b600000
[21507.577897] RBP: 000000013f2bc135 R08: 000000014b708b0a R09: 000000013f2bbe5e
[21507.577898] R10: 00007f2736aa5010 R11: 00007f3d7356a968 R12: 00000000007d83d0
[21507.577901] R13: 000000013f2bbe5f R14: 0000000000044aa2 R15: 00007f32c5adc010
[21507.577905] Mem-Info:
[21507.577961] active_anon:426 inactive_anon:22731911 isolated_anon:0
                active_file:7520 inactive_file:5752 isolated_file:0
                unevictable:0 dirty:1 writeback:0
                slab_reclaimable:10967 slab_unreclaimable:36894
                mapped:11978 shmem:10727 pagetables:44547 bounce:0
                free:109701830 free_pcp:1606 free_cma:0
[21507.577964] Node 0 active_anon:1600kB inactive_anon:15100788kB active_file:4136kB inactive_file:8kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:9236kB dirty:0kB writeback:0kB shmem:9408kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 4096kB writeback_tmp:0kB kernel_stack:6528kB all_unreclaimable? no
[21507.577967] Node 1 active_anon:96kB inactive_anon:15848232kB active_file:992kB inactive_file:20kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:420kB dirty:0kB writeback:0kB shmem:724kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 0kB writeback_tmp:0kB kernel_stack:4864kB all_unreclaimable? no
[21507.577969] Node 2 active_anon:0kB inactive_anon:30174152kB active_file:18496kB inactive_file:16624kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:31540kB dirty:4kB writeback:0kB shmem:32776kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 0kB writeback_tmp:0kB kernel_stack:0kB all_unreclaimable? no
[21507.577972] Node 3 active_anon:8kB inactive_anon:29804472kB active_file:6456kB inactive_file:6356kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:6716kB dirty:0kB writeback:0kB shmem:0kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 0kB writeback_tmp:0kB kernel_stack:0kB all_unreclaimable? no
[21507.577973] Node 0 DMA free:15888kB min:0kB low:12kB high:24kB reserved_highatomic:0KB active_anon:0kB inactive_anon:0kB active_file:0kB inactive_file:0kB unevictable:0kB writepending:0kB present:15972kB managed:15888kB mlocked:0kB pagetables:0kB bounce:0kB free_pcp:0kB local_pcp:0kB free_cma:0kB
[21507.577977] lowmem_reserve[]: 0 1621 31826 31826 31826
[21507.577979] Node 0 DMA32 free:119268kB min:88kB low:1748kB high:3408kB reserved_highatomic:0KB active_anon:0kB inactive_anon:1539808kB active_file:264kB inactive_file:4kB unevictable:0kB writepending:0kB present:1726208kB managed:1660672kB mlocked:0kB pagetables:232kB bounce:0kB free_pcp:0kB local_pcp:0kB free_cma:0kB
[21507.577982] lowmem_reserve[]: 0 0 30205 30205 30205
[21507.578003] Node 0 Normal free:10364kB min:11936kB low:42864kB high:73792kB reserved_highatomic:2048KB active_anon:1600kB inactive_anon:13560980kB active_file:3872kB inactive_file:4kB unevictable:0kB writepending:0kB present:31457280kB managed:30929996kB mlocked:0kB pagetables:87940kB bounce:0kB free_pcp:0kB local_pcp:0kB free_cma:0kB
[21507.578007] lowmem_reserve[]: 0 0 0 0 0
[21507.578010] Node 1 Normal free:12784kB min:10000kB low:42996kB high:75992kB reserved_highatomic:2048KB active_anon:96kB inactive_anon:15848232kB active_file:992kB inactive_file:20kB unevictable:0kB writepending:0kB present:33554432kB managed:32999412kB mlocked:0kB pagetables:90016kB bounce:0kB free_pcp:984kB local_pcp:0kB free_cma:0kB
[21507.578013] lowmem_reserve[]: 0 0 0 0 0
[21507.578015] Node 2 Movable free:219129288kB min:14380kB low:276524kB high:538668kB reserved_highatomic:0KB active_anon:0kB inactive_anon:30174152kB active_file:18496kB inactive_file:16624kB unevictable:0kB writepending:4kB present:262144000kB managed:262144000kB mlocked:0kB pagetables:0kB bounce:0kB free_pcp:3448kB local_pcp:0kB free_cma:0kB
[21507.578018] lowmem_reserve[]: 0 0 0 0 0
[21507.578020] Node 3 Movable free:219520452kB min:14380kB low:276524kB high:538668kB reserved_highatomic:0KB active_anon:8kB inactive_anon:29804472kB active_file:6456kB inactive_file:6356kB unevictable:0kB writepending:0kB present:262144000kB managed:262144000kB mlocked:0kB pagetables:0kB bounce:0kB free_pcp:1836kB local_pcp:0kB free_cma:0kB
[21507.578022] lowmem_reserve[]: 0 0 0 0 0
[21507.578024] Node 0 DMA: 0*4kB 0*8kB 1*16kB (U) 0*32kB 2*64kB (U) 1*128kB (U) 1*256kB (U) 0*512kB 1*1024kB (U) 1*2048kB (M) 3*4096kB (M) = 15888kB
[21507.578029] Node 0 DMA32: 58*4kB (UME) 65*8kB (UME) 52*16kB (UME) 50*32kB (UME) 40*64kB (UME) 29*128kB (UE) 2*256kB (UM) 1*512kB (E) 5*1024kB (UME) 3*2048kB (UME) 24*4096kB (UM) = 120048kB
[21507.578035] Node 0 Normal: 664*4kB (UMEH) 306*8kB (UMEH) 151*16kB (UMEH) 107*32kB (UME) 25*64kB (UMEH) 3*128kB (UH) 0*256kB 1*512kB (H) 0*1024kB 0*2048kB 0*4096kB = 13440kB
[21507.578541] Node 1 Normal: 714*4kB (UMEH) 373*8kB (UMEH) 268*16kB (UMEH) 59*32kB (UMEH) 29*64kB (UMH) 1*128kB (U) 0*256kB 1*512kB (U) 0*1024kB 0*2048kB 0*4096kB = 14512kB
[21507.578548] Node 2 Movable: 89*4kB (M) 15*8kB (M) 5*16kB (M) 2*32kB (M) 3*64kB (M) 1*128kB (M) 1*256kB (M) 1*512kB (M) 0*1024kB 1*2048kB (M) 53497*4096kB (M) = 219127468kB
[21507.578556] Node 3 Movable: 1*4kB (M) 1*8kB (M) 7*16kB (M) 9*32kB (M) 6*64kB (M) 5*128kB (M) 0*256kB 1*512kB (M) 1*1024kB (M) 0*2048kB 53593*4096kB (M) = 219519900kB
[21507.578616] Node 0 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[21507.578619] Node 0 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[21507.578620] Node 1 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[21507.578620] Node 1 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[21507.578621] Node 2 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[21507.578622] Node 2 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[21507.578623] Node 3 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[21507.578624] Node 3 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[21507.578624] 23971 total pagecache pages
[21507.578628] 0 pages in swap cache
[21507.578629] Swap cache stats: add 0, delete 0, find 0/0
[21507.578629] Free swap  = 0kB
[21507.578630] Total swap = 0kB
[21507.578630] 147760473 pages RAM
[21507.578631] 0 pages HighMem/MovableOnly
[21507.578631] 286981 pages reserved
[21507.578632] 0 pages hwpoisoned
[21507.578632] Tasks state (memory values in pages):
[21507.578633] [  pid  ]   uid  tgid total_vm      rss pgtables_bytes swapents oom_score_adj name
[21507.578766] [    677]     0   677    21848    13494   221184        0             0 systemd-journal
[21507.578774] [    715]     0   715     6194      862    81920        0         -1000 systemd-udevd
[21507.578783] [   1371]   101  1371    23272      415    81920        0             0 systemd-timesyn
[21507.578785] [   1372]   108  1372     1707      108    53248        0             0 rpcbind
[21507.578789] [   1395]     0  1395    20624      394    57344        0             0 irqbalance
[21507.578790] [   1396]     0  1396     1741      108    49152        0             0 ndctl
[21507.578793] [   1397]     0  1397    56472      651    86016        0             0 rsyslogd
[21507.578796] [   1398]     0  1398     4866      243    77824        0             0 systemd-logind
[21507.578800] [   1401]   104  1401     2259      450    53248        0          -900 dbus-daemon
[21507.578804] [   1417]     0  1417     2373      735    57344        0             0 dhclient
[21507.578807] [   1422]     0  1422    27611     1936   114688        0             0 unattended-upgr
[21507.578809] [   1428]     0  1428     1419       31    49152        0             0 agetty
[21507.578810] [   1429]     0  1429   667431     7489   495616        0          -999 containerd
[21507.578812] [   1468]     0  1468     3979      469    69632        0         -1000 sshd
[21507.578817] [   1577]   107  1577    96757      846   106496        0         -1000 nslcd
[21507.578818] [   1585]     0  1585     2142      215    61440        0             0 cron
[21507.578823] [   1862]   106  1862     5095      361    81920        0             0 exim4
[21507.578825] [   1924]  1234  1924     5290      352    73728        0             0 systemd
[21507.578827] [   1925]  1234  1925     5933      693    77824        0             0 (sd-pam)
[21507.578834] [   1989]  1234  1989     3049      806    69632        0             0 tmux: server
[21507.578836] [   2093]  1234  2093     2038      481    53248        0             0 bash
[21507.578838] [   3013]     0  3013     2574      150    57344        0             0 sudo
[21507.578840] [   3037]     0  3037     1430       30    49152        0             0 dmesg
[21507.578841] [   3415]  1234  3415     2007      449    57344        0             0 bash
[21507.578845] [   4635]  1234  4635     1414      101    53248        0             0 make
[21507.578846] [   4642]  1234  4642     1414       61    53248        0             0 make
[21507.578848] [   4723]  1234  4723     2007      466    57344        0             0 bash
[21507.578850] [   4797]  1234  4797     1414       72    45056        0             0 make
[21507.578851] [   4801]  1234  4801      598       17    40960        0             0 sh
[21507.578853] [   4802]  1234  4802     1562      474    57344        0             0 watch
[21507.578855] [  44437]  1234 44437     1414      510    45056        0             0 make
[21507.578856] [  44470]  1234 44470      598      182    40960        0             0 sh
[21507.578858] [  44471]  1234 44471     7173     3498    98304        0             0 perf
[21507.578859] [  44472]  1234 44472     1317      179    49152        0             0 tee
[21507.578861] [  44473]  1234 44473 36525624 22705984 182652928        0             0 cg.l.x
[21507.578863] oom-kill:constraint=CONSTRAINT_NONE,nodemask=(null),cpuset=/,mems_allowed=0-3,global_oom,task_memcg=/user.slice/user-1234.slice/session-1.scope,task=cg.l.x,pid=44473,uid=1234
[21507.579046] Out of memory: Killed process 44473 (cg.l.x) total-vm:146102496kB, anon-rss:90821084kB, file-rss:3424kB, shmem-rss:0kB, UID:1234 pgtables:178372kB oom_score_adj:0
[21511.053788] oom_reaper: reaped process 44473 (cg.l.x), now anon-rss:0kB, file-rss:0kB, shmem-rss:0kB
== END ============================
