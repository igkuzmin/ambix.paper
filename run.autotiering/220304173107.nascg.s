== BEGIN ==========================
Fri 04 Mar 2022 05:36:28 PM WET
 # note: 
 # tag: nascg.s.autotiering
 # running nascg (220304173107: Size s)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         167.390 seconds

   iteration           ||r||                 zeta
        1       0.21274497050781E-11   999.9996857982868
        2       0.13466746128763E-14    57.8675766650500
        3       0.13442694341536E-14    58.2941206350798
        4       0.13464408117019E-14    58.6783531949446
        5       0.13425918696573E-14    59.0239484906585
        6       0.13347441047106E-14    59.3343718346762
        7       0.13232514368165E-14    59.6128667120510
        8       0.13140762521729E-14    59.8624490951615
        9       0.13008437423297E-14    60.0859075092866
       10       0.12902528718980E-14    60.2858074951204
       11       0.12794574162113E-14    60.4644993187783
       12       0.12692291871814E-14    60.6241279736679
       13       0.12625439273432E-14    60.7666446959973
       14       0.12539371056070E-14    60.8938193721784
       15       0.12483906687382E-14    61.0072533517978
       16       0.12426234245404E-14    61.1083922942578
       17       0.12371521098928E-14    61.1985387720231
       18       0.12335703801769E-14    61.2788644311374
       19       0.12276893721087E-14    61.3504215716461
       20       0.12242673884429E-14    61.4141540599919
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                  3232.94
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   537.58
 Mop/s/thread    =                    16.80
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              09 Feb 2022

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Fri Mar  4 17:36:28 2022


 Performance counter stats for 'system wide':

         84,608.06 Joules power/energy-ram/                                           
        501,851.87 Joules power/energy-pkg/                                           

    3403.474170110 seconds time elapsed

== DMESG ==========================
[28855.302739] oom_reaper: reaped process 50593 (bfs), now anon-rss:0kB, file-rss:0kB, shmem-rss:0kB
== END ============================
