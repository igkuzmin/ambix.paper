== BEGIN ==========================
Fri 04 Mar 2022 06:51:11 PM WET
 # note: 
 # tag: nascg.m.autotiering
 # running nascg (220304173107: Size m)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         171.355 seconds

   iteration           ||r||                 zeta
        1       0.21274733695949E-11   999.9996857982868
        2       0.13466878152228E-14    57.8675766650493
        3       0.13442063324709E-14    58.2941206350796
        4       0.13463083085298E-14    58.6783531949446
        5       0.13424303237014E-14    59.0239484906585
        6       0.13347740312857E-14    59.3343718346765
        7       0.13233175030748E-14    59.6128667120508
        8       0.13140158558871E-14    59.8624490951619
        9       0.13008157303197E-14    60.0859075092866
       10       0.12903618730270E-14    60.2858074951207
       11       0.12793082493607E-14    60.4644993187777
       12       0.12693172585060E-14    60.6241279736679
       13       0.12625089809776E-14    60.7666446959979
       14       0.12539288338185E-14    60.8938193721789
       15       0.12483609088700E-14    61.0072533517981
       16       0.12426460060112E-14    61.1083922942578
       17       0.12372702057722E-14    61.1985387720229
       18       0.12335067392369E-14    61.2788644311370
       19       0.12276574069483E-14    61.3504215716461
       20       0.12243505217996E-14    61.4141540599925
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                  3245.79
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   535.45
 Mop/s/thread    =                    16.73
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              09 Feb 2022

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Fri Mar  4 18:51:12 2022


 Performance counter stats for 'system wide':

         85,127.11 Joules power/energy-ram/                                           
        507,515.10 Joules power/energy-pkg/                                           

    3420.161499289 seconds time elapsed

== DMESG ==========================
[28855.302739] oom_reaper: reaped process 50593 (bfs), now anon-rss:0kB, file-rss:0kB, shmem-rss:0kB
== END ============================
