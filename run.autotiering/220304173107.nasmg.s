== BEGIN ==========================
Fri 04 Mar 2022 06:34:33 PM WET
 # note: 
 # tag: nasmg.s.autotiering
 # running nasmg (220304173107: Size s)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1024x1024x1024  (class D)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:          30.468 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 VERIFICATION FAILED
 L2 Norm is              0.1590005169528E-09
 The correct L2 Norm is  0.1583275060440E-09


 MG Benchmark Completed.
 Class           =                        D
 Size            =           1024x1024x1024
 Iterations      =                       50
 Time in seconds =                   163.75
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                 19016.16
 Mop/s/thread    =                   594.25
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              09 Feb 2022

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Fri Mar  4 18:34:33 2022


 Performance counter stats for 'system wide':

          7,849.16 Joules power/energy-ram/                                           
         32,152.27 Joules power/energy-pkg/                                           

     194.245921144 seconds time elapsed

== DMESG ==========================
[28855.302739] oom_reaper: reaped process 50593 (bfs), now anon-rss:0kB, file-rss:0kB, shmem-rss:0kB
== END ============================
