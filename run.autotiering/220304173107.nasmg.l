== BEGIN ==========================
Sat 05 Mar 2022 01:03:53 AM WET
 # note: 
 # tag: nasmg.l.autotiering
 # running nasmg (220304173107: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 2048x2048x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

bash: Killed
===================================
# started on Sat Mar  5 01:03:53 2022


 Performance counter stats for 'system wide':

          2,816.02 Joules power/energy-ram/                                           
         17,766.89 Joules power/energy-pkg/                                           

     117.841916393 seconds time elapsed

== DMESG ==========================
[50325.340602] oom_reaper: reaped process 28485 (cg.l.x), now anon-rss:0kB, file-rss:0kB, shmem-rss:0kB
[56139.903357] mg.E.x invoked oom-killer: gfp_mask=0x400dc0(GFP_KERNEL_ACCOUNT|__GFP_ZERO), order=0, oom_score_adj=0
[56139.903361] CPU: 6 PID: 34772 Comm: mg.E.x Tainted: G            E     5.9.6-vanila #1
[56139.903362] Hardware name: Supermicro SYS-6019P-WT/X11DDW-L, BIOS 3.2 10/16/2019
[56139.903363] Call Trace:
[56139.903381]  dump_stack+0x6d/0x88
[56139.903385]  dump_header+0x4a/0x1d8
[56139.903387]  oom_kill_process.cold.36+0xb/0x10
[56139.903392]  out_of_memory+0x1a8/0x4d0
[56139.903395]  __alloc_pages_slowpath.constprop.110+0xbd7/0xcc0
[56139.903398]  __alloc_pages_nodemask+0x2de/0x310
[56139.903405]  pte_alloc_one+0x13/0x40
[56139.903409]  __pte_alloc+0x16/0x100
[56139.903412]  handle_mm_fault+0x1524/0x1640
[56139.903416]  exc_page_fault+0x290/0x550
[56139.903425]  ? asm_exc_page_fault+0x8/0x30
[56139.903427]  asm_exc_page_fault+0x1e/0x30
[56139.903430] RIP: 0033:0x55c791606bb8
[56139.903432] Code: 66 0f 59 c7 66 0f 58 d4 66 0f 58 cb 66 0f 59 d5 66 44 0f 5c c0 66 0f 59 ce 66 41 0f 28 c0 66 0f 5c c1 66 0f 5c c2 66 0f 28 d4 <42> 0f 11 04 0e 49 83 c1 10 4d 39 d9 75 a2 8b bc 24 20 01 00 00 39
[56139.903433] RSP: 002b:00007f21c5415890 EFLAGS: 00010206
[56139.903436] RAX: 00000000ad779b11 RBX: 00007f21c54199f0 RCX: 00000000ad779b11
[56139.903437] RDX: 00000000ad779b11 RSI: 00007f2738ffc8a8 RDI: 00007f499e2d78a8
[56139.903438] RBP: 000055c7916152c0 R08: 00007f39922a68a8 R09: 0000000000003750
[56139.903438] R10: 00007f21c5419130 R11: 0000000000004000 R12: 0000000000000802
[56139.903439] R13: 0000000000000800 R14: 0000000000000802 R15: 00007f443270a010
[56139.903441] Mem-Info:
[56139.903483] active_anon:431 inactive_anon:24323526 isolated_anon:0
                active_file:8425 inactive_file:5797 isolated_file:0
                unevictable:0 dirty:2 writeback:0
                slab_reclaimable:14602 slab_unreclaimable:37349
                mapped:24481 shmem:20967 pagetables:48052 bounce:0
                free:108102362 free_pcp:4913 free_cma:0
[56139.903486] Node 0 active_anon:1600kB inactive_anon:15087668kB active_file:1296kB inactive_file:392kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:10732kB dirty:0kB writeback:0kB shmem:11968kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 4096kB writeback_tmp:0kB kernel_stack:6688kB all_unreclaimable? yes
[56139.903489] Node 1 active_anon:96kB inactive_anon:15844336kB active_file:0kB inactive_file:464kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:4kB dirty:0kB writeback:0kB shmem:724kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 0kB writeback_tmp:0kB kernel_stack:4960kB all_unreclaimable? yes
[56139.903491] Node 2 active_anon:20kB inactive_anon:26273208kB active_file:22116kB inactive_file:17336kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:78568kB dirty:8kB writeback:0kB shmem:71176kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 0kB writeback_tmp:0kB kernel_stack:0kB all_unreclaimable? no
[56139.903494] Node 3 active_anon:8kB inactive_anon:40088892kB active_file:10388kB inactive_file:4996kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:8620kB dirty:0kB writeback:0kB shmem:0kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 0kB writeback_tmp:0kB kernel_stack:0kB all_unreclaimable? no
[56139.903500] Node 0 DMA free:15888kB min:0kB low:12kB high:24kB reserved_highatomic:0KB active_anon:0kB inactive_anon:0kB active_file:0kB inactive_file:0kB unevictable:0kB writepending:0kB present:15972kB managed:15888kB mlocked:0kB pagetables:0kB bounce:0kB free_pcp:0kB local_pcp:0kB free_cma:0kB
[56139.903504] lowmem_reserve[]: 0 1621 31826 31826 31826
[56139.903508] Node 0 DMA32 free:119676kB min:88kB low:1748kB high:3408kB reserved_highatomic:0KB active_anon:0kB inactive_anon:1535180kB active_file:0kB inactive_file:0kB unevictable:0kB writepending:0kB present:1726208kB managed:1660672kB mlocked:0kB pagetables:2508kB bounce:0kB free_pcp:7816kB local_pcp:244kB free_cma:0kB
[56139.903511] lowmem_reserve[]: 0 0 30205 30205 30205
[56139.903513] Node 0 Normal free:7516kB min:7840kB low:38768kB high:69696kB reserved_highatomic:0KB active_anon:1600kB inactive_anon:13552488kB active_file:1296kB inactive_file:372kB unevictable:0kB writepending:0kB present:31457280kB managed:30929996kB mlocked:0kB pagetables:97544kB bounce:0kB free_pcp:3196kB local_pcp:244kB free_cma:0kB
[56139.903516] lowmem_reserve[]: 0 0 0 0 0
[56139.903525] Node 1 Normal free:5356kB min:10000kB low:42996kB high:75992kB reserved_highatomic:0KB active_anon:96kB inactive_anon:15844336kB active_file:0kB inactive_file:464kB unevictable:0kB writepending:0kB present:33554432kB managed:32999412kB mlocked:0kB pagetables:92156kB bounce:0kB free_pcp:4164kB local_pcp:0kB free_cma:0kB
[56139.903528] lowmem_reserve[]: 0 0 0 0 0
[56139.903530] Node 2 Movable free:223026960kB min:14380kB low:276524kB high:538668kB reserved_highatomic:0KB active_anon:20kB inactive_anon:26273208kB active_file:22116kB inactive_file:17336kB unevictable:0kB writepending:8kB present:262144000kB managed:262144000kB mlocked:0kB pagetables:0kB bounce:0kB free_pcp:2164kB local_pcp:244kB free_cma:0kB
[56139.903533] lowmem_reserve[]: 0 0 0 0 0
[56139.903535] Node 3 Movable free:209233548kB min:14380kB low:276524kB high:538668kB reserved_highatomic:0KB active_anon:8kB inactive_anon:40088892kB active_file:10388kB inactive_file:4996kB unevictable:0kB writepending:0kB present:262144000kB managed:262144000kB mlocked:0kB pagetables:0kB bounce:0kB free_pcp:2492kB local_pcp:0kB free_cma:0kB
[56139.903538] lowmem_reserve[]: 0 0 0 0 0
[56139.903539] Node 0 DMA: 0*4kB 0*8kB 1*16kB (U) 0*32kB 2*64kB (U) 1*128kB (U) 1*256kB (U) 0*512kB 1*1024kB (U) 1*2048kB (M) 3*4096kB (M) = 15888kB
[56139.903545] Node 0 DMA32: 235*4kB (UME) 53*8kB (UME) 36*16kB (ME) 26*32kB (UME) 20*64kB (ME) 8*128kB (UME) 2*256kB (E) 3*512kB (UME) 2*1024kB (UE) 1*2048kB (U) 25*4096kB (UM) = 113620kB
[56139.903551] Node 0 Normal: 118*4kB (ME) 46*8kB (UME) 153*16kB (UME) 106*32kB (UME) 5*64kB (U) 0*128kB 0*256kB 0*512kB 0*1024kB 0*2048kB 0*4096kB = 7000kB
[56139.903556] Node 1 Normal: 393*4kB (UME) 93*8kB (UME) 54*16kB (UME) 4*32kB (UM) 0*64kB 0*128kB 0*256kB 0*512kB 0*1024kB 0*2048kB 0*4096kB = 3308kB
[56139.903561] Node 2 Movable: 1*4kB (M) 0*8kB 0*16kB 1*32kB (M) 0*64kB 0*128kB 0*256kB 1*512kB (M) 0*1024kB 1*2048kB (M) 54449*4096kB (M) = 223025700kB
[56139.903565] Node 3 Movable: 0*4kB 1*8kB (M) 0*16kB 1*32kB (M) 0*64kB 1*128kB (M) 1*256kB (M) 1*512kB (M) 1*1024kB (M) 1*2048kB (M) 51081*4096kB (M) = 209231784kB
[56139.903572] Node 0 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[56139.903573] Node 0 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[56139.903574] Node 1 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[56139.903574] Node 1 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[56139.903575] Node 2 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[56139.903576] Node 2 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[56139.903577] Node 3 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[56139.903577] Node 3 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[56139.903578] 35061 total pagecache pages
[56139.903580] 0 pages in swap cache
[56139.903580] Swap cache stats: add 0, delete 0, find 0/0
[56139.903581] Free swap  = 0kB
[56139.903581] Total swap = 0kB
[56139.903582] 147760473 pages RAM
[56139.903583] 0 pages HighMem/MovableOnly
[56139.903583] 286981 pages reserved
[56139.903584] 0 pages hwpoisoned
[56139.903584] Tasks state (memory values in pages):
[56139.903584] [  pid  ]   uid  tgid total_vm      rss pgtables_bytes swapents oom_score_adj name
[56139.903673] [    677]     0   677    43522    30889   380928        0             0 systemd-journal
[56139.903676] [    715]     0   715     6194     1010    81920        0         -1000 systemd-udevd
[56139.903680] [   1371]   101  1371    23272      635    81920        0             0 systemd-timesyn
[56139.903682] [   1372]   108  1372     1707      108    53248        0             0 rpcbind
[56139.903684] [   1395]     0  1395    20624      500    57344        0             0 irqbalance
[56139.903685] [   1396]     0  1396     1741      108    49152        0             0 ndctl
[56139.903686] [   1397]     0  1397    56472     1032    86016        0             0 rsyslogd
[56139.903687] [   1398]     0  1398     4866      430    77824        0             0 systemd-logind
[56139.903689] [   1401]   104  1401     2292      530    53248        0          -900 dbus-daemon
[56139.903690] [   1417]     0  1417     2373      766    57344        0             0 dhclient
[56139.903692] [   1422]     0  1422    27611     1936   114688        0             0 unattended-upgr
[56139.903693] [   1428]     0  1428     1419       31    49152        0             0 agetty
[56139.903694] [   1429]     0  1429   722922     7639   524288        0          -999 containerd
[56139.903696] [   1468]     0  1468     3979      715    69632        0         -1000 sshd
[56139.903697] [   1577]   107  1577    96757      918   106496        0         -1000 nslcd
[56139.903699] [   1585]     0  1585     2142      260    61440        0             0 cron
[56139.903700] [   1862]   106  1862     5095      477    81920        0             0 exim4
[56139.903701] [   1924]  1234  1924     5290      349    73728        0             0 systemd
[56139.903703] [   1925]  1234  1925     5933      693    77824        0             0 (sd-pam)
[56139.903705] [   1989]  1234  1989     3184     1003    73728        0             0 tmux: server
[56139.903706] [   2093]  1234  2093     2038      431    53248        0             0 bash
[56139.903707] [   3013]     0  3013     2574      109    57344        0             0 sudo
[56139.903709] [   3037]     0  3037     1430       30    49152        0             0 dmesg
[56139.903710] [   3415]  1234  3415     2007      395    57344        0             0 bash
[56139.903712] [   4635]  1234  4635     1414       99    53248        0             0 make
[56139.903713] [   4723]  1234  4723     2007      412    57344        0             0 bash
[56139.903715] [   4797]  1234  4797     1414       62    45056        0             0 make
[56139.903716] [   4801]  1234  4801      598       17    40960        0             0 sh
[56139.903718] [   4802]  1234  4802     1562      525    57344        0             0 watch
[56139.903720] [  52818]  1234 52818     1414      551    49152        0             0 make
[56139.903722] [  34713]  1234 34713     1414      586    53248        0             0 make
[56139.903724] [  34751]  1234 34751      598      174    49152        0             0 sh
[56139.903726] [  34752]  1234 34752     7173     3811    94208        0             0 perf
[56139.903727] [  34753]  1234 34753     1317      187    40960        0             0 tee
[56139.903731] [  34754]  1234 34754 55488884 24285754 195133440        0             0 mg.E.x
[56139.903733] [  34938]     0 34938     4278     1955    73728        0             0 sshd
[56139.903734] [  34939]   105 34939     3979     1238    69632        0             0 sshd
[56139.903736] [  34947]  1234 34947     1562      174    49152        0             0 watch
[56139.903737] [  34948]  1234 34948      598      190    40960        0             0 sh
[56139.903738] [  34949]     0 34949     2574     1030    53248        0             0 sudo
[56139.903743] [  34950]     0 34950     1662      741    53248        0             0 status
[56139.903744] [  34953]     0 34953     4278     1978    73728        0             0 sshd
[56139.903745] [  34954]     0 34954      576      187    40960        0             0 numastat
[56139.903747] [  34955]   105 34955     3979     1266    69632        0             0 sshd
[56139.903748] oom-kill:constraint=CONSTRAINT_NONE,nodemask=(null),cpuset=/,mems_allowed=0-3,global_oom,task_memcg=/user.slice/user-1234.slice/session-1.scope,task=mg.E.x,pid=34754,uid=1234
[56139.903834] Out of memory: Killed process 34754 (mg.E.x) total-vm:221955536kB, anon-rss:97138940kB, file-rss:4096kB, shmem-rss:0kB, UID:1234 pgtables:190560kB oom_score_adj:0
== END ============================
