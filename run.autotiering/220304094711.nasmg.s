== BEGIN ==========================
Fri 04 Mar 2022 10:49:31 AM WET
 # note: 
 # tag: nasmg.s.autotiering
 # running nasmg (220304094711: Size s)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1024x1024x1024  (class D)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:          31.592 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 VERIFICATION FAILED
 L2 Norm is              0.1590005169528E-09
 The correct L2 Norm is  0.1583275060440E-09


 MG Benchmark Completed.
 Class           =                        D
 Size            =           1024x1024x1024
 Iterations      =                       50
 Time in seconds =                   163.93
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                 18995.19
 Mop/s/thread    =                   593.60
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              09 Feb 2022

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Fri Mar  4 10:49:31 2022


 Performance counter stats for 'system wide':

          7,914.93 Joules power/energy-ram/                                           
         32,848.26 Joules power/energy-pkg/                                           

     197.201724903 seconds time elapsed

== DMESG ==========================
[   57.795450] Policy zone: Normal
== END ============================
