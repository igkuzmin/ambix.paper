== BEGIN ==========================
Fri 04 Mar 2022 11:21:12 PM WET
 # note: 
 # tag: nascg.l.autotiering
 # running nascg (220304173107: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     9000000
 Iterations:                      5
 Number of available threads:    32

bash: Killed
===================================
# started on Fri Mar  4 23:21:13 2022


 Performance counter stats for 'system wide':

         11,729.18 Joules power/energy-ram/                                           
         74,374.39 Joules power/energy-pkg/                                           

     464.072619088 seconds time elapsed

== DMESG ==========================
[28855.302739] oom_reaper: reaped process 50593 (bfs), now anon-rss:0kB, file-rss:0kB, shmem-rss:0kB
[50321.583233] cg.l.x invoked oom-killer: gfp_mask=0x400dc0(GFP_KERNEL_ACCOUNT|__GFP_ZERO), order=0, oom_score_adj=0
[50321.583237] CPU: 19 PID: 28485 Comm: cg.l.x Tainted: G            E     5.9.6-vanila #1
[50321.583238] Hardware name: Supermicro SYS-6019P-WT/X11DDW-L, BIOS 3.2 10/16/2019
[50321.583239] Call Trace:
[50321.583246]  dump_stack+0x6d/0x88
[50321.583249]  dump_header+0x4a/0x1d8
[50321.583251]  oom_kill_process.cold.36+0xb/0x10
[50321.583254]  out_of_memory+0x1a8/0x4d0
[50321.583257]  __alloc_pages_slowpath.constprop.110+0xbd7/0xcc0
[50321.583259]  __alloc_pages_nodemask+0x2de/0x310
[50321.583263]  pte_alloc_one+0x13/0x40
[50321.583266]  __pte_alloc+0x16/0x100
[50321.583268]  handle_mm_fault+0x1524/0x1640
[50321.583272]  exc_page_fault+0x290/0x550
[50321.583275]  ? asm_exc_page_fault+0x8/0x30
[50321.583276]  asm_exc_page_fault+0x1e/0x30
[50321.583279] RIP: 0033:0x7fbd820e088b
[50321.583281] Code: c5 fe 6f 06 c5 fe 6f 4e 20 c5 fe 6f 56 40 c5 fe 6f 5e 60 48 81 c6 80 00 00 00 48 81 ea 80 00 00 00 c5 fd 7f 07 c5 fd 7f 4f 20 <c5> fd 7f 57 40 c5 fd 7f 5f 60 48 81 c7 80 00 00 00 48 81 fa 80 00
[50321.583282] RSP: 002b:00007fff326d6b68 EFLAGS: 00010206
[50321.583284] RAX: 00007f9aecfffab0 RBX: 00000000037e79a5 RCX: 00007f9aed000a38
[50321.583284] RDX: 0000000000000a18 RSI: 00007fa6b2b13318 RDI: 00007f9aecffffc0
[50321.583285] RBP: 00000000035d5d4a R08: fffffffffffffff0 R09: 00000000035d5b54
[50321.583286] R10: 00007fa696bd7010 R11: 00007f9aecfffab0 R12: 00000000000152af
[50321.583287] R13: 00000000035d5b55 R14: 0000000000044aa2 R15: 00007fb225c0e010
[50321.583289] Mem-Info:
[50321.583304] active_anon:429 inactive_anon:23943838 isolated_anon:0
                active_file:7506 inactive_file:7060 isolated_file:0
                unevictable:0 dirty:9 writeback:0
                slab_reclaimable:13923 slab_unreclaimable:37447
                mapped:23207 shmem:20967 pagetables:47262 bounce:0
                free:108483706 free_pcp:2930 free_cma:0
[50321.583307] Node 0 active_anon:1600kB inactive_anon:15090972kB active_file:1444kB inactive_file:0kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:10720kB dirty:16kB writeback:0kB shmem:11968kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 4096kB writeback_tmp:0kB kernel_stack:6688kB all_unreclaimable? yes
[50321.583309] Node 1 active_anon:96kB inactive_anon:15848296kB active_file:0kB inactive_file:1216kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:4kB dirty:4kB writeback:0kB shmem:724kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 0kB writeback_tmp:0kB kernel_stack:4816kB all_unreclaimable? yes
[50321.583312] Node 2 active_anon:12kB inactive_anon:33021236kB active_file:19572kB inactive_file:22096kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:73556kB dirty:16kB writeback:0kB shmem:71176kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 0kB writeback_tmp:0kB kernel_stack:0kB all_unreclaimable? no
[50321.583314] Node 3 active_anon:8kB inactive_anon:31814848kB active_file:9784kB inactive_file:5328kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:8548kB dirty:0kB writeback:0kB shmem:0kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 0kB writeback_tmp:0kB kernel_stack:0kB all_unreclaimable? no
[50321.583315] Node 0 DMA free:15888kB min:0kB low:12kB high:24kB reserved_highatomic:0KB active_anon:0kB inactive_anon:0kB active_file:0kB inactive_file:0kB unevictable:0kB writepending:0kB present:15972kB managed:15888kB mlocked:0kB pagetables:0kB bounce:0kB free_pcp:0kB local_pcp:0kB free_cma:0kB
[50321.583318] lowmem_reserve[]: 0 1621 31826 31826 31826
[50321.583321] Node 0 DMA32 free:120744kB min:88kB low:1748kB high:3408kB reserved_highatomic:0KB active_anon:0kB inactive_anon:1535736kB active_file:216kB inactive_file:0kB unevictable:0kB writepending:0kB present:1726208kB managed:1660672kB mlocked:0kB pagetables:1612kB bounce:0kB free_pcp:1480kB local_pcp:244kB free_cma:0kB
[50321.583324] lowmem_reserve[]: 0 0 30205 30205 30205
[50321.583325] Node 0 Normal free:3012kB min:5792kB low:36720kB high:67648kB reserved_highatomic:2048KB active_anon:1600kB inactive_anon:13555236kB active_file:0kB inactive_file:1088kB unevictable:0kB writepending:16kB present:31457280kB managed:30929996kB mlocked:0kB pagetables:95316kB bounce:0kB free_pcp:6372kB local_pcp:0kB free_cma:0kB
[50321.583328] lowmem_reserve[]: 0 0 0 0 0
[50321.583330] Node 1 Normal free:6452kB min:46864kB low:79860kB high:112856kB reserved_highatomic:0KB active_anon:96kB inactive_anon:15848296kB active_file:0kB inactive_file:1216kB unevictable:0kB writepending:4kB present:33554432kB managed:32999412kB mlocked:0kB pagetables:92120kB bounce:0kB free_pcp:0kB local_pcp:0kB free_cma:0kB
[50321.583333] lowmem_reserve[]: 0 0 0 0 0
[50321.583335] Node 2 Movable free:216278272kB min:14380kB low:276524kB high:538668kB reserved_highatomic:0KB active_anon:12kB inactive_anon:33021236kB active_file:19572kB inactive_file:22096kB unevictable:0kB writepending:12kB present:262144000kB managed:262144000kB mlocked:0kB pagetables:0kB bounce:0kB free_pcp:1860kB local_pcp:0kB free_cma:0kB
[50321.583338] lowmem_reserve[]: 0 0 0 0 0
[50321.583340] Node 3 Movable free:217510456kB min:14380kB low:276524kB high:538668kB reserved_highatomic:0KB active_anon:8kB inactive_anon:31814848kB active_file:9784kB inactive_file:5328kB unevictable:0kB writepending:0kB present:262144000kB managed:262144000kB mlocked:0kB pagetables:0kB bounce:0kB free_pcp:1972kB local_pcp:160kB free_cma:0kB
[50321.583343] lowmem_reserve[]: 0 0 0 0 0
[50321.583344] Node 0 DMA: 0*4kB 0*8kB 1*16kB (U) 0*32kB 2*64kB (U) 1*128kB (U) 1*256kB (U) 0*512kB 1*1024kB (U) 1*2048kB (M) 3*4096kB (M) = 15888kB
[50321.583350] Node 0 DMA32: 38*4kB (ME) 51*8kB (UME) 45*16kB (ME) 48*32kB (UME) 48*64kB (UME) 28*128kB (UM) 1*256kB (E) 1*512kB (E) 4*1024kB (UM) 2*2048kB (UE) 25*4096kB (UM) = 120832kB
[50321.583356] Node 0 Normal: 103*4kB (UMEH) 33*8kB (MEH) 18*16kB (UMEH) 3*32kB (H) 4*64kB (H) 1*128kB (H) 0*256kB 0*512kB 0*1024kB 0*2048kB 0*4096kB = 1444kB
[50321.583361] Node 1 Normal: 704*4kB (UME) 372*8kB (UM) 36*16kB (U) 15*32kB (U) 2*64kB (U) 0*128kB 0*256kB 0*512kB 0*1024kB 0*2048kB 0*4096kB = 6976kB
[50321.583366] Node 2 Movable: 1*4kB (M) 0*8kB 0*16kB 0*32kB 0*64kB 0*128kB 0*256kB 1*512kB (M) 0*1024kB 0*2048kB 52802*4096kB (M) = 216277508kB
[50321.583370] Node 3 Movable: 1*4kB (M) 1*8kB (M) 1*16kB (M) 1*32kB (M) 0*64kB 0*128kB 1*256kB (M) 0*512kB 0*1024kB 0*2048kB 53103*4096kB (M) = 217510204kB
[50321.583376] Node 0 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[50321.583377] Node 0 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[50321.583378] Node 1 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[50321.583379] Node 1 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[50321.583379] Node 2 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[50321.583380] Node 2 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[50321.583381] Node 3 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[50321.583382] Node 3 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[50321.583382] 35491 total pagecache pages
[50321.583383] 0 pages in swap cache
[50321.583384] Swap cache stats: add 0, delete 0, find 0/0
[50321.583385] Free swap  = 0kB
[50321.583385] Total swap = 0kB
[50321.583386] 147760473 pages RAM
[50321.583386] 0 pages HighMem/MovableOnly
[50321.583387] 286981 pages reserved
[50321.583387] 0 pages hwpoisoned
[50321.583388] Tasks state (memory values in pages):
[50321.583388] [  pid  ]   uid  tgid total_vm      rss pgtables_bytes swapents oom_score_adj name
[50321.583875] [    677]     0   677    41441    28969   364544        0             0 systemd-journal
[50321.583897] [    715]     0   715     6194     1010    81920        0         -1000 systemd-udevd
[50321.583969] [   1371]   101  1371    23272      635    81920        0             0 systemd-timesyn
[50321.583974] [   1372]   108  1372     1707      108    53248        0             0 rpcbind
[50321.583985] [   1395]     0  1395    20624      489    57344        0             0 irqbalance
[50321.583988] [   1396]     0  1396     1741      108    49152        0             0 ndctl
[50321.583998] [   1397]     0  1397    56472      874    86016        0             0 rsyslogd
[50321.583999] [   1398]     0  1398     4866      430    77824        0             0 systemd-logind
[50321.584012] [   1401]   104  1401     2292      530    53248        0          -900 dbus-daemon
[50321.584017] [   1417]     0  1417     2373      765    57344        0             0 dhclient
[50321.584019] [   1422]     0  1422    27611     1936   114688        0             0 unattended-upgr
[50321.584022] [   1428]     0  1428     1419       31    49152        0             0 agetty
[50321.584025] [   1429]     0  1429   686056     7473   503808        0          -999 containerd
[50321.584045] [   1468]     0  1468     3979      715    69632        0         -1000 sshd
[50321.584053] [   1577]   107  1577    96757      918   106496        0         -1000 nslcd
[50321.584056] [   1585]     0  1585     2142      260    61440        0             0 cron
[50321.584066] [   1862]   106  1862     5095      477    81920        0             0 exim4
[50321.584072] [   1924]  1234  1924     5290      349    73728        0             0 systemd
[50321.584077] [   1925]  1234  1925     5933      693    77824        0             0 (sd-pam)
[50321.584087] [   1989]  1234  1989     3176      994    73728        0             0 tmux: server
[50321.584101] [   2093]  1234  2093     2038      431    53248        0             0 bash
[50321.584118] [   3013]     0  3013     2574      109    57344        0             0 sudo
[50321.584120] [   3037]     0  3037     1430       30    49152        0             0 dmesg
[50321.584126] [   3415]  1234  3415     2007      395    57344        0             0 bash
[50321.584165] [   4635]  1234  4635     1414       99    53248        0             0 make
[50321.584185] [   4723]  1234  4723     2007      412    57344        0             0 bash
[50321.584186] [   4797]  1234  4797     1414       62    45056        0             0 make
[50321.584189] [   4801]  1234  4801      598       17    40960        0             0 sh
[50321.584449] [   4802]  1234  4802     1562      525    57344        0             0 watch
[50321.584463] [  52818]  1234 52818     1414      551    49152        0             0 make
[50321.584476] [  28449]  1234 28449     1414      550    49152        0             0 make
[50321.584506] [  28482]  1234 28482      598      174    45056        0             0 sh
[50321.584518] [  28483]  1234 28483     7173     3786    98304        0             0 perf
[50321.584519] [  28484]  1234 28484     1317      187    53248        0             0 tee
[50321.584522] [  28485]  1234 28485 36525624 23905145 192184320        0             0 cg.l.x
[50321.584525] [  29270]     0 29270     4278     2008    73728        0             0 sshd
[50321.584529] [  29273]   105 29273     3979     1255    69632        0             0 sshd
[50321.584539] [  29277]  1234 29277     1562      174    49152        0             0 watch
[50321.584564] [  29278]  1234 29278      598      190    45056        0             0 sh
[50321.584769] [  29279]     0 29279     2574      989    65536        0             0 sudo
[50321.584816] [  29280]     0 29280     1662      762    49152        0             0 status
[50321.584818] [  29281]     0 29281     1662       60    45056        0             0 status
[50321.584822] oom-kill:constraint=CONSTRAINT_NONE,nodemask=(null),cpuset=/,mems_allowed=0-3,global_oom,task_memcg=/user.slice/user-1234.slice/session-1.scope,task=cg.l.x,pid=28485,uid=1234
[50321.584925] Out of memory: Killed process 28485 (cg.l.x) total-vm:146102496kB, anon-rss:95616476kB, file-rss:4236kB, shmem-rss:0kB, UID:1234 pgtables:187680kB oom_score_adj:0
[50325.340602] oom_reaper: reaped process 28485 (cg.l.x), now anon-rss:0kB, file-rss:0kB, shmem-rss:0kB
== END ============================
