== BEGIN ==========================
Fri 04 Mar 2022 05:03:15 PM WET
 # note: 
 # tag: nasmg.l.autotiering
 # running nasmg (220304094711: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 2048x2048x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

bash: Killed
===================================
# started on Fri Mar  4 17:03:15 2022


 Performance counter stats for 'system wide':

          2,829.30 Joules power/energy-ram/                                           
         18,121.39 Joules power/energy-pkg/                                           

     118.590034013 seconds time elapsed

== DMESG ==========================
[21511.053788] oom_reaper: reaped process 44473 (cg.l.x), now anon-rss:0kB, file-rss:0kB, shmem-rss:0kB
[27302.100557] mg.E.x invoked oom-killer: gfp_mask=0x400dc0(GFP_KERNEL_ACCOUNT|__GFP_ZERO), order=0, oom_score_adj=0
[27302.100573] CPU: 6 PID: 50351 Comm: mg.E.x Tainted: G            E     5.9.6-vanila #1
[27302.100574] Hardware name: Supermicro SYS-6019P-WT/X11DDW-L, BIOS 3.2 10/16/2019
[27302.100576] Call Trace:
[27302.100625]  dump_stack+0x6d/0x88
[27302.100636]  dump_header+0x4a/0x1d8
[27302.100640]  oom_kill_process.cold.36+0xb/0x10
[27302.100651]  out_of_memory+0x1a8/0x4d0
[27302.100655]  __alloc_pages_slowpath.constprop.110+0xbd7/0xcc0
[27302.100663]  __alloc_pages_nodemask+0x2de/0x310
[27302.100678]  pte_alloc_one+0x13/0x40
[27302.100720]  __pte_alloc+0x16/0x100
[27302.100724]  handle_mm_fault+0x1524/0x1640
[27302.100728]  exc_page_fault+0x290/0x550
[27302.100738]  ? asm_exc_page_fault+0x8/0x30
[27302.100740]  asm_exc_page_fault+0x1e/0x30
[27302.100746] RIP: 0033:0x55c4dd687bb8
[27302.100754] Code: 66 0f 59 c7 66 0f 58 d4 66 0f 58 cb 66 0f 59 d5 66 44 0f 5c c0 66 0f 59 ce 66 41 0f 28 c0 66 0f 5c c1 66 0f 5c c2 66 0f 28 d4 <42> 0f 11 04 0e 49 83 c1 10 4d 39 d9 75 a2 8b bc 24 20 01 00 00 39
[27302.100756] RSP: 002b:00007fa599e2c890 EFLAGS: 00010206
[27302.100757] RAX: 00000000494f874d RBX: 00007fa599e309f0 RCX: 00000000494f874d
[27302.100758] RDX: 00000000494f874d RSI: 00007fa7e75ffa88 RDI: 00007fca4c8daa88
[27302.100759] RBP: 000055c4dd6962c0 R08: 00007fba408a9a88 R09: 0000000000000570
[27302.100760] R10: 00007fa599e2cf50 R11: 0000000000004000 R12: 0000000000000802
[27302.100761] R13: 0000000000000800 R14: 0000000000000802 R15: 00007fc802117010
[27302.100764] Mem-Info:
[27302.100794] active_anon:430 inactive_anon:24387626 isolated_anon:0
                active_file:8142 inactive_file:6021 isolated_file:0
                unevictable:0 dirty:1 writeback:0
                slab_reclaimable:12231 slab_unreclaimable:36902
                mapped:14185 shmem:10727 pagetables:47824 bounce:0
                free:108042554 free_pcp:1488 free_cma:0
[27302.100797] Node 0 active_anon:1600kB inactive_anon:15075616kB active_file:4164kB inactive_file:4kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:9052kB dirty:0kB writeback:0kB shmem:9408kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 4096kB writeback_tmp:0kB kernel_stack:6592kB all_unreclaimable? no
[27302.100800] Node 1 active_anon:96kB inactive_anon:15860972kB active_file:1740kB inactive_file:0kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:848kB dirty:0kB writeback:0kB shmem:724kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 0kB writeback_tmp:0kB kernel_stack:4912kB all_unreclaimable? no
[27302.100802] Node 2 active_anon:12kB inactive_anon:25847132kB active_file:19936kB inactive_file:17048kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:39768kB dirty:4kB writeback:0kB shmem:32776kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 0kB writeback_tmp:0kB kernel_stack:0kB all_unreclaimable? no
[27302.100804] Node 3 active_anon:12kB inactive_anon:40766784kB active_file:6728kB inactive_file:7032kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:7072kB dirty:0kB writeback:0kB shmem:0kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 0kB writeback_tmp:0kB kernel_stack:0kB all_unreclaimable? no
[27302.100806] Node 0 DMA free:15888kB min:0kB low:12kB high:24kB reserved_highatomic:0KB active_anon:0kB inactive_anon:0kB active_file:0kB inactive_file:0kB unevictable:0kB writepending:0kB present:15972kB managed:15888kB mlocked:0kB pagetables:0kB bounce:0kB free_pcp:0kB local_pcp:0kB free_cma:0kB
[27302.100809] lowmem_reserve[]: 0 1621 31826 31826 31826
[27302.100812] Node 0 DMA32 free:120816kB min:88kB low:1748kB high:3408kB reserved_highatomic:0KB active_anon:0kB inactive_anon:1537364kB active_file:32kB inactive_file:0kB unevictable:0kB writepending:0kB present:1726208kB managed:1660672kB mlocked:0kB pagetables:808kB bounce:0kB free_pcp:0kB local_pcp:0kB free_cma:0kB
[27302.100815] lowmem_reserve[]: 0 0 30205 30205 30205
[27302.100817] Node 0 Normal free:9992kB min:9888kB low:40816kB high:71744kB reserved_highatomic:2048KB active_anon:1600kB inactive_anon:13538252kB active_file:4132kB inactive_file:4kB unevictable:0kB writepending:0kB present:31457280kB managed:30929996kB mlocked:0kB pagetables:109244kB bounce:0kB free_pcp:0kB local_pcp:0kB free_cma:0kB
[27302.100820] lowmem_reserve[]: 0 0 0 0 0
[27302.100823] Node 1 Normal free:7788kB min:10000kB low:42996kB high:75992kB reserved_highatomic:2048KB active_anon:96kB inactive_anon:15860972kB active_file:1740kB inactive_file:0kB unevictable:0kB writepending:0kB present:33554432kB managed:32999412kB mlocked:0kB pagetables:81244kB bounce:0kB free_pcp:0kB local_pcp:0kB free_cma:0kB
[27302.100825] lowmem_reserve[]: 0 0 0 0 0
[27302.100828] Node 2 Movable free:223456436kB min:14380kB low:276524kB high:538668kB reserved_highatomic:0KB active_anon:12kB inactive_anon:25847132kB active_file:19936kB inactive_file:17048kB unevictable:0kB writepending:4kB present:262144000kB managed:262144000kB mlocked:0kB pagetables:0kB bounce:0kB free_pcp:3024kB local_pcp:0kB free_cma:0kB
[27302.100830] lowmem_reserve[]: 0 0 0 0 0
[27302.100832] Node 3 Movable free:208559296kB min:14380kB low:276524kB high:538668kB reserved_highatomic:0KB active_anon:12kB inactive_anon:40766784kB active_file:6728kB inactive_file:7032kB unevictable:0kB writepending:0kB present:262144000kB managed:262144000kB mlocked:0kB pagetables:0kB bounce:0kB free_pcp:3388kB local_pcp:0kB free_cma:0kB
[27302.100835] lowmem_reserve[]: 0 0 0 0 0
[27302.100836] Node 0 DMA: 0*4kB 0*8kB 1*16kB (U) 0*32kB 2*64kB (U) 1*128kB (U) 1*256kB (U) 0*512kB 1*1024kB (U) 1*2048kB (M) 3*4096kB (M) = 15888kB
[27302.100842] Node 0 DMA32: 222*4kB (UME) 95*8kB (UME) 88*16kB (UME) 73*32kB (UME) 37*64kB (UME) 19*128kB (UME) 1*256kB (M) 1*512kB (U) 4*1024kB (UE) 2*2048kB (UE) 25*4096kB (UM) = 121552kB
[27302.100848] Node 0 Normal: 356*4kB (UMEH) 254*8kB (UMEH) 150*16kB (UMEH) 86*32kB (UMH) 34*64kB (UM) 1*128kB (H) 0*256kB 1*512kB (H) 0*1024kB 0*2048kB 0*4096kB = 11424kB
[27302.100853] Node 1 Normal: 703*4kB (UMEH) 264*8kB (UMEH) 72*16kB (UMEH) 28*32kB (UMEH) 19*64kB (UH) 2*128kB (U) 0*256kB 0*512kB 0*1024kB 0*2048kB 0*4096kB = 8444kB
[27302.100859] Node 2 Movable: 1*4kB (M) 0*8kB 0*16kB 1*32kB (M) 1*64kB (M) 6*128kB (M) 0*256kB 1*512kB (M) 0*1024kB 1*2048kB (M) 54553*4096kB (M) = 223452516kB
[27302.100864] Node 3 Movable: 0*4kB 1*8kB (M) 0*16kB 0*32kB 1*64kB (M) 1*128kB (M) 1*256kB (M) 0*512kB 1*1024kB (M) 1*2048kB (M) 50916*4096kB (M) = 208555464kB
[27302.100870] Node 0 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[27302.100871] Node 0 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[27302.100872] Node 1 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[27302.100873] Node 1 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[27302.100874] Node 2 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[27302.100875] Node 2 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[27302.100876] Node 3 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[27302.100877] Node 3 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[27302.100877] 24852 total pagecache pages
[27302.100879] 0 pages in swap cache
[27302.100880] Swap cache stats: add 0, delete 0, find 0/0
[27302.100880] Free swap  = 0kB
[27302.100880] Total swap = 0kB
[27302.100881] 147760473 pages RAM
[27302.100882] 0 pages HighMem/MovableOnly
[27302.100882] 286981 pages reserved
[27302.100882] 0 pages hwpoisoned
[27302.100883] Tasks state (memory values in pages):
[27302.100883] [  pid  ]   uid  tgid total_vm      rss pgtables_bytes swapents oom_score_adj name
[27302.101029] [    677]     0   677    23929    15259   237568        0             0 systemd-journal
[27302.101032] [    715]     0   715     6194      862    81920        0         -1000 systemd-udevd
[27302.101035] [   1371]   101  1371    23272      352    81920        0             0 systemd-timesyn
[27302.101037] [   1372]   108  1372     1707      108    53248        0             0 rpcbind
[27302.101038] [   1395]     0  1395    20624      423    57344        0             0 irqbalance
[27302.101040] [   1396]     0  1396     1741      108    49152        0             0 ndctl
[27302.101041] [   1397]     0  1397    56472      708    86016        0             0 rsyslogd
[27302.101042] [   1398]     0  1398     4866      243    77824        0             0 systemd-logind
[27302.101044] [   1401]   104  1401     2259      448    53248        0          -900 dbus-daemon
[27302.101045] [   1417]     0  1417     2373      750    57344        0             0 dhclient
[27302.101046] [   1422]     0  1422    27611     1936   114688        0             0 unattended-upgr
[27302.101048] [   1428]     0  1428     1419       31    49152        0             0 agetty
[27302.101049] [   1429]     0  1429   667495     7536   495616        0          -999 containerd
[27302.101050] [   1468]     0  1468     3979      551    69632        0         -1000 sshd
[27302.101058] [   1577]   107  1577    96757      879   106496        0         -1000 nslcd
[27302.101060] [   1585]     0  1585     2142      220    61440        0             0 cron
[27302.101062] [   1862]   106  1862     5095      440    81920        0             0 exim4
[27302.101064] [   1924]  1234  1924     5290      349    73728        0             0 systemd
[27302.101065] [   1925]  1234  1925     5933      693    77824        0             0 (sd-pam)
[27302.101067] [   1989]  1234  1989     3049      799    69632        0             0 tmux: server
[27302.101068] [   2093]  1234  2093     2038      470    53248        0             0 bash
[27302.101070] [   3013]     0  3013     2574      136    57344        0             0 sudo
[27302.101071] [   3037]     0  3037     1430       30    49152        0             0 dmesg
[27302.101084] [   3415]  1234  3415     2007      436    57344        0             0 bash
[27302.101089] [   4635]  1234  4635     1414       91    53248        0             0 make
[27302.101097] [   4642]  1234  4642     1414       61    53248        0             0 make
[27302.101098] [   4723]  1234  4723     2007      453    57344        0             0 bash
[27302.101101] [   4797]  1234  4797     1414       62    45056        0             0 make
[27302.101105] [   4801]  1234  4801      598       17    40960        0             0 sh
[27302.101106] [   4802]  1234  4802     1562      471    57344        0             0 watch
[27302.101109] [  50307]  1234 50307     1414      553    53248        0             0 make
[27302.101110] [  50341]  1234 50341      598      190    45056        0             0 sh
[27302.101111] [  50342]  1234 50342     7173     3624    98304        0             0 perf
[27302.101113] [  50343]  1234 50343     1317      187    49152        0             0 tee
[27302.101127] [  50344]  1234 50344 55472500 24361489 195756032        0             0 mg.E.x
[27302.101129] [  50527]  1234 50527     1562      174    49152        0             0 watch
[27302.101134] [  50528]  1234 50528      598      189    45056        0             0 sh
[27302.101136] [  50529]     0 50529     2574     1009    61440        0             0 sudo
[27302.101138] [  50530]     0 50530     1662      765    49152        0             0 status
[27302.101144] [  50533]     0 50533      576      187    36864        0             0 numastat
[27302.101149] oom-kill:constraint=CONSTRAINT_NONE,nodemask=(null),cpuset=/,mems_allowed=0-3,global_oom,task_memcg=/user.slice/user-1234.slice/session-1.scope,task=mg.E.x,pid=50344,uid=1234
[27302.101324] Out of memory: Killed process 50344 (mg.E.x) total-vm:221890000kB, anon-rss:97441808kB, file-rss:4232kB, shmem-rss:0kB, UID:1234 pgtables:191168kB oom_score_adj:0
== END ============================
