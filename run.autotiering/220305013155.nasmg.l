== BEGIN ==========================
Sat 05 Mar 2022 09:32:59 AM WET
 # note: 
 # tag: nasmg.l.autotiering
 # running nasmg (220305013155: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 2048x2048x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

bash: Killed
===================================
# started on Sat Mar  5 09:32:59 2022


 Performance counter stats for 'system wide':

          3,364.22 Joules power/energy-ram/                                           
         21,495.39 Joules power/energy-pkg/                                           

     142.393681948 seconds time elapsed

== DMESG ==========================
[79867.565836] oom_reaper: reaped process 14472 (cg.l.x), now anon-rss:0kB, file-rss:0kB, shmem-rss:0kB
[86706.822004] mg.E.x invoked oom-killer: gfp_mask=0x400dc0(GFP_KERNEL_ACCOUNT|__GFP_ZERO), order=0, oom_score_adj=0
[86706.822025] CPU: 10 PID: 21817 Comm: mg.E.x Tainted: G            E     5.9.6-vanila #1
[86706.822028] Hardware name: Supermicro SYS-6019P-WT/X11DDW-L, BIOS 3.2 10/16/2019
[86706.822029] Call Trace:
[86706.822062]  dump_stack+0x6d/0x88
[86706.822077]  dump_header+0x4a/0x1d8
[86706.822080]  oom_kill_process.cold.36+0xb/0x10
[86706.822089]  out_of_memory+0x1a8/0x4d0
[86706.822094]  __alloc_pages_slowpath.constprop.110+0xbd7/0xcc0
[86706.822098]  __alloc_pages_nodemask+0x2de/0x310
[86706.822106]  pte_alloc_one+0x13/0x40
[86706.822113]  __pte_alloc+0x16/0x100
[86706.822118]  handle_mm_fault+0x1524/0x1640
[86706.822122]  exc_page_fault+0x290/0x550
[86706.822129]  ? asm_exc_page_fault+0x8/0x30
[86706.822130]  asm_exc_page_fault+0x1e/0x30
[86706.822136] RIP: 0033:0x56140ce7fbb8
[86706.822153] Code: 66 0f 59 c7 66 0f 58 d4 66 0f 58 cb 66 0f 59 d5 66 44 0f 5c c0 66 0f 59 ce 66 41 0f 28 c0 66 0f 5c c1 66 0f 5c c2 66 0f 28 d4 <42> 0f 11 04 0e 49 83 c1 10 4d 39 d9 75 a2 8b bc 24 20 01 00 00 39
[86706.822155] RSP: 002b:00007f1ac3c88890 EFLAGS: 00010206
[86706.822159] RAX: 000000010732a439 RBX: 00007f1ac3c8c9f0 RCX: 000000010732a439
[86706.822160] RDX: 000000010732a439 RSI: 00007f2309dfd1e8 RDI: 00007f456f0d81e8
[86706.822160] RBP: 000056140ce8e2c0 R08: 00007f35630a71e8 R09: 0000000000002e10
[86706.822163] R10: 00007f1ac3c8b7f0 R11: 0000000000004000 R12: 0000000000000802
[86706.822164] R13: 0000000000000800 R14: 0000000000000802 R15: 00007f3d35786010
[86706.822167] Mem-Info:
[86706.822203] active_anon:9004 inactive_anon:24514074 isolated_anon:0
                active_file:9427 inactive_file:6868 isolated_file:0
                unevictable:0 dirty:5 writeback:0
                slab_reclaimable:15657 slab_unreclaimable:38954
                mapped:20818 shmem:33255 pagetables:48190 bounce:0
                free:107897670 free_pcp:2195 free_cma:0
[86706.822215] Node 0 active_anon:5568kB inactive_anon:15056808kB active_file:1328kB inactive_file:356kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:7076kB dirty:16kB writeback:0kB shmem:16888kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 4096kB writeback_tmp:0kB kernel_stack:6656kB all_unreclaimable? no
[86706.822218] Node 1 active_anon:100kB inactive_anon:15845040kB active_file:2384kB inactive_file:12kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:92kB dirty:0kB writeback:0kB shmem:724kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 0kB writeback_tmp:0kB kernel_stack:4848kB all_unreclaimable? no
[86706.822220] Node 2 active_anon:30344kB inactive_anon:26469524kB active_file:21516kB inactive_file:21648kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:67528kB dirty:4kB writeback:0kB shmem:115408kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 0kB writeback_tmp:0kB kernel_stack:0kB all_unreclaimable? no
[86706.822222] Node 3 active_anon:4kB inactive_anon:40684924kB active_file:12480kB inactive_file:5456kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:8576kB dirty:0kB writeback:0kB shmem:0kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 0kB writeback_tmp:0kB kernel_stack:0kB all_unreclaimable? no
[86706.822225] Node 0 DMA free:15888kB min:0kB low:12kB high:24kB reserved_highatomic:0KB active_anon:0kB inactive_anon:0kB active_file:0kB inactive_file:0kB unevictable:0kB writepending:0kB present:15972kB managed:15888kB mlocked:0kB pagetables:0kB bounce:0kB free_pcp:0kB local_pcp:0kB free_cma:0kB
[86706.822228] lowmem_reserve[]: 0 1621 31826 31826 31826
[86706.822234] Node 0 DMA32 free:120964kB min:88kB low:1748kB high:3408kB reserved_highatomic:0KB active_anon:0kB inactive_anon:1535740kB active_file:0kB inactive_file:0kB unevictable:0kB writepending:0kB present:1726208kB managed:1660672kB mlocked:0kB pagetables:672kB bounce:0kB free_pcp:0kB local_pcp:0kB free_cma:0kB
[86706.822242] lowmem_reserve[]: 0 0 30205 30205 30205
[86706.822248] Node 0 Normal free:13052kB min:11936kB low:42864kB high:73792kB reserved_highatomic:2048KB active_anon:5568kB inactive_anon:13521068kB active_file:1328kB inactive_file:356kB unevictable:0kB writepending:16kB present:31457280kB managed:30929996kB mlocked:0kB pagetables:107132kB bounce:0kB free_pcp:1980kB local_pcp:0kB free_cma:0kB
[86706.822251] lowmem_reserve[]: 0 0 0 0 0
[86706.822256] Node 1 Normal free:7208kB min:10000kB low:42996kB high:75992kB reserved_highatomic:0KB active_anon:100kB inactive_anon:15845040kB active_file:2384kB inactive_file:12kB unevictable:0kB writepending:0kB present:33554432kB managed:32999412kB mlocked:0kB pagetables:84956kB bounce:0kB free_pcp:1464kB local_pcp:0kB free_cma:0kB
[86706.822263] lowmem_reserve[]: 0 0 0 0 0
[86706.822265] Node 2 Movable free:222795592kB min:14380kB low:276524kB high:538668kB reserved_highatomic:0KB active_anon:30344kB inactive_anon:26469524kB active_file:21516kB inactive_file:21648kB unevictable:0kB writepending:4kB present:262144000kB managed:262144000kB mlocked:0kB pagetables:0kB bounce:0kB free_pcp:3304kB local_pcp:0kB free_cma:0kB
[86706.822271] lowmem_reserve[]: 0 0 0 0 0
[86706.822273] Node 3 Movable free:208638060kB min:14380kB low:276524kB high:538668kB reserved_highatomic:0KB active_anon:4kB inactive_anon:40684924kB active_file:12480kB inactive_file:5456kB unevictable:0kB writepending:0kB present:262144000kB managed:262144000kB mlocked:0kB pagetables:0kB bounce:0kB free_pcp:1680kB local_pcp:0kB free_cma:0kB
[86706.822276] lowmem_reserve[]: 0 0 0 0 0
[86706.822277] Node 0 DMA: 0*4kB 0*8kB 1*16kB (U) 0*32kB 2*64kB (U) 1*128kB (U) 1*256kB (U) 0*512kB 1*1024kB (U) 1*2048kB (M) 3*4096kB (M) = 15888kB
[86706.822282] Node 0 DMA32: 68*4kB (UME) 74*8kB (UME) 101*16kB (UME) 75*32kB (UM) 45*64kB (UME) 17*128kB (UME) 3*256kB (UME) 3*512kB (UE) 3*1024kB (UE) 2*2048kB (UM) 25*4096kB (UM) = 121808kB
[86706.822717] Node 0 Normal: 1446*4kB (UMEH) 382*8kB (UMEH) 163*16kB (UMEH) 127*32kB (UMEH) 48*64kB (UMH) 5*128kB (UH) 0*256kB 0*512kB 0*1024kB 0*2048kB 0*4096kB = 19224kB
[86706.822723] Node 1 Normal: 488*4kB (UME) 243*8kB (UME) 84*16kB (UME) 39*32kB (UME) 21*64kB (UME) 5*128kB (UM) 1*256kB (U) 0*512kB 0*1024kB 0*2048kB 0*4096kB = 8728kB
[86706.822728] Node 2 Movable: 0*4kB 1*8kB (M) 1*16kB (M) 8*32kB (M) 9*64kB (M) 5*128kB (M) 0*256kB 0*512kB 0*1024kB 1*2048kB (M) 54392*4096kB (M) = 222793176kB
[86706.822733] Node 3 Movable: 0*4kB 1*8kB (M) 0*16kB 1*32kB (M) 1*64kB (M) 1*128kB (M) 0*256kB 0*512kB 0*1024kB 0*2048kB 50936*4096kB (M) = 208634088kB
[86706.822748] Node 0 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[86706.822749] Node 0 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[86706.822750] Node 1 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[86706.822751] Node 1 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[86706.822752] Node 2 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[86706.822753] Node 2 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[86706.822754] Node 3 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[86706.822755] Node 3 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[86706.822755] 49544 total pagecache pages
[86706.822757] 0 pages in swap cache
[86706.822758] Swap cache stats: add 0, delete 0, find 0/0
[86706.822759] Free swap  = 0kB
[86706.822759] Total swap = 0kB
[86706.822760] 147760473 pages RAM
[86706.822760] 0 pages HighMem/MovableOnly
[86706.822761] 286981 pages reserved
[86706.822761] 0 pages hwpoisoned
[86706.822762] Tasks state (memory values in pages):
[86706.822762] [  pid  ]   uid  tgid total_vm      rss pgtables_bytes swapents oom_score_adj name
[86706.823056] [    677]     0   677    40537    26617   344064        0             0 systemd-journal
[86706.823082] [    715]     0   715     6194     1010    81920        0         -1000 systemd-udevd
[86706.823116] [   1371]   101  1371    23272      635    81920        0             0 systemd-timesyn
[86706.823122] [   1372]   108  1372     1707      108    53248        0             0 rpcbind
[86706.823142] [   1395]     0  1395    20624      500    57344        0             0 irqbalance
[86706.823143] [   1396]     0  1396     1741      108    49152        0             0 ndctl
[86706.823146] [   1397]     0  1397    56472      875    86016        0             0 rsyslogd
[86706.823149] [   1398]     0  1398     4866      430    77824        0             0 systemd-logind
[86706.823151] [   1401]   104  1401     2292      536    53248        0          -900 dbus-daemon
[86706.823152] [   1417]     0  1417     2373      767    57344        0             0 dhclient
[86706.823156] [   1422]     0  1422    27611     1936   114688        0             0 unattended-upgr
[86706.823163] [   1428]     0  1428     1419       31    49152        0             0 agetty
[86706.823167] [   1429]     0  1429   722922     7704   524288        0          -999 containerd
[86706.823171] [   1468]     0  1468     3979      715    69632        0         -1000 sshd
[86706.823175] [   1577]   107  1577    96757      918   106496        0         -1000 nslcd
[86706.823182] [   1585]     0  1585     2142      260    61440        0             0 cron
[86706.823184] [   1862]   106  1862     5095      477    81920        0             0 exim4
[86706.823198] [   1924]  1234  1924     5290      349    73728        0             0 systemd
[86706.823205] [   1925]  1234  1925     5933      693    77824        0             0 (sd-pam)
[86706.823229] [   1989]  1234  1989     3354     1237    73728        0             0 tmux: server
[86706.823241] [   2093]  1234  2093     2038      431    53248        0             0 bash
[86706.823250] [   3013]     0  3013     2574      109    57344        0             0 sudo
[86706.823255] [   3037]     0  3037     1430       30    49152        0             0 dmesg
[86706.823260] [   4635]  1234  4635     1414       99    53248        0             0 make
[86706.823262] [   4723]  1234  4723     2007      412    57344        0             0 bash
[86706.823264] [   4797]  1234  4797     1414       62    45056        0             0 make
[86706.823267] [   4801]  1234  4801      598       17    40960        0             0 sh
[86706.823277] [   4802]  1234  4802     1562      525    57344        0             0 watch
[86706.823289] [  37763]  1234 37763     1414      552    45056        0             0 make
[86706.823292] [  21752]  1234 21752     1414      562    49152        0             0 make
[86706.823293] [  21787]  1234 21787      598      174    45056        0             0 sh
[86706.823295] [  21788]  1234 21788     7173     3793    94208        0             0 perf
[86706.823296] [  21789]  1234 21789     1317      171    45056        0             0 tee
[86706.823298] [  21791]  1234 21791 55472500 24473650 196665344        0             0 mg.E.x
[86706.823299] [  22018]  1234 22018     1562      174    49152        0             0 watch
[86706.823301] [  22019]  1234 22019      598      175    40960        0             0 sh
[86706.823302] [  22020]     0 22020     2574      995    61440        0             0 sudo
[86706.823303] [  22021]     0 22021     1662      782    53248        0             0 status
[86706.823305] [  22022]     0 22022     2545      288    65536        0             0 pgrep
[86706.823306] oom-kill:constraint=CONSTRAINT_NONE,nodemask=(null),cpuset=/,mems_allowed=0-3,global_oom,task_memcg=/user.slice/user-1234.slice/session-1.scope,task=mg.E.x,pid=21791,uid=1234
[86706.823511] Out of memory: Killed process 21791 (mg.E.x) total-vm:221890000kB, anon-rss:97890564kB, file-rss:4368kB, shmem-rss:0kB, UID:1234 pgtables:192056kB oom_score_adj:0
[86710.415379] oom_reaper: reaped process 21791 (mg.E.x), now anon-rss:0kB, file-rss:0kB, shmem-rss:0kB
== END ============================
