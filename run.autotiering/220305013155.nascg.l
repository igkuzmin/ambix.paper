== BEGIN ==========================
Sat 05 Mar 2022 07:33:29 AM WET
 # note: 
 # tag: nascg.l.autotiering
 # running nascg (220305013155: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     9000000
 Iterations:                      5
 Number of available threads:    32

bash: Killed
===================================
# started on Sat Mar  5 07:33:29 2022


 Performance counter stats for 'system wide':

         11,923.80 Joules power/energy-ram/                                           
         75,509.26 Joules power/energy-pkg/                                           

     469.695844295 seconds time elapsed

== DMESG ==========================
[57703.542758] oom_reaper: reaped process 35016 (bfs), now anon-rss:0kB, file-rss:0kB, shmem-rss:0kB
[79863.186269] cg.l.x invoked oom-killer: gfp_mask=0x400dc0(GFP_KERNEL_ACCOUNT|__GFP_ZERO), order=0, oom_score_adj=0
[79863.186273] CPU: 2 PID: 14496 Comm: cg.l.x Tainted: G            E     5.9.6-vanila #1
[79863.186274] Hardware name: Supermicro SYS-6019P-WT/X11DDW-L, BIOS 3.2 10/16/2019
[79863.186275] Call Trace:
[79863.186285]  dump_stack+0x6d/0x88
[79863.186289]  dump_header+0x4a/0x1d8
[79863.186291]  oom_kill_process.cold.36+0xb/0x10
[79863.186296]  out_of_memory+0x1a8/0x4d0
[79863.186299]  __alloc_pages_slowpath.constprop.110+0xbd7/0xcc0
[79863.186302]  __alloc_pages_nodemask+0x2de/0x310
[79863.186306]  pte_alloc_one+0x13/0x40
[79863.186311]  __pte_alloc+0x16/0x100
[79863.186313]  handle_mm_fault+0x1524/0x1640
[79863.186317]  exc_page_fault+0x290/0x550
[79863.186321]  ? asm_exc_page_fault+0x8/0x30
[79863.186323]  asm_exc_page_fault+0x1e/0x30
[79863.186326] RIP: 0033:0x7fd0bf1e1925
[79863.186328] Code: 3b 06 00 0f 87 f0 00 00 00 c5 fe 6f 01 c5 fe 6f 49 e0 c5 fe 6f 51 c0 c5 fe 6f 59 a0 48 81 e9 80 00 00 00 48 81 ea 80 00 00 00 <c4> c1 7d 7f 01 c4 c1 7d 7f 49 e0 c4 c1 7d 7f 51 c0 c4 c1 7d 7f 59
[79863.186329] RSP: 002b:00007fadee2e3368 EFLAGS: 00010202
[79863.186331] RAX: 00007fcf37fffdf8 RBX: 00000001133d86b1 RCX: 00007fc9afc70a14
[79863.186332] RDX: 00000000000008c8 RSI: 00007fc9afc7016c RDI: 00007fcf37fffdf8
[79863.186333] RBP: 00000001090d49d4 R08: 000000000000001c R09: 00007fcf38000720
[79863.186333] R10: 00007fb9d3cd8010 R11: 00007fcf3800073c R12: 0000000000683ba9
[79863.186334] R13: 00000001090d477b R14: 0000000000044aa2 R15: 00007fc562d0f010
[79863.186336] Mem-Info:
[79863.186351] active_anon:9010 inactive_anon:22127656 isolated_anon:0
                active_file:8923 inactive_file:5970 isolated_file:0
                unevictable:0 dirty:3 writeback:0
                slab_reclaimable:16275 slab_unreclaimable:39304
                mapped:19337 shmem:31207 pagetables:43502 bounce:0
                free:110289251 free_pcp:2998 free_cma:0
[79863.186354] Node 0 active_anon:5604kB inactive_anon:15081172kB active_file:2092kB inactive_file:0kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:7076kB dirty:0kB writeback:0kB shmem:16888kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 4096kB writeback_tmp:0kB kernel_stack:6720kB all_unreclaimable? yes
[79863.186357] Node 1 active_anon:88kB inactive_anon:15845472kB active_file:124kB inactive_file:0kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:4kB dirty:0kB writeback:0kB shmem:724kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 0kB writeback_tmp:0kB kernel_stack:4768kB all_unreclaimable? yes
[79863.186359] Node 2 active_anon:30344kB inactive_anon:28867808kB active_file:21312kB inactive_file:19120kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:61656kB dirty:12kB writeback:0kB shmem:107216kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 0kB writeback_tmp:0kB kernel_stack:0kB all_unreclaimable? no
[79863.186361] Node 3 active_anon:4kB inactive_anon:28716172kB active_file:12164kB inactive_file:5736kB unevictable:0kB isolated(anon):0kB isolated(file):0kB mapped:8612kB dirty:0kB writeback:0kB shmem:0kB shmem_thp: 0kB shmem_pmdmapped: 0kB anon_thp: 0kB writeback_tmp:0kB kernel_stack:0kB all_unreclaimable? no
[79863.186363] Node 0 DMA free:15888kB min:0kB low:12kB high:24kB reserved_highatomic:0KB active_anon:0kB inactive_anon:0kB active_file:0kB inactive_file:0kB unevictable:0kB writepending:0kB present:15972kB managed:15888kB mlocked:0kB pagetables:0kB bounce:0kB free_pcp:0kB local_pcp:0kB free_cma:0kB
[79863.186366] lowmem_reserve[]: 0 1621 31826 31826 31826
[79863.186369] Node 0 DMA32 free:120900kB min:88kB low:1748kB high:3408kB reserved_highatomic:0KB active_anon:0kB inactive_anon:1534840kB active_file:0kB inactive_file:0kB unevictable:0kB writepending:0kB present:1726208kB managed:1660672kB mlocked:0kB pagetables:3692kB bounce:0kB free_pcp:0kB local_pcp:0kB free_cma:0kB
[79863.186372] lowmem_reserve[]: 0 0 30205 30205 30205
[79863.186374] Node 0 Normal free:6372kB min:13984kB low:44912kB high:75840kB reserved_highatomic:0KB active_anon:5604kB inactive_anon:13546332kB active_file:1988kB inactive_file:0kB unevictable:0kB writepending:0kB present:31457280kB managed:30929996kB mlocked:0kB pagetables:90308kB bounce:0kB free_pcp:8024kB local_pcp:248kB free_cma:0kB
[79863.186377] lowmem_reserve[]: 0 0 0 0 0
[79863.186379] Node 1 Normal free:6724kB min:10000kB low:42996kB high:75992kB reserved_highatomic:0KB active_anon:88kB inactive_anon:15845472kB active_file:124kB inactive_file:0kB unevictable:0kB writepending:0kB present:33554432kB managed:32999412kB mlocked:0kB pagetables:80008kB bounce:0kB free_pcp:0kB local_pcp:0kB free_cma:0kB
[79863.186382] lowmem_reserve[]: 0 0 0 0 0
[79863.186383] Node 2 Movable free:220401248kB min:14380kB low:276524kB high:538668kB reserved_highatomic:0KB active_anon:30344kB inactive_anon:28867808kB active_file:21312kB inactive_file:19120kB unevictable:0kB writepending:12kB present:262144000kB managed:262144000kB mlocked:0kB pagetables:0kB bounce:0kB free_pcp:1884kB local_pcp:196kB free_cma:0kB
[79863.186386] lowmem_reserve[]: 0 0 0 0 0
[79863.186389] Node 3 Movable free:220605872kB min:14380kB low:276524kB high:538668kB reserved_highatomic:0KB active_anon:4kB inactive_anon:28716172kB active_file:12164kB inactive_file:5736kB unevictable:0kB writepending:0kB present:262144000kB managed:262144000kB mlocked:0kB pagetables:0kB bounce:0kB free_pcp:2084kB local_pcp:0kB free_cma:0kB
[79863.186391] lowmem_reserve[]: 0 0 0 0 0
[79863.186393] Node 0 DMA: 0*4kB 0*8kB 1*16kB (U) 0*32kB 2*64kB (U) 1*128kB (U) 1*256kB (U) 0*512kB 1*1024kB (U) 1*2048kB (M) 3*4096kB (M) = 15888kB
[79863.186398] Node 0 DMA32: 211*4kB (UME) 179*8kB (UME) 122*16kB (UME) 92*32kB (UME) 43*64kB (UME) 21*128kB (UME) 5*256kB (UME) 3*512kB (UE) 1*1024kB (E) 1*2048kB (M) 25*4096kB (UM) = 120900kB
[79863.186404] Node 0 Normal: 211*4kB (UME) 119*8kB (UME) 41*16kB (UME) 32*32kB (UM) 3*64kB (U) 0*128kB 1*256kB (U) 0*512kB 0*1024kB 0*2048kB 0*4096kB = 3924kB
[79863.186408] Node 1 Normal: 697*4kB (UME) 226*8kB (UM) 194*16kB (UM) 82*32kB (UM) 6*64kB (U) 0*128kB 0*256kB 0*512kB 0*1024kB 0*2048kB 0*4096kB = 10708kB
[79863.186413] Node 2 Movable: 0*4kB 0*8kB 0*16kB 0*32kB 0*64kB 1*128kB (M) 1*256kB (M) 0*512kB 0*1024kB 1*2048kB (M) 53808*4096kB (M) = 220400000kB
[79863.186417] Node 3 Movable: 0*4kB 0*8kB 1*16kB (M) 0*32kB 1*64kB (M) 1*128kB (M) 1*256kB (M) 0*512kB 1*1024kB (M) 0*2048kB 53858*4096kB (M) = 220603856kB
[79863.186424] Node 0 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[79863.186425] Node 0 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[79863.186426] Node 1 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[79863.186426] Node 1 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[79863.186427] Node 2 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[79863.186428] Node 2 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[79863.186429] Node 3 hugepages_total=0 hugepages_free=0 hugepages_surp=0 hugepages_size=1048576kB
[79863.186429] Node 3 hugepages_total=6250 hugepages_free=6250 hugepages_surp=0 hugepages_size=2048kB
[79863.186430] 46025 total pagecache pages
[79863.186432] 0 pages in swap cache
[79863.186432] Swap cache stats: add 0, delete 0, find 0/0
[79863.186433] Free swap  = 0kB
[79863.186433] Total swap = 0kB
[79863.186434] 147760473 pages RAM
[79863.186434] 0 pages HighMem/MovableOnly
[79863.186435] 286981 pages reserved
[79863.186435] 0 pages hwpoisoned
[79863.186436] Tasks state (memory values in pages):
[79863.186436] [  pid  ]   uid  tgid total_vm      rss pgtables_bytes swapents oom_score_adj name
[79863.186471] [    677]     0   677    38456    24084   323584        0             0 systemd-journal
[79863.186474] [    715]     0   715     6194     1010    81920        0         -1000 systemd-udevd
[79863.186478] [   1371]   101  1371    23272      635    81920        0             0 systemd-timesyn
[79863.186479] [   1372]   108  1372     1707      108    53248        0             0 rpcbind
[79863.186481] [   1395]     0  1395    20624      500    57344        0             0 irqbalance
[79863.186482] [   1396]     0  1396     1741      108    49152        0             0 ndctl
[79863.186483] [   1397]     0  1397    56472      873    86016        0             0 rsyslogd
[79863.186485] [   1398]     0  1398     4866      430    77824        0             0 systemd-logind
[79863.186486] [   1401]   104  1401     2292      534    53248        0          -900 dbus-daemon
[79863.186488] [   1417]     0  1417     2373      766    57344        0             0 dhclient
[79863.186489] [   1422]     0  1422    27611     1936   114688        0             0 unattended-upgr
[79863.186490] [   1428]     0  1428     1419       31    49152        0             0 agetty
[79863.186492] [   1429]     0  1429   722922     7691   524288        0          -999 containerd
[79863.186493] [   1468]     0  1468     3979      715    69632        0         -1000 sshd
[79863.186495] [   1577]   107  1577    96757      918   106496        0         -1000 nslcd
[79863.186496] [   1585]     0  1585     2142      260    61440        0             0 cron
[79863.186498] [   1862]   106  1862     5095      477    81920        0             0 exim4
[79863.186499] [   1924]  1234  1924     5290      349    73728        0             0 systemd
[79863.186501] [   1925]  1234  1925     5933      693    77824        0             0 (sd-pam)
[79863.186502] [   1989]  1234  1989     3353     1236    73728        0             0 tmux: server
[79863.186504] [   2093]  1234  2093     2038      431    53248        0             0 bash
[79863.186505] [   3013]     0  3013     2574      109    57344        0             0 sudo
[79863.186506] [   3037]     0  3037     1430       30    49152        0             0 dmesg
[79863.186508] [   4635]  1234  4635     1414       99    53248        0             0 make
[79863.186509] [   4723]  1234  4723     2007      412    57344        0             0 bash
[79863.186511] [   4797]  1234  4797     1414       62    45056        0             0 make
[79863.186512] [   4801]  1234  4801      598       17    40960        0             0 sh
[79863.186514] [   4802]  1234  4802     1562      525    57344        0             0 watch
[79863.186516] [  37763]  1234 37763     1414      552    45056        0             0 make
[79863.186518] [  14436]  1234 14436     1414      583    53248        0             0 make
[79863.186519] [  14469]  1234 14469      598      190    45056        0             0 sh
[79863.186521] [  14470]  1234 14470     7173     3712    98304        0             0 perf
[79863.186522] [  14471]  1234 14471     1317      187    49152        0             0 tee
[79863.186524] [  14472]  1234 14472 36525624 22088410 177668096        0             0 cg.l.x
[79863.186525] [  15356]     0 15356     4278     2026    73728        0             0 sshd
[79863.186527] [  15357]   105 15357     3979     1243    73728        0             0 sshd
[79863.186528] oom-kill:constraint=CONSTRAINT_NONE,nodemask=(null),cpuset=/,mems_allowed=0-3,global_oom,task_memcg=/user.slice/user-1234.slice/session-1.scope,task=cg.l.x,pid=14472,uid=1234
[79863.186565] Out of memory: Killed process 14472 (cg.l.x) total-vm:146102496kB, anon-rss:88350212kB, file-rss:3428kB, shmem-rss:0kB, UID:1234 pgtables:173504kB oom_score_adj:0
[79867.565836] oom_reaper: reaped process 14472 (cg.l.x), now anon-rss:0kB, file-rss:0kB, shmem-rss:0kB
== END ============================
