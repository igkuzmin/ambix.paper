== BEGIN ==========================
Fri 04 Mar 2022 09:52:41 AM WET
 # note: 
 # tag: nascg.s.autotiering
 # running nascg (220304094711: Size s)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         172.960 seconds

   iteration           ||r||                 zeta
        1       0.21274304899572E-11   999.9996857982868
        2       0.13482825794748E-14    57.8675766650496
        3       0.13442037089998E-14    58.2941206350794
        4       0.13463272028046E-14    58.6783531949444
        5       0.13425980520576E-14    59.0239484906585
        6       0.13347388469874E-14    59.3343718346762
        7       0.13232352500913E-14    59.6128667120506
        8       0.13140759323747E-14    59.8624490951615
        9       0.13007739003835E-14    60.0859075092864
       10       0.12902069093753E-14    60.2858074951209
       11       0.12793438026783E-14    60.4644993187786
       12       0.12692236684837E-14    60.6241279736680
       13       0.12626330247349E-14    60.7666446959977
       14       0.12539863485121E-14    60.8938193721789
       15       0.12484172359187E-14    61.0072533517981
       16       0.12425994638706E-14    61.1083922942580
       17       0.12372222880212E-14    61.1985387720229
       18       0.12334583539397E-14    61.2788644311378
       19       0.12276642064480E-14    61.3504215716461
       20       0.12242671715064E-14    61.4141540599927
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                  3145.04
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   552.60
 Mop/s/thread    =                    17.27
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              09 Feb 2022

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Fri Mar  4 09:52:41 2022


 Performance counter stats for 'system wide':

         82,630.80 Joules power/energy-ram/                                           
        491,553.71 Joules power/energy-pkg/                                           

    3320.935928980 seconds time elapsed

== DMESG ==========================
[   57.795450] Policy zone: Normal
== END ============================
