AMBIX.SRC := \
	$(wildcard run.autonuma/*) \
	$(wildcard run.base/*) \
	$(wildcard run.mem/*) \
	$(wildcard run.nimble/*) \
	$(wildcard run.ambix/*) \
	$(wildcard mmarques.*/*) \
	$(wildcard run.hemem/*) \
	$(wildcard run.hemem-lru/*) \
	# \
	$(wildcard run.autotiering/*) \
	$(wildcard run.autotiering_onenode/*) \
	$(wildcard run.memos/*) \
	$(wildcard run.kmod/*) \
	$(wildcard mmarques.ambix/*) \
	$(wildcard run.*/*) \
	$(wildcard off/run.nimble_nTHP/*)  \

OFF.SRC := $(wildcard off/*/*)

GNUPLOT.tags := 'Ambix AutoNUMA MemM HeMem HeMem(LRU)'
#FAKE.SRC := missed.run

PDF ?=  throughput.pdf energy.mem.pdf throughput_small.pdf energy_small.mem.pdf throughput_bfs.pdf

all: $(PDF) 

#PARSEARGS := --debug --src
energy.csv: parse.pl ${AMBIX.SRC} ${FAKE.SRC} Makefile
	@echo " # generating $@" #|tee -a parse.log
	@perl $^ >$@ ${PARSEARGS} --type energy  #2>>parse.log

throughput.csv: parse.pl ${AMBIX.SRC} ${FAKE.SRC} Makefile
	@echo " # generating $@" #|tee -a parse.log
	@perl $^ ${PARSEARGS} --type throughput --filter-out '.*\.(s|450)\..*' --filter '(nas|AVG).*' --avg > $@ #2>>parse.log

throughput_small.csv: parse.pl ${AMBIX.SRC} ${FAKE.SRC} Makefile
	@echo " # generating $@" #|tee -a parse.log
	@perl $^ ${PARSEARGS} --type throughput --filter '.*\.(s|450)\..*' --avg > $@ #2>>parse.log

energy.mem.pdf: energy.gp pallete.gp throughput.csv
	@echo " # generationg $@"
	@gnuplot pallete.gp -e "input_data='throughput.csv'; set output 'energy.mem.ps'; tags=${GNUPLOT.tags};" $<
	@epstopdf energy.mem.ps
	@rm energy.mem.ps

energy_small.mem.pdf: energy.gp pallete.gp throughput_small.csv
	@echo " # generationg $@"
	@gnuplot pallete.gp -e "input_data='throughput_small.csv'; set output 'energy_small.mem.ps'; tags=${GNUPLOT.tags};" $<
	@epstopdf energy_small.mem.ps
	@rm energy_small.mem.ps

throughput_bfs.csv: parse.pl ${AMBIX.SRC} ${FAKE.SRC} Makefile
	@echo " # generating $@"
	@perl $^ --type throughput --filter 'bfs.*' --avga >$@ 

energy_bfs.mem.pdf: energy.gp pallete.gp throughput_bfs.csv
	@echo " # generationg $@"
	@gnuplot pallete.gp -e "input_data='throughput_bfs.csv'; set output 'energy_bfs.mem.ps'; tags=${GNUPLOT.tags};" $<
	@epstopdf energy_bfs.mem.ps
	@rm energy_bfs.mem.ps

#energy_bfs.pkg.pdf: energy.gp pallete.gp  throughput_bfs.csv
#	@echo " # generationg $@"
#	@gnuplot pallete.gp -e 'input_data="throughput_bfs.csv"; set output "energy_bfs.pkg.ps"; col=4' $<
#	@epstopdf energy_bfs.pkg.ps
#	@rm energy_bfs.pkg.ps

throughput_bfs.pdf: throughput.gp pallete.gp throughput_bfs.csv
	@echo " # generating $@"
	@gnuplot -e "input_data='throughput_bfs.csv'; set output 'throughput_bfs.ps'; tags=${GNUPLOT.tags};" pallete.gp $< 
	@epstopdf throughput_bfs.ps
	@rm throughput_bfs.ps

energy.pkg.pdf: energy.gp pallete.gp throughput.csv
	@echo " # generationg $@"
	@gnuplot pallete.gp -e "input_data='throughput.csv'; set output 'energy.pkg.ps'; col=4; tags=${GNUPLOT.tags};" $<
	@epstopdf energy.pkg.ps
	@rm energy.pkg.ps

throughput.pdf: throughput.gp pallete.gp throughput.csv
	@echo " # generating $@"
	@gnuplot -e "input_data='throughput.csv'; set output 'throughput.ps'; tags=${GNUPLOT.tags};" pallete.gp $< 
	@epstopdf throughput.ps
	@rm throughput.ps

throughput_small.pdf: throughput.gp pallete.gp throughput_small.csv
	@echo " # generating $@"
	@gnuplot -e "input_data='throughput_small.csv'; set output 'throughput_small.ps'; wd=6; hd=4; tags=${GNUPLOT.tags};" pallete.gp $< 
	@epstopdf throughput_small.ps
	@rm throughput_small.ps


wiki/tests.md: parse.pl ${AMBIX.SRC} ${OFF.SRC} wiki.dir
	@echo " # updating $@"
	@perl $^ --type list >$@

list:
	@perl parse.pl ${AMBIX.SRC} --type list --src 2>list.csv |less

list.csv: parse.pl
	@echo " # generating $@"
	@perl parse.pl ${AMBIX.SRC} --src 2>$@

THRESHOLD ?= 0.05
check: parse.pl ${AMBIX.SRC}
	@perl $^  --type check --threshold ${THRESHOLD} | sort -r

FLT ?= .
lst: parse.pl ${AMBIX.SRC}
	@perl $^ --src 2>&1 |grep "${FLT}" |sort -t';'|sort -s -g -t';' -k2  |column -s';' -t

WIKI.PNGs := \
   	wiki/throughput.png wiki/throughput_small.png wiki/throughput_bfs.png \
	wiki/energy.mem.png wiki/energy_small.mem.png wiki/energy_bfs.mem.png

${WIKI.PNGs}: wiki/%.png: %.pdf wiki.dir
	@gs -dSAFER -dBATCH -dNOPAUSE -dEPSCrop -r600 -sDEVICE=png16m -sOutputFile=$@ $<

wiki: wiki/tests.md ${WIKI.PNGs} 

wiki.dir:
	@mkdir -p wiki

docker.push:
	@docker login -u igkuzmin
	@docker pull igkuzmin/gnuplot

FILES ?= energy.mem.pdf throughput.pdf throughput_small.pdf
push: ${FILES}
	@echo " # pushing artifacts"
	@for f in ${FILES}; do \
		curl -s -u igkuzmin:tVbJ5fGQe7FMR2nasMuG -X POST \
	   		https://api.bitbucket.org/2.0/repositories/igkuzmin/ambix.paper/downloads -F $$f=@$$f; \
	 done;

view: ${PDF}
	@nohup zathura ${PDF} &>/dev/null &

clean:
	@rm -f *.csv *.ps *.pdf wiki/tests.md *.png  parse.log

%.html: %.md
	@echo " # generating $@"
	@md2html -f --github $< >$@

.PHONY: clean wiki docker.push view list push wiki.dir check lst
