#!/bin/perl
use Getopt::Long;
use Data::Dumper qw(Dumper);


our $SRC;
our $THROUGHPUT;
our $TYPE = 'energy';
our $FILTER;
our $FILTER_OUT;
our $AVG;
our $DEBUG;
our $THRESHOLD = 0.05;

GetOptions(
    'type=s' => \our $TYPE,
    'src!'   => \our $SRC,
    'avg!'   => \our $AVG,
    'avga!'   => \our $AVGA,
    'filter=s' => \our $FILTER,
    'debug!' => \our $DEBUG,
    'threshold=f' => \our $THRESHOLD,
    'filter-out=s' => \our $FILTER_OUT);

sub stats {
    my @vals = sort { $a <=> $b } @_;
    my $n = scalar @vals;
    return 0, 0 if $n == 0;

    my $mean = 0;
    for my $v (@vals) {
        $mean += $v;
    }
    $mean /= $n;

    my $dispersion = 0;
    for my $v (@vals) {
        $dispersion += ($v - $mean)**2;
    }

    $dispersion /= $n;

    my $rd = $dispersion**(1/2) / $mean;

    return $mean, $rd;
}

sub merge {
    my @vals = sort { $a <=> $b } @_;
    # avg  -- my $sum = 0;
    # avg  -- my $n = 0;
    # avg  -- for my $x (@vals) {
    # avg  --     $sum += $x;
    # avg  --     $n++;
    # avg  -- }
    my $n = scalar @vals;
    return "n/a" if $n == 0;

    return sprintf "%.2f", $vals[($n - 1)/2];
    # avg -- return sprintf "%.2f", $sum / $n;
    #print STDERR Dumper \@vals;
}

sub clean {
    %rec = ();
    $rec{tag} = $rec{energy_pkg} = $rec{energy_ram} =
        $rec{h_exch_pages} = $rec{h_slow2fast} = $rec{h_fast2slow} =
            $rec{exch_pages} = $rec{slow2fast} = $rec{fast2slow} = 'n/a';

    $rec{note} = $rec{time_sec} = $rec{options} = '';
}

sub begin_record {
    clean;
    my $fd = fileno ${^LAST_FH};
    $rec{source} = readlink("/proc/$$/fd/$fd");
}

sub v {
    $n = shift @_;
    if (exists $rec{$n}) {
        return $rec{$n};
    }
    return '';
}

sub end_record {
    #printf("%10s | %10d | %15s | %15s | %10d | %10d | %10d |\n",
    #    $tag, $time, $energy_ram, $energy_pkg, $exch_pages, $slow2fast, $fast2slow);
    #$RESULTS[$tag]['energy_ram'] = energy_ram;
    #undef $tag, $energy_ram, $energy_pkg, $time, $exch_pages, $slow2fast, $fast2slow;

    $tag = v(tag);
    if (v(note)) {
        $tag .= ' ('.v(note).')';
    }

    if (!v('time')) {
        clean;
        return;
    }

    printf STDERR ("%s;%.4f;%s;%s;%s;%s %s\n", $tag, v('time'), v(energy_ram), v(energy_pkg), v(time_sec), v(source), v(options)) if $SRC;

    if (v(options) eq 'SKIP') {
        clean;
        return;
    }

    for my $key (keys %rec) {
        my $nv = $rec{$key};
        next if not $nv or $nv eq "n/a";
        push @{$RES{$tag}{$key}}, $nv;
    }

    clean;
}

sub main {
    printf STDERR "tag;throughput/time;energy_ram;energy_pkg;time_sec;source;\n" if $SRC;
    while(<>) {
        begin_record if /== BEGIN ==========================/;
        if (/Joules power/) { $_ =~ s/,//g; }
        $rec{energy_ram} = $1 if /([\d.,]+) Joules power\/energy-ram/;
        $rec{energy_pkg} = $1 if /([\d.,]+) Joules power\/energy-pkg/;
        $rec{time}       = $1 if /Average Time:\s+([\d.]+)/;
        $rec{time}       = $1 if /Mop\/s total     =\s+([\d.]+)/;
        $rec{exch_pages} = $1 if /ExchangePagesBase_nr_base_pages\s+(\d+)/;
        $rec{slow2fast}  = $1 if /Slow2FastBasePageMigrations_nr_base_pages (\d+)/;
        $rec{fast2slow}  = $1 if /Fast2SlowBasePageMigrations_nr_base_pages (\d+)/;
        $rec{h_exch_pages} = $1 if /ExchangePagesHuge_nr_base_pages\s+(\d+)/;
        $rec{h_slow2fast}  = $1 if /Slow2FastHugePageMigrations_nr_base_pages (\d+)/;
        $rec{h_fast2slow}  = $1 if /Fast2SlowHugePageMigrations_nr_base_pages (\d+)/;
        $rec{tag}        = $1 if / # tag: (.+)$/;
        $rec{note}       = $1 if / # note: (.+)$/;
        $rec{options}    = $1 if / # options: (.+)$/;
        $rec{time_sec}   = $1 if / Time in seconds =\s+([\d.]+)/;
        $rec{time_sec}   = $1/1000 if /real time\(ms\): (\d+),/;
        $rec{time_sec}   = $1 if /([\d.]+) seconds time elapsed/;
        end_record if /== END ============================/;
    }
}

sub list_tests
{
    my %tsts;
    my %tags;
    for my $key (sort keys %RES) {
        ($tname = $1, $tag = $2) if $key =~ /^([\w.]+)\.(.+)$/;
        $tsts{$tname} = 1;
        $tags{$tag} = 1;
    }
    printf "|%20s|%s|\n", 'test',  join("|", map { sprintf "%20s", $_ } sort keys %tags);
    printf "|%20s|%s|\n", '-'x20 , join('|', map { '-'x20 } sort keys %tags);
    for my $tst (sort keys %tsts) {
        printf("|%20s|", $tst);
        for my $tg (sort keys %tags) {
            if (exists $RES{"$tst.$tg"}) {
                my $l = ':plus: ';
                my $n_time = scalar(@{$RES{"$tst.$tg"}{time}});
                my $n_ram = scalar(@{$RES{"$tst.$tg"}{energy_ram}});
                my $n_pkg = scalar(@{$RES{"$tst.$tg"}{energy_pkg}});
                if ($n_time == $n_ram and $n_ram == $n_pkg) {
                    $l .= "($n_time)" if ($n_time > 1);
                }
                else {
                    $l .= '(';
                    if (exists $RES{"$tst.$tg"}{'time'}) {
                        #print Dumper \$RES{"$tst.$tg"}{time};
                        $l .= " prf:".scalar(@{$RES{"$tst.$tg"}{time}});
                    }
                    if (exists $RES{"$tst.$tg"}{energy_ram}) {
                        $l .= " ram:".scalar(@{$RES{"$tst.$tg"}{energy_ram}});
                    }
                    if (exists $RES{"$tst.$tg"}{energy_pkg}) {
                        $l .= " pkg:".scalar(@{$RES{"$tst.$tg"}{energy_pkg}});
                    }
                    $l .= ' )';
                }
                printf("%20s|", $l);
            }
            else { printf("%20s|", ':minus:');}
        }
        printf "\n";
    }
}

#print Dumper \%RES;
#sub energy_csv
#{
#    print "tag;time;ram;pkg;duration\n";
#    my $last_set_name = '';
#    for my $key (sort keys %RES) {
#        my ($tname, $tag, $opt);
#        ($tname = $1, $opt = $2, $tag = $3) if $key =~ /^([\w.]+)\.([\w.]+)\.(.+)$/;
#        my $tst = "$tname.$opt";
#
#        #print "$key\n";
#        next if $tag eq "base";
#        printf STDERR "Skip small test $key\n" and next if $key =~ /.*\.(s|450)\..*/;
#        if (not exists $RES{"$tst.base"}) {
#            print STDERR "Can't find base value for $tst (skipping)\n";
#            next;
#        }
#
#        %base = %{$RES{"$tst.base"}};
#        #print "--$tst.base--\n";
#        #if (exists $RES{"$tst.base"}) {
#        #    print "HERE\n";
#        #}
#
#        #print Dumper \%base;
#        #print Dumper \%values;
#
#        my %values = %{$RES{$key}};
#
#
#        my $time = merge(@{$values{time}});
#        my $ram =  merge(@{$values{energy_ram}});
#        my $pkg =  merge(@{$values{energy_pkg}});
#
#        my @missed = ();
#        push @missed, "time" if $time eq 'n/a';
#        push @missed, "energy-ram" if $ram eq 'n/a';
#        push @missed, "energy-pkg" if $pkg eq 'n/a';
#        if (@missed) {
#            printf STDERR "Skip $key: required value(s) absent - (".join(', ', @missed).")\n";
#            next;
#        }
#
#        $time /= merge(@{$base{time}});
#        $ram  /= merge(@{$base{energy_ram}});
#        $pkg  /= merge(@{$base{energy_pkg}});
#
#        if ($tname =~ "bfs\..+") { # bfs tests shows time, while othere ops
#            $time = 1/$time;
#        }
#
#        my $time_sec = merge(@{$values{time_sec}});
#
#        my $set_name = map_name($key);
#        if ($last_set_name ne $set_name) {
#            print "\n\n" if $last_set_name;
#            print "# $set_name\n";
#            $last_set_name = $set_name;
#        }
#
#        print "$tag;$time;$ram;$pkg;$time_sec\n";
#        #print map_name($key).";$time;$ram;$pkg;$time_sec;$tag\n";
#    }
#}
#
sub calc_AVG
{
    #my (%tags, @tsts, $sfx);# = $@;
    my @tags = @{ + shift };
    my @tsts = @{ + shift};
    my $sfx = shift;

    for my $tag (@tags) {
        my $time_mult = 1;
        my $ram_mult = 1;
        my $pkg_mult = 1;
        my $num = 0;
        for my $tst (@tsts) {
            $num += 1;
            my %values = %{$RES{"$tst.$tag"}};
            $time_mult *= merge(@{$values{time}});
            $ram_mult  *= merge(@{$values{energy_ram}});
            $pkg_mult  *= merge(@{$values{energy_pkg}});

        }
        @{$RES{"ALLAVG_$sfx.$tag"}{time}}       = ($time_mult ** (1/$num));
        @{$RES{"ALLAVG_$sfx.$tag"}{energy_ram}} = ($ram_mult  ** (1/$num));
        @{$RES{"ALLAVG_$sfx.$tag"}{energy_pkg}} = ($pkg_mult  ** (1/$num));

        #print Dumper \%{$RES{"AVG.s.$tag"}} if $tag eq 'memos';
        #printf STDERR "$tag: $time_mult, $ram_mult, $pkg_mult ---\n" if $tag eq 'memos';
        #$x = @{$RES{"ALLAVG_$sfx.$tag"}{time}};
        #print STDERR "HERE: $sfx.$tag: $time_mult ** 1/$num = $x\n" if $tag eq "nimble";
    }
}

sub throughput_csv
{
    my %tsts;
    my %tags;
    for my $key (sort keys %RES) {
        (not $DEBUG or printf STDERR "Skip '$key' (by filter)\n")     and next if $FILTER and not $key =~ /$FILTER/;
        (not $DEBUG or printf STDERR "Skip '$key' (by filter-out)\n") and next if $FILTER_OUT and $key =~ /$FILTER_OUT/;

        ($tname = $1, $tag = $2) if $key =~ /^([\w.]+)\.(.+)$/;
        $tsts{$tname} = 1;
        $tags{$tag} = 1;
    }

    if ($AVG) {
        my @tk = keys %tags;
        my @extra_tags;


        my @tt = grep {/.*\.(l|g29\.n50|600).*/} keys %tsts;
        if (@tt) {
            calc_AVG(\@tk, \@tt, 'l');
            push @extra_tags, "ALLAVG_l";
        }

        @tt = grep {/.*\.(m|g28\.n50|500).*/} keys %tsts;
        if (@tt) {
            calc_AVG(\@tk, \@tt, 'm');
            push @extra_tags, "ALLAVG_m";
        }

        @tt = grep {/.*\.(s|g27\.n50|450).*/} keys %tsts;
        if (@tt) {
            calc_AVG(\@tk, \@tt, 's');
            push @extra_tags, "ALLAVG_s";
        }

        for my $name (@extra_tags) {
            $tsts{$name} = 1;
        }
    }

    if ($AVGA) {
        my @tk = keys %tags;
        @tt = keys %tsts;
        calc_AVG(\@tk, \@tt, '');
        $tsts{"ALLAVG_"} = 1;
    }

    for my $tg (sort keys %tags) {
        next if $tg eq 'base';
        printf("# %s\n", map_tag($tg));
        for my $tst (sort {$b cmp $a} keys %tsts) {
            if (not exists $RES{"$tst.base"}) {
                print STDERR "Can't find base value for $tst (skipping)\n";
                next;
            }
            my %base = %{$RES{"$tst.base"}};

            printf map_name("$tst.$tg").";";
            my %values = %{$RES{"$tst.$tg"}};

            my $time = merge(@{$values{time}});
            my $ram =  merge(@{$values{energy_ram}});
            my $pkg =  merge(@{$values{energy_pkg}});

            $time /= merge(@{$base{time}})             if not $time eq 'n/a';
            $ram  = merge(@{$base{energy_ram}}) / $ram if $ram != 0 and not $ram eq 'n/a';
            $pkg  = merge(@{$base{energy_pkg}}) / $pkg if $pkg != 0 and not $pkg eq 'n/a';

            $time = 1/$time if "$tst" =~ "bfs\..+" and not $time eq 'n/a'; # bfs tests shows time, while othere ops
            $time = 1/$time if "$tst" eq "ALLAVG_" and not $time eq 'n/a' and $time;
            #if ($time eq 'n/a') { print ';'; } else { printf "%.2f;", $time / merge(@{$base{time}});}
            #if ($ram eq 'n/a')  { print ';'; } else { printf "%.2f;", $ram  / merge(@{$base{energy_ram}}); }
            #if ($pkg eq 'n/a')  { print ';'; } else { printf "%.2f;", $pkg  / merge(@{$base{energy_pkg}}); }

            printf "%.2f;%.2f;%.2f\n", $time, $ram, $pkg;
        }
        printf "\n\n";
    }
}

sub check_values {
    for my $key (sort keys %RES) {
        my %rec = %{$RES{$key}};
        my ($mean, $rd) = stats(@{$rec{time}});
        next if $rd < $THRESHOLD;
        printf "%.2f %20s [%s]\n", $rd, $key, join(',', sort { $a <=> $b } @{$rec{time}});
    }
}

#sub compute_avg {
#    for my $key (sort keys %RES) {
#        ($tname = $1, $tag = $2) if $key =~ /^([\w.]+)\.(.+)$/;
#        $tsts{$tname} = 1;
#        $tags{$tag} = 1;
#    }
#
#    for my $tg (sort keys %tags) {
#        next if $tg eq 'base';
#        printf("# $tg\n");
#        for my $tst (sort {$b cmp $a} keys %tsts) {
#            if (not exists $RES{"$tst.base"}) {
#                print STDERR "Can't find base value for $tst (skipping)\n";
#                next;
#}

sub map_name {
    my ($key) = @_;
    ($name = $1, $tag = $3) if $key =~ /^([\w.]+)\.(.+)$/;
    $name = $key if not $name;
    return "BFS_s" if $name eq "bfs.g27.n50";
    return "BFS_m" if $name eq "bfs.g28.n50";
    return "BFS_l" if $name eq "bfs.g29.n50";
    return "BT_s" if $name eq "nasbt.450";
    return "BT_m" if $name eq "nasbt.512";
    return "BT_l" if $name eq "nasbt.600";
    return "CG_s" if $name eq "nascg.s";
    return "CG_m" if $name eq "nascg.m";
    return "CG_l" if $name eq "nascg.l";
    return "FT_s" if $name eq "nasft.s";
    return "FT_m" if $name eq "nasft.m";
    return "FT_l" if $name eq "nasft.l";
    return "MG_s" if $name eq "nasmg.s";
    return "MG_m" if $name eq "nasmg.m";
    return "MG_l" if $name eq "nasmg.l";
    return $name;
}

sub map_tag {
    my ($key) = @_;
    return "AutoNUMA" if $key eq 'autonuma';
    return "AutoTiering" if $key eq 'autotiering';
    return "AutoTiering(OneNode)" if $key eq 'autotiering_onenode';
    return "MemM" if $key eq 'mem';
    return "Memos" if $key eq 'memos';
    return "Ambix" if $key eq 'ambix';
    return "Nimble" if $key eq 'nimble';
    return "HeMem" if $key eq 'hemem';
    return "HeMem(LRU)" if $key eq 'hemem-lru';
    return "Nimble(noTHP)" if $key eq 'nimble[nTHP]';
    return $key;
}

main;
#energy_csv if $TYPE eq 'energy';
check_values if $TYPE eq 'check';
throughput_csv if $TYPE eq 'throughput';
list_tests if $TYPE eq 'list';
