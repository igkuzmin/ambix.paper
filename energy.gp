if (!exists('wd')) wd=12
if (!exists('hd')) hd=5
if (!exists('tags')) tags = 'Ambix AutoNUMA MemM'

set terminal postscript eps enhanced color solid size wd,hd

if (!exists('col')) col=3
if (!exists('input_data')) input_data='throughput.csv'

#if (col == 3) set title "Energy Consumption (Memory)"
#if (col == 4) set title "Energy Consumption (Package)"


set datafile separator ';'
set grid ytics

set ylabel "Relative energy savings (vs ADM-default)"
#set xlabel 'Workload'


#set xtics rotate by -25
set style data histograms
set style histogram clustered gap 2
set style fill solid
set boxwidth .95
set key bmargin center horizontal

#set title "Energy"
unset xrange
plot  1 title "ADM-default (baseline)" lc rgb "#fb3d3d" lw 2, \
     for [i in tags] input_data using col:xtic(1) index i title i

# vim: ft=gnuplot
