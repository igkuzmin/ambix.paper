== BEGIN ==========================
Mon 08 Nov 2021 07:55:17 AM WET
 # note: 
 # tag: nasmg.s.mem
 # running nasmg (211108075215: Size s)
== BEFORE =========================
R_DRAM: 5968670
W_DRAM: 2216052
R_OPT: 2279526
W_OPT: 482255


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1024x1024x1024  (class D)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:          12.850 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 VERIFICATION FAILED
 L2 Norm is              0.1590005169528E-09
 The correct L2 Norm is  0.1583275060440E-09


 MG Benchmark Completed.
 Class           =                        D
 Size            =           1024x1024x1024
 Iterations      =                       50
 Time in seconds =                   282.69
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                 11015.09
 Mop/s/thread    =                   344.22
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 5968670
W_DRAM: 2216052
R_OPT: 2279526
W_OPT: 482255
===================================
# started on Mon Nov  8 07:55:17 2021


 Performance counter stats for 'system wide':

          9,911.30 Joules power/energy-ram/                                           
         41,034.22 Joules power/energy-pkg/                                           

     296.961318654 seconds time elapsed

== END ============================
