== BEGIN ==========================
Sun 03 Oct 2021 04:27:53 AM WEST
 # note: 
 # tag: nasmg.m.mem
 # running nasmg (211003032753: Size m)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1536x1536x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         267.167 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.1746922003355E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           1536x1536x1280
 Iterations      =                       50
 Time in seconds =                  3796.23
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  2306.95
 Mop/s/thread    =                    72.09
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Sun Oct  3 04:27:53 2021


 Performance counter stats for 'system wide':

        112,481.55 Joules power/energy-ram/                                           
        527,220.19 Joules power/energy-pkg/                                           

    4063.398560974 seconds time elapsed

== END ============================
