== BEGIN ==========================
Sat 02 Oct 2021 09:38:36 PM WEST
 # note: 
 # tag: nascg.l.mem
 # running nascg (211002190235: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     9000000
 Iterations:                      5
 Number of available threads:    32

 Initialization time =        1318.762 seconds

   iteration           ||r||                 zeta
        1       0.44592820709336E-11  1499.9998413806252
        2       0.11051069261573E-14    72.8774756683722
        3       0.11378029241848E-14    73.2915890919610
        4       0.11834256607949E-14    73.6707042243700
        5       0.12190004734375E-14    74.0174332195386
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.7401743321954E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  9000000
 Iterations      =                        5
 Time in seconds =                  1548.08
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1068.72
 Mop/s/thread    =                    33.40
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Sat Oct  2 21:38:36 2021


 Performance counter stats for 'system wide':

         94,225.90 Joules power/energy-ram/                                           
        396,271.89 Joules power/energy-pkg/                                           

    2866.865714912 seconds time elapsed

== END ============================
