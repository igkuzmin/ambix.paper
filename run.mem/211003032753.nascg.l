== BEGIN ==========================
Sun 03 Oct 2021 05:43:19 AM WEST
 # note: 
 # tag: nascg.l.mem
 # running nascg (211003032753: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     9000000
 Iterations:                      5
 Number of available threads:    32

 Initialization time =        1311.194 seconds

   iteration           ||r||                 zeta
        1       0.44593143984793E-11  1499.9998413806252
        2       0.11025324005767E-14    72.8774756683720
        3       0.11378802730281E-14    73.2915890919601
        4       0.11834645055592E-14    73.6707042243706
        5       0.12189744789788E-14    74.0174332195390
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.7401743321954E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  9000000
 Iterations      =                        5
 Time in seconds =                  1483.27
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1115.42
 Mop/s/thread    =                    34.86
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Sun Oct  3 05:43:19 2021


 Performance counter stats for 'system wide':

         91,649.46 Joules power/energy-ram/                                           
        386,894.26 Joules power/energy-pkg/                                           

    2794.465017128 seconds time elapsed

== END ============================
