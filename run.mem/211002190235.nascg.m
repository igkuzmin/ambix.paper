== BEGIN ==========================
Sat 02 Oct 2021 07:02:36 PM WEST
 # note: 
 # options: SKIP
 # tag: nascg.m.mem
 # running nascg (211002190235: Size m)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         115.866 seconds

   iteration           ||r||                 zeta
        1       0.21273523009047E-11   999.9996857982868
        2       0.13466499274144E-14    57.8675766650496
        3       0.13442311458820E-14    58.2941206350798
        4       0.13464499960332E-14    58.6783531949449
        5       0.13426157870380E-14    59.0239484906588
        6       0.13348226807662E-14    59.3343718346762
        7       0.13232233928035E-14    59.6128667120508
        8       0.13139288728432E-14    59.8624490951621
        9       0.13007748447376E-14    60.0859075092867
       10       0.12902854235662E-14    60.2858074951209
       11       0.12794028611535E-14    60.4644993187786
       12       0.12692297660239E-14    60.6241279736679
       13       0.12625538150793E-14    60.7666446959977
       14       0.12539151352700E-14    60.8938193721787
       15       0.12484348175918E-14    61.0072533517979
       16       0.12425112477775E-14    61.1083922942572
       17       0.12371528925881E-14    61.1985387720226
       18       0.12335157254091E-14    61.2788644311380
       19       0.12277405608910E-14    61.3504215716458
       20       0.12242845575095E-14    61.4141540599923
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                   413.54
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  4202.68
 Mop/s/thread    =                   131.33
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Sat Oct  2 19:02:36 2021


 Performance counter stats for 'system wide':

         17,339.31 Joules power/energy-ram/                                           
         80,508.81 Joules power/energy-pkg/                                           

     532.193667106 seconds time elapsed

== END ============================
