== BEGIN ==========================
Sat 09 Oct 2021 07:28:10 AM WEST
 # note: 
 # tag: nasmg.s.mem
 # running nasmg (211009071629: Size s)
== BEFORE =========================
R_DRAM: 160692
W_DRAM: 65575
R_OPT: 346
W_OPT: 0


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1024x1024x1024  (class D)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:          26.228 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 VERIFICATION FAILED
 L2 Norm is              0.1590005169528E-09
 The correct L2 Norm is  0.1583275060440E-09


 MG Benchmark Completed.
 Class           =                        D
 Size            =           1024x1024x1024
 Iterations      =                       50
 Time in seconds =                   298.96
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                 10415.45
 Mop/s/thread    =                   325.48
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 293300
W_DRAM: 102329
R_OPT: 2614
W_OPT: 554
===================================
# started on Sat Oct  9 07:28:10 2021


 Performance counter stats for 'system wide':

         10,856.43 Joules power/energy-ram/                                           
         45,055.08 Joules power/energy-pkg/                                           

     327.152001042 seconds time elapsed

== END ============================
