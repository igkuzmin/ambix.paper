== BEGIN ==========================
Sun 03 Oct 2021 03:27:53 AM WEST
 # note: 
 # tag: nascg.m.mem
 # running nascg (211003032753: Size m)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         138.744 seconds

   iteration           ||r||                 zeta
        1       0.21274500671886E-11   999.9996857982868
        2       0.13466905997012E-14    57.8675766650496
        3       0.13442552855029E-14    58.2941206350794
        4       0.13464260921963E-14    58.6783531949452
        5       0.13424699920888E-14    59.0239484906585
        6       0.13347723062738E-14    59.3343718346757
        7       0.13232639723148E-14    59.6128667120514
        8       0.13139162795956E-14    59.8624490951619
        9       0.13008007464311E-14    60.0859075092869
       10       0.12901715289957E-14    60.2858074951207
       11       0.12795065267513E-14    60.4644993187783
       12       0.12691766734910E-14    60.6241279736677
       13       0.12625845985414E-14    60.7666446959977
       14       0.12540088741764E-14    60.8938193721787
       15       0.12484747244109E-14    61.0072533517981
       16       0.12425887858928E-14    61.1083922942576
       17       0.12371253744336E-14    61.1985387720226
       18       0.12336025994830E-14    61.2788644311374
       19       0.12277718653607E-14    61.3504215716453
       20       0.12242373461228E-14    61.4141540599917
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                   571.36
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  3041.80
 Mop/s/thread    =                    95.06
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Sun Oct  3 03:27:54 2021


 Performance counter stats for 'system wide':

         24,910.53 Joules power/energy-ram/                                           
        103,986.69 Joules power/energy-pkg/                                           

     712.526476895 seconds time elapsed

== END ============================
