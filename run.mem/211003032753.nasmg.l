== BEGIN ==========================
Sun 03 Oct 2021 06:51:21 AM WEST
 # note: 
 # tag: nasmg.l.mem
 # running nasmg (211003032753: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 2048x2048x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         610.388 seconds

  iter   1
  iter   5
  iter  10
