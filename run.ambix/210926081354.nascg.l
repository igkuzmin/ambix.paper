== BEGIN ==========================
Sun 26 Sep 2021 03:26:57 PM WEST
 # note: 
 # tag: nascg.l.ambix
 # running nascg (210926081354: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     9000000
 Iterations:                      5
 Number of available threads:    32

 Initialization time =        1897.978 seconds

   iteration           ||r||                 zeta
        1       0.44592694720969E-11  1499.9998413806252
        2       0.11050818892029E-14    72.8774756683720
        3       0.11378737905815E-14    73.2915890919610
        4       0.11834640436907E-14    73.6707042243702
        5       0.12188678085021E-14    74.0174332195386
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.7401743321954E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  9000000
 Iterations      =                        5
 Time in seconds =                   949.87
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1741.79
 Mop/s/thread    =                    54.43
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Sun Sep 26 15:26:57 2021


 Performance counter stats for 'system wide':

         84,614.81 Joules power/energy-ram/                                           
        397,422.91 Joules power/energy-pkg/                                           

    2858.272909504 seconds time elapsed

== END ============================
