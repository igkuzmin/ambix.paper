== BEGIN ==========================
Sun 26 Sep 2021 11:52:44 AM WEST
 # note: 
 # tag: nasmg.s.ambix
 # options: SKIP
 # running nasmg (210926081354: Size s)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1024x1024x1024  (class D)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:          95.549 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 VERIFICATION FAILED
 L2 Norm is              0.1590005169528E-09
 The correct L2 Norm is  0.1583275060440E-09


 MG Benchmark Completed.
 Class           =                        D
 Size            =           1024x1024x1024
 Iterations      =                       50
 Time in seconds =                   471.99
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  6597.23
 Mop/s/thread    =                   206.16
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Sun Sep 26 11:52:44 2021


 Performance counter stats for 'system wide':

         16,522.12 Joules power/energy-ram/                                           
         77,623.17 Joules power/energy-pkg/                                           

     567.576948359 seconds time elapsed

== END ============================
