== BEGIN ==========================
Sat 06 Nov 2021 05:53:08 PM WET
 # note: 
 # tag: nasmg.s.ambix
 # running nasmg (211106175308: Size s)
== BEFORE =========================
R_DRAM: 643791
W_DRAM: 105561
R_OPT: 2614
W_OPT: 554


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1024x1024x1024  (class D)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:          12.832 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 VERIFICATION FAILED
 L2 Norm is              0.1590005169528E-09
 The correct L2 Norm is  0.1583275060440E-09


 MG Benchmark Completed.
 Class           =                        D
 Size            =           1024x1024x1024
 Iterations      =                       50
 Time in seconds =                   306.82
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                 10148.79
 Mop/s/thread    =                   317.15
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 129108
W_DRAM: 34282
R_OPT: 3840
W_OPT: 729
===================================
# started on Sat Nov  6 17:53:09 2021


 Performance counter stats for 'system wide':

         10,791.21 Joules power/energy-ram/                                           
         45,144.35 Joules power/energy-pkg/                                           

     321.245629219 seconds time elapsed

== END ============================
