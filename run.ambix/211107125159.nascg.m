== BEGIN ==========================
Sun 07 Nov 2021 03:29:27 PM WET
 # note: 
 # tag: nascg.m.ambix
 # running nascg (211107125159: Size m)
== BEFORE =========================
R_DRAM: 3660552
W_DRAM: 1520267
R_OPT: 1075125
W_OPT: 244179


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         183.175 seconds

   iteration           ||r||                 zeta
        1       0.21274440503522E-11   999.9996857982868
        2       0.13450106833887E-14    57.8675766650498
        3       0.13442805964420E-14    58.2941206350796
        4       0.13463732412099E-14    58.6783531949450
        5       0.13426165027799E-14    59.0239484906580
        6       0.13347686052517E-14    59.3343718346765
        7       0.13232957030621E-14    59.6128667120510
        8       0.13139806167288E-14    59.8624490951617
        9       0.13007445222698E-14    60.0859075092867
       10       0.12902334514258E-14    60.2858074951209
       11       0.12793398677999E-14    60.4644993187783
       12       0.12692340135621E-14    60.6241279736673
       13       0.12624652439943E-14    60.7666446959980
       14       0.12539837729864E-14    60.8938193721784
       15       0.12484212305982E-14    61.0072533517981
       16       0.12425713402683E-14    61.1083922942578
       17       0.12372013866642E-14    61.1985387720226
       18       0.12335072312073E-14    61.2788644311374
       19       0.12276702551703E-14    61.3504215716456
       20       0.12242678544212E-14    61.4141540599919
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                   654.09
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  2657.05
 Mop/s/thread    =                    83.03
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 3847999
W_DRAM: 1526079
R_OPT: 1175709
W_OPT: 245365
===================================
# started on Sun Nov  7 15:29:27 2021


 Performance counter stats for 'system wide':

         26,072.27 Joules power/energy-ram/                                           
        121,794.79 Joules power/energy-pkg/                                           

     841.321428207 seconds time elapsed

== END ============================
