== BEGIN ==========================
Sat 06 Nov 2021 11:13:10 PM WET
 # note: 
 # tag: nascg.m.ambix
 # running nascg (211106205427: Size m)
== BEFORE =========================
R_DRAM: 2580861
W_DRAM: 1118319
R_OPT: 646987
W_OPT: 156410


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         132.915 seconds

   iteration           ||r||                 zeta
        1       0.21274465851313E-11   999.9996857982868
        2       0.13450134844485E-14    57.8675766650500
        3       0.13442196858596E-14    58.2941206350794
        4       0.13463959018646E-14    58.6783531949449
        5       0.13424724186501E-14    59.0239484906585
        6       0.13348660932668E-14    59.3343718346763
        7       0.13233393715700E-14    59.6128667120516
        8       0.13139897875219E-14    59.8624490951615
        9       0.13008203333266E-14    60.0859075092867
       10       0.12903296421386E-14    60.2858074951212
       11       0.12794282273684E-14    60.4644993187781
       12       0.12692499732502E-14    60.6241279736680
       13       0.12624457696728E-14    60.7666446959980
       14       0.12540016890677E-14    60.8938193721789
       15       0.12483669423510E-14    61.0072533517979
       16       0.12426159879490E-14    61.1083922942582
       17       0.12372746449703E-14    61.1985387720225
       18       0.12334233456270E-14    61.2788644311375
       19       0.12277206455693E-14    61.3504215716458
       20       0.12243247969503E-14    61.4141540599921
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                   696.28
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  2496.05
 Mop/s/thread    =                    78.00
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 2822517
W_DRAM: 1124687
R_OPT: 773806
W_OPT: 156718
===================================
# started on Sat Nov  6 23:13:11 2021


 Performance counter stats for 'system wide':

         27,858.88 Joules power/energy-ram/                                           
        120,108.70 Joules power/energy-pkg/                                           

     831.551324174 seconds time elapsed

== END ============================
