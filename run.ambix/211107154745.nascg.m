== BEGIN ==========================
Sun 07 Nov 2021 06:27:03 PM WET
 # note: 
 # tag: nascg.m.ambix
 # running nascg (211107154745: Size m)
== BEFORE =========================
R_DRAM: 4924772
W_DRAM: 2080539
R_OPT: 1449491
W_OPT: 335707


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         273.540 seconds

   iteration           ||r||                 zeta
        1       0.21274317356284E-11   999.9996857982868
        2       0.13450136068036E-14    57.8675766650500
        3       0.13441309668513E-14    58.2941206350800
        4       0.13463922131498E-14    58.6783531949446
        5       0.13424732866072E-14    59.0239484906585
        6       0.13347886161884E-14    59.3343718346763
        7       0.13232198638018E-14    59.6128667120510
        8       0.13139567037048E-14    59.8624490951617
        9       0.13008645846177E-14    60.0859075092867
       10       0.12902124920660E-14    60.2858074951207
       11       0.12793547343640E-14    60.4644993187778
       12       0.12692541848333E-14    60.6241279736682
       13       0.12625367931353E-14    60.7666446959977
       14       0.12540552787691E-14    60.8938193721789
       15       0.12484495637378E-14    61.0072533517979
       16       0.12426200958760E-14    61.1083922942580
       17       0.12372604615697E-14    61.1985387720226
       18       0.12336452586853E-14    61.2788644311375
       19       0.12276964742343E-14    61.3504215716462
       20       0.12242449756691E-14    61.4141540599921
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                   836.58
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  2077.45
 Mop/s/thread    =                    64.92
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 5202225
W_DRAM: 2085637
R_OPT: 1540507
W_OPT: 337466
===================================
# started on Sun Nov  7 18:27:04 2021


 Performance counter stats for 'system wide':

         34,089.18 Joules power/energy-ram/                                           
        157,375.34 Joules power/energy-pkg/                                           

    1114.705489514 seconds time elapsed

== END ============================
