== BEGIN ==========================
Sun 26 Sep 2021 12:02:12 PM WEST
 # note: 
 # tag: nasmg.l.ambix
 # running nasmg (210926081354: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 2048x2048x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         541.897 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.2004924648216E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           2048x2048x1280
 Iterations      =                       50
 Time in seconds =                 11036.58
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1410.70
 Mop/s/thread    =                    44.08
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Sun Sep 26 12:02:12 2021


 Performance counter stats for 'system wide':

        280,308.51 Joules power/energy-ram/                                           
      1,529,139.08 Joules power/energy-pkg/                                           

   11587.477340846 seconds time elapsed

== END ============================
