== BEGIN ==========================
Sat 06 Nov 2021 08:54:27 PM WET
 # note: 
 # tag: nasmg.s.ambix
 # running nasmg (211106205427: Size s)
== BEFORE =========================
R_DRAM: 1533413
W_DRAM: 657041
R_OPT: 415468
W_OPT: 88408


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1024x1024x1024  (class D)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:          11.850 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 VERIFICATION FAILED
 L2 Norm is              0.1590005169528E-09
 The correct L2 Norm is  0.1583275060440E-09


 MG Benchmark Completed.
 Class           =                        D
 Size            =           1024x1024x1024
 Iterations      =                       50
 Time in seconds =                   308.97
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                 10078.22
 Mop/s/thread    =                   314.94
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 1663429
W_DRAM: 691426
R_OPT: 418663
W_OPT: 89022
===================================
# started on Sat Nov  6 20:54:28 2021


 Performance counter stats for 'system wide':

         10,851.77 Joules power/energy-ram/                                           
         45,542.13 Joules power/energy-pkg/                                           

     322.411569421 seconds time elapsed

== END ============================
