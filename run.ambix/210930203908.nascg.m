== BEGIN ==========================
Thu 30 Sep 2021 10:25:23 PM WEST
 # note: 
 # tag: nascg.m.ambix
 # running nascg (210930203908: Size m)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         135.544 seconds

   iteration           ||r||                 zeta
        1       0.21274154404537E-11   999.9996857982868
        2       0.13479120159312E-14    57.8675766650493
        3       0.13442993836801E-14    58.2941206350794
        4       0.13464197320280E-14    58.6783531949446
        5       0.13423854427868E-14    59.0239484906588
        6       0.13348724468416E-14    59.3343718346759
        7       0.13232812357745E-14    59.6128667120516
        8       0.13140984254070E-14    59.8624490951613
        9       0.13009220223017E-14    60.0859075092864
       10       0.12902398471287E-14    60.2858074951207
       11       0.12794587311636E-14    60.4644993187783
       12       0.12692839746121E-14    60.6241279736682
       13       0.12626375912156E-14    60.7666446959979
       14       0.12539563277249E-14    60.8938193721790
       15       0.12484200806874E-14    61.0072533517981
       16       0.12426754983526E-14    61.1083922942572
       17       0.12372014222938E-14    61.1985387720226
       18       0.12336850183183E-14    61.2788644311378
       19       0.12277149007913E-14    61.3504215716456
       20       0.12242897615005E-14    61.4141540599923
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                   696.31
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  2495.94
 Mop/s/thread    =                    78.00
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Thu Sep 30 22:25:23 2021


 Performance counter stats for 'system wide':

         27,845.35 Joules power/energy-ram/                                           
        120,720.44 Joules power/energy-pkg/                                           

     831.982972183 seconds time elapsed

== END ============================
