== BEGIN ==========================
Sat 06 Nov 2021 08:35:55 PM WET
 # note: 
 # tag: nascg.m.ambix
 # running nascg (211106175308: Size m)
== BEFORE =========================
R_DRAM: 1267259
W_DRAM: 638693
R_OPT: 285476
W_OPT: 86360


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         153.204 seconds

   iteration           ||r||                 zeta
        1       0.21274550614105E-11   999.9996857982868
        2       0.13467101519891E-14    57.8675766650492
        3       0.13442458194652E-14    58.2941206350798
        4       0.13464353552017E-14    58.6783531949449
        5       0.13425681138285E-14    59.0239484906582
        6       0.13348209657244E-14    59.3343718346759
        7       0.13233051690619E-14    59.6128667120514
        8       0.13139717075455E-14    59.8624490951617
        9       0.13008538720939E-14    60.0859075092872
       10       0.12903438423218E-14    60.2858074951201
       11       0.12794529511057E-14    60.4644993187784
       12       0.12692914262887E-14    60.6241279736674
       13       0.12624826893885E-14    60.7666446959980
       14       0.12539562527307E-14    60.8938193721787
       15       0.12484935351488E-14    61.0072533517978
       16       0.12425153004698E-14    61.1083922942580
       17       0.12372271050576E-14    61.1985387720229
       18       0.12335669594858E-14    61.2788644311378
       19       0.12277078921257E-14    61.3504215716458
       20       0.12243259760927E-14    61.4141540599923
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                   699.01
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  2486.31
 Mop/s/thread    =                    77.70
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 1510044
W_DRAM: 645053
R_OPT: 413388
W_OPT: 86670
===================================
# started on Sat Nov  6 20:35:55 2021


 Performance counter stats for 'system wide':

         28,462.16 Joules power/energy-ram/                                           
        123,021.08 Joules power/energy-pkg/                                           

     854.847480016 seconds time elapsed

== END ============================
