== BEGIN ==========================
Thu 30 Sep 2021 09:12:33 PM WEST
 # note: 
 # tag: nasmg.m.ambix
 # running nasmg (210930203908: Size m)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1536x1536x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         243.965 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.1746922003355E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           1536x1536x1280
 Iterations      =                       50
 Time in seconds =                  4121.39
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  2124.94
 Mop/s/thread    =                    66.40
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Thu Sep 30 21:12:33 2021


 Performance counter stats for 'system wide':

        109,401.20 Joules power/energy-ram/                                           
        576,289.71 Joules power/energy-pkg/                                           

    4369.972334988 seconds time elapsed

== END ============================
