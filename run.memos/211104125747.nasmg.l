== BEGIN ==========================
Thu 04 Nov 2021 06:19:49 PM WET
 # note: 
 # tag: nasmg.l.memos
 # running nasmg (211104125747: Size l)
== BEFORE =========================
R_DRAM: 1827064
W_DRAM: 555549
R_OPT: 486983
W_OPT: 201838


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 2048x2048x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         546.932 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.2004924648216E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           2048x2048x1280
 Iterations      =                       50
 Time in seconds =                  9781.77
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1591.66
 Mop/s/thread    =                    49.74
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 2140465
W_DRAM: 604220
R_OPT: 948268
W_OPT: 321172
===================================
# started on Thu Nov  4 18:19:49 2021


 Performance counter stats for 'system wide':

        250,176.44 Joules power/energy-ram/                                           
      1,345,254.43 Joules power/energy-pkg/                                           

   10335.984227837 seconds time elapsed

== END ============================
