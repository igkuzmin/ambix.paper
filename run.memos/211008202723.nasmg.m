== BEGIN ==========================
Sat 09 Oct 2021 03:56:40 AM WEST
 # note: 
 # tag: nasmg.m.memos
 # running nasmg (211008202723: Size m)
== BEFORE =========================
R_DRAM: 2558025
W_DRAM: 684105
R_OPT: 1148181
W_OPT: 232546


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1536x1536x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         262.127 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.1746922003355E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           1536x1536x1280
 Iterations      =                       50
 Time in seconds =                  4286.82
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  2042.94
 Mop/s/thread    =                    63.84
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 2757320
W_DRAM: 726449
R_OPT: 1346701
W_OPT: 284038
===================================
# started on Sat Oct  9 03:56:41 2021


 Performance counter stats for 'system wide':

        112,664.77 Joules power/energy-ram/                                           
        595,438.02 Joules power/energy-pkg/                                           

    4548.974430814 seconds time elapsed

== END ============================
