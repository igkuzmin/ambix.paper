== BEGIN ==========================
Fri 08 Oct 2021 08:44:19 PM WEST
 # note: 
 # tag: nasft.s.memos
 # running nasft (211008202723: Size s)
== BEFORE =========================
R_DRAM: 231713
W_DRAM: 64500
R_OPT: 7801
W_OPT: 3010


 NAS Parallel Benchmarks (NPB3.4-OMP) - FT Benchmark

 Size                : 1024x1024x 512
 Iterations                  :     25
 Number of available threads :     32

 T =    1     Checksum =    5.100021384457D+02    5.102354099484D+02
 T =    2     Checksum =    5.107077402572D+02    5.110372633663D+02
 T =    3     Checksum =    5.111258943980D+02    5.113134002360D+02
 T =    4     Checksum =    5.113745426197D+02    5.114685939810D+02
 T =    5     Checksum =    5.115371222930D+02    5.115707374808D+02
 T =    6     Checksum =    5.116520247117D+02    5.116449517887D+02
 T =    7     Checksum =    5.117383844125D+02    5.117031961855D+02
 T =    8     Checksum =    5.118063403327D+02    5.117514932905D+02
 T =    9     Checksum =    5.118615759864D+02    5.117929844143D+02
 T =   10     Checksum =    5.119074780580D+02    5.118293855692D+02
 T =   11     Checksum =    5.119461989515D+02    5.118616995522D+02
 T =   12     Checksum =    5.119791954308D+02    5.118905677615D+02
 T =   13     Checksum =    5.120075109022D+02    5.119164444681D+02
 T =   14     Checksum =    5.120319290468D+02    5.119396827497D+02
 T =   15     Checksum =    5.120530609872D+02    5.119605764397D+02
 T =   16     Checksum =    5.120713970144D+02    5.119793803911D+02
 T =   17     Checksum =    5.120873387758D+02    5.119963202810D+02
 T =   18     Checksum =    5.121012203067D+02    5.120115975573D+02
 T =   19     Checksum =    5.121133224506D+02    5.120253922561D+02
 T =   20     Checksum =    5.121238832012D+02    5.120378649681D+02
 T =   21     Checksum =    5.121331054187D+02    5.120491585092D+02
 T =   22     Checksum =    5.121411627798D+02    5.120593995031D+02
 T =   23     Checksum =    5.121482044900D+02    5.120686999316D+02
 T =   24     Checksum =    5.121543590893D+02    5.120771586436D+02
 T =   25     Checksum =    5.121597375743D+02    5.120848627974D+02
 class = U


 FT Benchmark Completed.
 Class           =                        U
 Size            =           1024x1024x 512
 Iterations      =                       25
 Time in seconds =                   184.27
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                 11404.07
 Mop/s/thread    =                   356.38
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 291854
W_DRAM: 100283
R_OPT: 8034
W_OPT: 3184
===================================
# started on Fri Oct  8 20:44:20 2021


 Performance counter stats for 'system wide':

          6,443.09 Joules power/energy-ram/                                           
         29,913.92 Joules power/energy-pkg/                                           

     194.591625152 seconds time elapsed

== END ============================
