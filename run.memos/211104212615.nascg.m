== BEGIN ==========================
Fri 05 Nov 2021 06:13:51 AM WET
 # note: 
 # tag: nascg.m.memos
 # running nascg (211104212615: Size m)
== BEFORE =========================
R_DRAM: 4625527
W_DRAM: 1293437
R_OPT: 2065658
W_OPT: 652497


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         191.491 seconds

   iteration           ||r||                 zeta
        1       0.21274502207234E-11   999.9996857982868
        2       0.13479429135618E-14    57.8675766650493
        3       0.13441777144967E-14    58.2941206350793
        4       0.13464697788000E-14    58.6783531949446
        5       0.13424243255098E-14    59.0239484906588
        6       0.13347781076769E-14    59.3343718346759
        7       0.13233005676507E-14    59.6128667120510
        8       0.13140060299901E-14    59.8624490951615
        9       0.13009692337904E-14    60.0859075092867
       10       0.12903249880429E-14    60.2858074951204
       11       0.12792980121651E-14    60.4644993187781
       12       0.12692960070164E-14    60.6241279736682
       13       0.12624932701022E-14    60.7666446959980
       14       0.12539295896366E-14    60.8938193721784
       15       0.12485777698360E-14    61.0072533517978
       16       0.12425666976782E-14    61.1083922942580
       17       0.12372190422117E-14    61.1985387720226
       18       0.12335210771759E-14    61.2788644311380
       19       0.12276217194302E-14    61.3504215716458
       20       0.12243113027396E-14    61.4141540599919
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                   770.51
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  2255.59
 Mop/s/thread    =                    70.49
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 4766776
W_DRAM: 1298659
R_OPT: 2187781
W_OPT: 652771
===================================
# started on Fri Nov  5 06:13:52 2021


 Performance counter stats for 'system wide':

         27,955.20 Joules power/energy-ram/                                           
        134,573.76 Joules power/energy-pkg/                                           

     962.162298976 seconds time elapsed

== END ============================
