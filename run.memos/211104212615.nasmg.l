== BEGIN ==========================
Fri 05 Nov 2021 02:54:34 AM WET
 # note: 
 # tag: nasmg.l.memos
 # running nasmg (211104212615: Size l)
== BEFORE =========================
R_DRAM: 4317463
W_DRAM: 1250493
R_OPT: 1542095
W_OPT: 523681


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 2048x2048x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         585.762 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.2004924648216E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           2048x2048x1280
 Iterations      =                       50
 Time in seconds =                 11361.72
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1370.33
 Mop/s/thread    =                    42.82
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 4625527
W_DRAM: 1293437
R_OPT: 2065658
W_OPT: 652497
===================================
# started on Fri Nov  5 02:54:35 2021


 Performance counter stats for 'system wide':

        287,687.88 Joules power/energy-ram/                                           
      1,559,481.39 Joules power/energy-pkg/                                           

   11956.531662667 seconds time elapsed

== END ============================
