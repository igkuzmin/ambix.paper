== BEGIN ==========================
Fri 08 Oct 2021 08:36:04 PM WEST
 # note: 
 # tag: nasmg.s.memos
 # running nasmg (211008202723: Size s)
== BEFORE =========================
R_DRAM: 106697
W_DRAM: 32843
R_OPT: 20
W_OPT: 46


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1024x1024x1024  (class D)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:          14.955 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 VERIFICATION FAILED
 L2 Norm is              0.1590005169528E-09
 The correct L2 Norm is  0.1583275060440E-09


 MG Benchmark Completed.
 Class           =                        D
 Size            =           1024x1024x1024
 Iterations      =                       50
 Time in seconds =                   477.19
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  6525.43
 Mop/s/thread    =                   203.92
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 231713
W_DRAM: 64500
R_OPT: 7801
W_OPT: 3010
===================================
# started on Fri Oct  8 20:36:04 2021


 Performance counter stats for 'system wide':

         14,649.15 Joules power/energy-ram/                                           
         67,099.76 Joules power/energy-pkg/                                           

     493.990645907 seconds time elapsed

== END ============================
