== BEGIN ==========================
Thu 04 Nov 2021 09:12:05 PM WET
 # note: 
 # tag: nascg.m.memos
 # running nascg (211104125747: Size m)
== BEFORE =========================
R_DRAM: 2140465
W_DRAM: 604220
R_OPT: 948268
W_OPT: 321172


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         157.179 seconds

   iteration           ||r||                 zeta
        1       0.21274465851313E-11   999.9996857982868
        2       0.13479509932233E-14    57.8675766650492
        3       0.13441608757370E-14    58.2941206350794
        4       0.13464221765712E-14    58.6783531949446
        5       0.13425773760907E-14    59.0239484906582
        6       0.13348238883393E-14    59.3343718346759
        7       0.13232303874835E-14    59.6128667120514
        8       0.13141096784877E-14    59.8624490951621
        9       0.13008288807565E-14    60.0859075092866
       10       0.12902327282629E-14    60.2858074951206
       11       0.12794483470234E-14    60.4644993187783
       12       0.12692451538762E-14    60.6241279736680
       13       0.12625514153086E-14    60.7666446959977
       14       0.12539405436739E-14    60.8938193721784
       15       0.12484155645994E-14    61.0072533517975
       16       0.12425306218213E-14    61.1083922942576
       17       0.12372075327615E-14    61.1985387720226
       18       0.12334904713440E-14    61.2788644311381
       19       0.12277349161399E-14    61.3504215716461
       20       0.12244111260757E-14    61.4141540599921
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                   689.62
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  2520.17
 Mop/s/thread    =                    78.76
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 2364841
W_DRAM: 609665
R_OPT: 1059513
W_OPT: 321460
===================================
# started on Thu Nov  4 21:12:06 2021


 Performance counter stats for 'system wide':

         27,408.39 Joules power/energy-ram/                                           
        121,828.65 Joules power/energy-pkg/                                           

     849.257450971 seconds time elapsed

== END ============================
