== BEGIN ==========================
Fri 08 Oct 2021 09:00:46 PM WEST
 # note: 
 # tag: nascg.l.memos
 # running nascg (211008202723: Size l)
== BEFORE =========================
R_DRAM: 593235
W_DRAM: 104958
R_OPT: 61282
W_OPT: 3406


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     9000000
 Iterations:                      5
 Number of available threads:    32

 Initialization time =        2676.776 seconds

   iteration           ||r||                 zeta
        1       0.44593116744666E-11  1499.9998413806252
        2       0.11025325138580E-14    72.8774756683715
        3       0.11378402104006E-14    73.2915890919605
        4       0.11834819186153E-14    73.6707042243695
        5       0.12189317065515E-14    74.0174332195390
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.7401743321954E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  9000000
 Iterations      =                        5
 Time in seconds =                   980.31
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1687.70
 Mop/s/thread    =                    52.74
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 1225560
W_DRAM: 137919
R_OPT: 333995
W_OPT: 17142
===================================
# started on Fri Oct  8 21:00:46 2021


 Performance counter stats for 'system wide':

        104,049.72 Joules power/energy-ram/                                           
        505,152.96 Joules power/energy-pkg/                                           

    3657.234196313 seconds time elapsed

== END ============================
