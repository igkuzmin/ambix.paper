== BEGIN ==========================
Thu 04 Nov 2021 09:41:37 PM WET
 # note: 
 # tag: nasmg.s.memos
 # running nasmg (211104212615: Size s)
== BEFORE =========================
R_DRAM: 2777371
W_DRAM: 648903
R_OPT: 1060275
W_OPT: 322083


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1024x1024x1024  (class D)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:          11.712 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 VERIFICATION FAILED
 L2 Norm is              0.1590005169528E-09
 The correct L2 Norm is  0.1583275060440E-09


 MG Benchmark Completed.
 Class           =                        D
 Size            =           1024x1024x1024
 Iterations      =                       50
 Time in seconds =                   302.92
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                 10279.47
 Mop/s/thread    =                   321.23
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 2907209
W_DRAM: 682591
R_OPT: 1061202
W_OPT: 322431
===================================
# started on Thu Nov  4 21:41:37 2021


 Performance counter stats for 'system wide':

         10,685.54 Joules power/energy-ram/                                           
         44,013.99 Joules power/energy-pkg/                                           

     316.063550570 seconds time elapsed

== END ============================
