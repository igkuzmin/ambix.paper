== BEGIN ==========================
Thu 04 Nov 2021 03:09:42 PM WET
 # note: 
 # tag: nasmg.m.memos
 # running nasmg (211104125747: Size m)
== BEFORE =========================
R_DRAM: 981939
W_DRAM: 166100
R_OPT: 134246
W_OPT: 69814


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1536x1536x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         248.477 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.1746922003355E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           1536x1536x1280
 Iterations      =                       50
 Time in seconds =                  4634.83
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1889.54
 Mop/s/thread    =                    59.05
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 1168371
W_DRAM: 203341
R_OPT: 347004
W_OPT: 126166
===================================
# started on Thu Nov  4 15:09:42 2021


 Performance counter stats for 'system wide':

        119,886.94 Joules power/energy-ram/                                           
        636,660.61 Joules power/energy-pkg/                                           

    4883.417424160 seconds time elapsed

== END ============================
