== BEGIN ==========================
Sat 09 Oct 2021 05:12:30 AM WEST
 # note: 
 # tag: nascg.m.memos
 # running nascg (211008202723: Size m)
== BEFORE =========================
R_DRAM: 2757320
W_DRAM: 726449
R_OPT: 1346701
W_OPT: 284038


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         242.350 seconds

   iteration           ||r||                 zeta
        1       0.21274439315797E-11   999.9996857982868
        2       0.13479522577099E-14    57.8675766650492
        3       0.13442393090662E-14    58.2941206350794
        4       0.13463968622384E-14    58.6783531949449
        5       0.13424645314888E-14    59.0239484906577
        6       0.13348193772723E-14    59.3343718346763
        7       0.13233033491524E-14    59.6128667120506
        8       0.13140061563507E-14    59.8624490951622
        9       0.13008328613298E-14    60.0859075092866
       10       0.12902275727264E-14    60.2858074951206
       11       0.12793991997957E-14    60.4644993187786
       12       0.12693699596924E-14    60.6241279736680
       13       0.12625080010176E-14    60.7666446959979
       14       0.12539254578257E-14    60.8938193721784
       15       0.12484148972480E-14    61.0072533517975
       16       0.12427095835894E-14    61.1083922942578
       17       0.12373384617294E-14    61.1985387720225
       18       0.12334719851394E-14    61.2788644311374
       19       0.12277101146526E-14    61.3504215716461
       20       0.12242638016345E-14    61.4141540599921
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                   610.40
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  2847.24
 Mop/s/thread    =                    88.98
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 2985490
W_DRAM: 730072
R_OPT: 1401567
W_OPT: 286059
===================================
# started on Sat Oct  9 05:12:30 2021


 Performance counter stats for 'system wide':

         25,996.02 Joules power/energy-ram/                                           
        122,349.49 Joules power/energy-pkg/                                           

     856.850475307 seconds time elapsed

== END ============================
