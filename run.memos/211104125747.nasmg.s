== BEGIN ==========================
Thu 04 Nov 2021 01:13:04 PM WET
 # note: 
 # tag: nasmg.s.memos
 # running nasmg (211104125747: Size s)
== BEFORE =========================
R_DRAM: 408048
W_DRAM: 39280
R_OPT: 5243
W_OPT: 208


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1024x1024x1024  (class D)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:          12.934 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 VERIFICATION FAILED
 L2 Norm is              0.1590005169528E-09
 The correct L2 Norm is  0.1583275060440E-09


 MG Benchmark Completed.
 Class           =                        D
 Size            =           1024x1024x1024
 Iterations      =                       50
 Time in seconds =                   355.03
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  8770.66
 Mop/s/thread    =                   274.08
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              29 Mar 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 536170
W_DRAM: 72073
R_OPT: 8112
W_OPT: 1384
===================================
# started on Thu Nov  4 13:13:04 2021


 Performance counter stats for 'system wide':

         11,876.49 Joules power/energy-ram/                                           
         50,286.82 Joules power/energy-pkg/                                           

     369.380608898 seconds time elapsed

== END ============================
