#FROM atlassian/default-image:latest
FROM centos:7

LABEL name=gnuplot

RUN \
  yum upgrade; \
  yum -y install \
    gnuplot \
    make \
    perl \
    texlive-epstopdf \
    ghostscript \
    git \
    wget \
    perl-Data-Dumper \
    ; \
    rm -rf /var/lib/apt/lists/*;
