set linetype  3 lc rgb "#00b04f" lw 1
set linetype  2 lc rgb "#eb7d30" lw 1
set linetype  1 lc rgb "#4270c2" lw 1
set linetype  4 lc rgb "#ffbf00" lw 1
set linetype  5 lc rgb "#efe342" lw 1
set linetype  6 lc rgb "#9365bc" lw 1
set linetype cycle  6
#set linetype  1 lc rgb "#1f77b4" lw 1
#set linetype  2 lc rgb "#ff7f0e" lw 1
#set linetype  3 lc rgb "#2ca02c" lw 1
#set linetype  4 lc rgb "#d62728" lw 1
#set linetype  5 lc rgb "#9467bd" lw 1
#set linetype  6 lc rgb "#8c564b" lw 1
#set linetype  7 lc rgb "#e377c2" lw 1
#set linetype  8 lc rgb "#7f7f7f" lw 1
#set linetype  9 lc rgb "#bcbd22" lw 1
#set linetype 10 lc rgb "#17becf" lw 1
#set linetype cycle 10

#set terminal enhanced font "Helvetica,14"
set tics font ",17"
set ylabel font ",20"
set key font ",20" width -10 spacing 2
set border 3


# vim: ft=gnuplot
