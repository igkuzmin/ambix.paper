== BEGIN ==========================
Tue 05 Oct 2021 02:59:32 AM WEST
 # setting up vm
vm.migration_batch_size = 8
vm.sysctl_enable_thp_migration = 0
vm.limit_mt_num = 4
never
never
 # setup memory (/sys/fs/cgroup/nimble -> 26Gb)
 # note: 
 # launcher args: --exchange_pages --prefer_memnode --cpunode  0 --fast_mem 0 --slow_mem 2 --dumpstats --dumpstats_period 5 --memory_manage
 # tag: nascg.m.nimble[nTHP+npf]
 # running nascg (211005002510: Size m)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         186.262 seconds

   iteration           ||r||                 zeta
        1       0.21274438157040E-11   999.9996857982868
        2       0.13434063387701E-14    57.8675766650496
        3       0.13442238440095E-14    58.2941206350796
        4       0.13463101027988E-14    58.6783531949449
        5       0.13425052419214E-14    59.0239484906585
        6       0.13347893471357E-14    59.3343718346763
        7       0.13233008197071E-14    59.6128667120508
        8       0.13139338615744E-14    59.8624490951613
        9       0.13008776419400E-14    60.0859075092864
       10       0.12902824729503E-14    60.2858074951204
       11       0.12794972454585E-14    60.4644993187784
       12       0.12692606515194E-14    60.6241279736677
       13       0.12625467810151E-14    60.7666446959979
       14       0.12540761323899E-14    60.8938193721787
       15       0.12483865433124E-14    61.0072533517979
       16       0.12426360071658E-14    61.1083922942576
       17       0.12372383886781E-14    61.1985387720231
       18       0.12335731382828E-14    61.2788644311375
       19       0.12275972950879E-14    61.3504215716453
       20       0.12242935030556E-14    61.4141540599919
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                  1991.88
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   872.52
 Mop/s/thread    =                    27.27
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


cycles: 5014916126762
real time(ms): 2180378, user time(ms): 69206744, system time(ms): 366165, virtual cpu time(ms): 69572909
min_flt: 10172173, maj_flt: 68, maxrss: 40691228 KB
child arg: npb.bin/cg.m.x
child pid: 34218
===================================
===================================
WaitBasePageMigration_ms 0
WaitHugePageMigration_ms 0
ExchangePages_nr_exchanges 374
ExchangePagesBase_nr_base_pages 699
ExchangePagesHuge_nr_base_pages 0
Fast2Slow_nr_migrations 374
Fast2SlowBasePageMigrations_nr_base_pages 18446744073709551615
Fast2SlowHugePageMigrations_nr_base_pages 0
Slow2Fast_nr_migrations 387
Slow2FastBasePageMigrations_nr_base_pages 162868
Slow2FastHugePageMigrations_nr_base_pages 0
== END ============================
