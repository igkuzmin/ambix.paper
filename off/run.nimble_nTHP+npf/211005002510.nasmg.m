== BEGIN ==========================
Tue 05 Oct 2021 01:42:59 AM WEST
 # setting up vm
vm.migration_batch_size = 8
vm.sysctl_enable_thp_migration = 0
vm.limit_mt_num = 4
never
never
 # setup memory (/sys/fs/cgroup/nimble -> 26Gb)
 # note: 
 # launcher args: --exchange_pages --prefer_memnode --cpunode  0 --fast_mem 0 --slow_mem 2 --dumpstats --dumpstats_period 5 --memory_manage
 # tag: nasmg.m.nimble[nTHP+npf]
 # running nasmg (211005002510: Size m)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1536x1536x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         247.216 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.1746922003355E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           1536x1536x1280
 Iterations      =                       50
 Time in seconds =                  4340.42
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  2017.71
 Mop/s/thread    =                    63.05
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


cycles: 10563066760482
real time(ms): 4592598, user time(ms): 144571770, system time(ms): 974925, virtual cpu time(ms): 145546695
min_flt: 19471408, maj_flt: 18, maxrss: 77887940 KB
child arg: npb.bin/mg.E.x
child pid: 29947
===================================
===================================
WaitBasePageMigration_ms 48
WaitHugePageMigration_ms 0
ExchangePages_nr_exchanges 705
ExchangePagesBase_nr_base_pages 18
ExchangePagesHuge_nr_base_pages 0
Fast2Slow_nr_migrations 705
Fast2SlowBasePageMigrations_nr_base_pages 18446744073709551615
Fast2SlowHugePageMigrations_nr_base_pages 0
Slow2Fast_nr_migrations 705
Slow2FastBasePageMigrations_nr_base_pages 101532
Slow2FastHugePageMigrations_nr_base_pages 0
== END ============================
