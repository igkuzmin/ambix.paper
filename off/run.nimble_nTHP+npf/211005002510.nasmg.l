== BEGIN ==========================
Tue 05 Oct 2021 08:28:02 AM WEST
 # setting up vm
vm.migration_batch_size = 8
vm.sysctl_enable_thp_migration = 0
vm.limit_mt_num = 4
never
never
 # setup memory (/sys/fs/cgroup/nimble -> 26Gb)
 # note: 
 # launcher args: --exchange_pages --prefer_memnode --cpunode  0 --fast_mem 0 --slow_mem 2 --dumpstats --dumpstats_period 5 --memory_manage
 # tag: nasmg.l.nimble[nTHP+npf]
 # running nasmg (211005002510: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 2048x2048x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         557.894 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.2004924648216E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           2048x2048x1280
 Iterations      =                       50
 Time in seconds =                  9879.73
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1575.88
 Mop/s/thread    =                    49.25
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


cycles: 24024597546258
real time(ms): 10445380, user time(ms): 327164190, system time(ms): 2282985, virtual cpu time(ms): 329447175
min_flt: 34589494, maj_flt: 21, maxrss: 138361540 KB
child arg: npb.bin/mg.E.x
child pid: 56700
===================================
===================================
WaitBasePageMigration_ms 0
WaitHugePageMigration_ms 0
ExchangePages_nr_exchanges 1823
ExchangePagesBase_nr_base_pages 624
ExchangePagesHuge_nr_base_pages 0
Fast2Slow_nr_migrations 1823
Fast2SlowBasePageMigrations_nr_base_pages 0
Fast2SlowHugePageMigrations_nr_base_pages 0
Slow2Fast_nr_migrations 1823
Slow2FastBasePageMigrations_nr_base_pages 0
Slow2FastHugePageMigrations_nr_base_pages 0
== END ============================
