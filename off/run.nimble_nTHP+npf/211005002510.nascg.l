== BEGIN ==========================
Tue 05 Oct 2021 11:22:07 AM WEST
 # setting up vm
vm.migration_batch_size = 8
vm.sysctl_enable_thp_migration = 0
vm.limit_mt_num = 4
never
never
 # setup memory (/sys/fs/cgroup/nimble -> 26Gb)
 # note: 
 # launcher args: --exchange_pages --prefer_memnode --cpunode  0 --fast_mem 0 --slow_mem 2 --dumpstats --dumpstats_period 5 --memory_manage
 # tag: nascg.l.nimble[nTHP+npf]
 # running nascg (211005002510: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     9000000
 Iterations:                      5
 Number of available threads:    32

 Initialization time =        3292.601 seconds

   iteration           ||r||                 zeta
        1       0.44592577149010E-11  1499.9998413806252
        2       0.11028676461857E-14    72.8774756683727
        3       0.11379085874568E-14    73.2915890919610
        4       0.11834533138884E-14    73.6707042243709
        5       0.12189317005242E-14    74.0174332195390
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.7401743321954E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  9000000
 Iterations      =                        5
 Time in seconds =                 10966.62
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   150.86
 Mop/s/thread    =                     4.71
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


cycles: 32823614714066
real time(ms): 14271005, user time(ms): 452094000, system time(ms): 2913245, virtual cpu time(ms): 455007245
min_flt: 38592796, maj_flt: 44, maxrss: 154374408 KB
child arg: npb.bin/cg.l.x
child pid: 64065
===================================
===================================
WaitBasePageMigration_ms 80
WaitHugePageMigration_ms 0
ExchangePages_nr_exchanges 2747
ExchangePagesBase_nr_base_pages 981
ExchangePagesHuge_nr_base_pages 0
Fast2Slow_nr_migrations 2747
Fast2SlowBasePageMigrations_nr_base_pages 18446744073709551615
Fast2SlowHugePageMigrations_nr_base_pages 0
Slow2Fast_nr_migrations 2751
Slow2FastBasePageMigrations_nr_base_pages 95029
Slow2FastHugePageMigrations_nr_base_pages 0
== END ============================
