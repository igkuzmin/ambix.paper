== BEGIN ==========================
Mon 04 Oct 2021 11:08:40 AM WEST
 # setting up vm
vm.migration_batch_size = 8
vm.sysctl_enable_thp_migration = 0
vm.limit_mt_num = 4
never
never
 # setup memory (/sys/fs/cgroup/nimble -> 26Gb)
 # note: 
 # launcher args: --exchange_pages --prefer_memnode --cpunode  0 --fast_mem 0 --slow_mem 2 --dumpstats --dumpstats_period 5 --memory_manage
 # tag: nasmg.l.nimble[nTHP]
 # running nasmg (211003202052: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 2048x2048x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         552.154 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.2004924648216E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           2048x2048x1280
 Iterations      =                       50
 Time in seconds =                  9810.57
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1586.99
 Mop/s/thread    =                    49.59
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


cycles: 23852719499876
real time(ms): 10370646, user time(ms): 324779116, system time(ms): 2153638, virtual cpu time(ms): 326932754
min_flt: 34589851, maj_flt: 28, maxrss: 138361432 KB
child arg: npb.bin/mg.E.x
child pid: 55182
===================================
# started on Mon Oct  4 11:08:41 2021


 Performance counter stats for 'system wide':

        252,258.06 Joules power/energy-ram/                                           
      1,342,479.12 Joules power/energy-pkg/                                           

   10370.683517666 seconds time elapsed

===================================
WaitBasePageMigration_ms 180
WaitHugePageMigration_ms 0
ExchangePages_nr_exchanges 1901
ExchangePagesBase_nr_base_pages 1872
ExchangePagesHuge_nr_base_pages 0
Fast2Slow_nr_migrations 1901
Fast2SlowBasePageMigrations_nr_base_pages 18446744073709551615
Fast2SlowHugePageMigrations_nr_base_pages 0
Slow2Fast_nr_migrations 1901
Slow2FastBasePageMigrations_nr_base_pages 95504
Slow2FastHugePageMigrations_nr_base_pages 0
== END ============================
