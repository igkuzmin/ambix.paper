== BEGIN ==========================
Sat 09 Oct 2021 11:10:01 AM WEST
 # setting up vm
vm.migration_batch_size = 8
vm.sysctl_enable_thp_migration = 0
vm.limit_mt_num = 4
never
never
 # setup memory (/sys/fs/cgroup/nimble -> 26Gb)
 # note: 
 # launcher args: --exchange_pages --prefer_memnode --cpunode  0 --fast_mem 0 --slow_mem 2 --dumpstats --dumpstats_period 5 --memory_manage
 # tag: nasft.s.nimble[nTHP]
 # running nasft (211009104939: Size s)
== BEFORE =========================
R_DRAM: 452972
W_DRAM: 33848
R_OPT: 0
W_OPT: 0


 NAS Parallel Benchmarks (NPB3.4-OMP) - FT Benchmark

 Size                : 1024x1024x 512
 Iterations                  :     25
 Number of available threads :     32

 T =    1     Checksum =    5.100021384457D+02    5.102354099484D+02
 T =    2     Checksum =    5.107077402572D+02    5.110372633663D+02
 T =    3     Checksum =    5.111258943980D+02    5.113134002360D+02
 T =    4     Checksum =    5.113745426197D+02    5.114685939810D+02
 T =    5     Checksum =    5.115371222930D+02    5.115707374808D+02
 T =    6     Checksum =    5.116520247117D+02    5.116449517887D+02
 T =    7     Checksum =    5.117383844125D+02    5.117031961855D+02
 T =    8     Checksum =    5.118063403327D+02    5.117514932905D+02
 T =    9     Checksum =    5.118615759864D+02    5.117929844143D+02
 T =   10     Checksum =    5.119074780580D+02    5.118293855692D+02
 T =   11     Checksum =    5.119461989515D+02    5.118616995522D+02
 T =   12     Checksum =    5.119791954308D+02    5.118905677615D+02
 T =   13     Checksum =    5.120075109022D+02    5.119164444681D+02
 T =   14     Checksum =    5.120319290468D+02    5.119396827497D+02
 T =   15     Checksum =    5.120530609872D+02    5.119605764397D+02
 T =   16     Checksum =    5.120713970144D+02    5.119793803911D+02
 T =   17     Checksum =    5.120873387758D+02    5.119963202810D+02
 T =   18     Checksum =    5.121012203067D+02    5.120115975573D+02
 T =   19     Checksum =    5.121133224506D+02    5.120253922561D+02
 T =   20     Checksum =    5.121238832012D+02    5.120378649681D+02
 T =   21     Checksum =    5.121331054187D+02    5.120491585092D+02
 T =   22     Checksum =    5.121411627798D+02    5.120593995031D+02
 T =   23     Checksum =    5.121482044900D+02    5.120686999316D+02
 T =   24     Checksum =    5.121543590893D+02    5.120771586436D+02
 T =   25     Checksum =    5.121597375743D+02    5.120848627974D+02
 class = U


 FT Benchmark Completed.
 Class           =                        U
 Size            =           1024x1024x 512
 Iterations      =                       25
 Time in seconds =                   167.65
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                 12534.75
 Mop/s/thread    =                   391.71
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              13 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


cycles: 407059151670
real time(ms): 176981, user time(ms): 5521833, system time(ms): 55521, virtual cpu time(ms): 5577354
min_flt: 5257190, maj_flt: 1, maxrss: 21033176 KB
child arg: npb.bin/ft.s.x
child pid: 4681
== AFTER ==========================
R_DRAM: 510979
W_DRAM: 68757
R_OPT: 0
W_OPT: 0
===================================
# started on Sat Oct  9 11:10:01 2021


 Performance counter stats for 'system wide':

          5,914.38 Joules power/energy-ram/                                           
         27,263.94 Joules power/energy-pkg/                                           

     176.984878157 seconds time elapsed

===================================
WaitBasePageMigration_ms 0
WaitHugePageMigration_ms 0
ExchangePages_nr_exchanges 0
ExchangePagesBase_nr_base_pages 0
ExchangePagesHuge_nr_base_pages 0
Fast2Slow_nr_migrations 0
Fast2SlowBasePageMigrations_nr_base_pages 0
Fast2SlowHugePageMigrations_nr_base_pages 0
Slow2Fast_nr_migrations 35
Slow2FastBasePageMigrations_nr_base_pages 0
Slow2FastHugePageMigrations_nr_base_pages 0
== END ============================
