== BEGIN ==========================
Sat 09 Oct 2021 11:12:58 AM WEST
 # setting up vm
vm.migration_batch_size = 8
vm.sysctl_enable_thp_migration = 0
vm.limit_mt_num = 4
never
never
 # setup memory (/sys/fs/cgroup/nimble -> 26Gb)
 # note: 
 # launcher args: --exchange_pages --prefer_memnode --cpunode  0 --fast_mem 0 --slow_mem 2 --dumpstats --dumpstats_period 5 --memory_manage
 # tag: nasmg.s.nimble[nTHP]
 # running nasmg (211009104939: Size s)
== BEFORE =========================
R_DRAM: 510979
W_DRAM: 68757
R_OPT: 0
W_OPT: 0


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1024x1024x1024  (class D)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:          12.827 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 VERIFICATION FAILED
 L2 Norm is              0.1590005169528E-09
 The correct L2 Norm is  0.1583275060440E-09


 MG Benchmark Completed.
 Class           =                        D
 Size            =           1024x1024x1024
 Iterations      =                       50
 Time in seconds =                   352.80
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  8826.01
 Mop/s/thread    =                   275.81
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


cycles: 843950068790
real time(ms): 366930, user time(ms): 11528935, system time(ms): 79882, virtual cpu time(ms): 11608817
min_flt: 6936982, maj_flt: 1, maxrss: 27750140 KB
child arg: npb.bin/mg.E.x
child pid: 5050
== AFTER ==========================
R_DRAM: 638446
W_DRAM: 101449
R_OPT: 2697
W_OPT: 1088
===================================
# started on Sat Oct  9 11:12:59 2021


 Performance counter stats for 'system wide':

         11,760.99 Joules power/energy-ram/                                           
         49,715.87 Joules power/energy-pkg/                                           

     366.933962940 seconds time elapsed

===================================
WaitBasePageMigration_ms 48
WaitHugePageMigration_ms 0
ExchangePages_nr_exchanges 73
ExchangePagesBase_nr_base_pages 1362
ExchangePagesHuge_nr_base_pages 0
Fast2Slow_nr_migrations 73
Fast2SlowBasePageMigrations_nr_base_pages 18446744073709551614
Fast2SlowHugePageMigrations_nr_base_pages 0
Slow2Fast_nr_migrations 73
Slow2FastBasePageMigrations_nr_base_pages 1094
Slow2FastHugePageMigrations_nr_base_pages 0
== END ============================
