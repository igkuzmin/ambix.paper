== BEGIN ==========================
Sun 03 Oct 2021 09:37:49 PM WEST
 # setting up vm
vm.migration_batch_size = 8
vm.sysctl_enable_thp_migration = 0
vm.limit_mt_num = 4
never
never
 # setup memory (/sys/fs/cgroup/nimble -> 26Gb)
 # note: 
 # launcher args: --exchange_pages --prefer_memnode --cpunode  0 --fast_mem 0 --slow_mem 2 --dumpstats --dumpstats_period 5 --memory_manage
 # tag: nasmg.m.nimble[nTHP]
 # running nasmg (211003202052: Size m)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1536x1536x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         253.564 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.1746922003355E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           1536x1536x1280
 Iterations      =                       50
 Time in seconds =                  4356.17
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  2010.41
 Mop/s/thread    =                    62.83
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


cycles: 10615523161082
real time(ms): 4615406, user time(ms): 145152499, system time(ms): 1071920, virtual cpu time(ms): 146224419
min_flt: 19471490, maj_flt: 19, maxrss: 77887908 KB
child arg: npb.bin/mg.E.x
child pid: 63317
===================================
# started on Sun Oct  3 21:37:49 2021


 Performance counter stats for 'system wide':

        114,645.90 Joules power/energy-ram/                                           
        608,416.86 Joules power/energy-pkg/                                           

    4615.410285746 seconds time elapsed

===================================
WaitBasePageMigration_ms 24
WaitHugePageMigration_ms 0
ExchangePages_nr_exchanges 672
ExchangePagesBase_nr_base_pages 636
ExchangePagesHuge_nr_base_pages 0
Fast2Slow_nr_migrations 672
Fast2SlowBasePageMigrations_nr_base_pages 0
Fast2SlowHugePageMigrations_nr_base_pages 0
Slow2Fast_nr_migrations 672
Slow2FastBasePageMigrations_nr_base_pages 0
Slow2FastHugePageMigrations_nr_base_pages 0
== END ============================
