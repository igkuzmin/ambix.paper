== BEGIN ==========================
Sun 03 Oct 2021 10:54:45 PM WEST
 # setting up vm
vm.migration_batch_size = 8
vm.sysctl_enable_thp_migration = 0
vm.limit_mt_num = 4
never
never
 # setup memory (/sys/fs/cgroup/nimble -> 26Gb)
 # note: 
 # launcher args: --exchange_pages --prefer_memnode --cpunode  0 --fast_mem 0 --slow_mem 2 --dumpstats --dumpstats_period 5 --memory_manage
 # tag: nascg.m.nimble[nTHP]
 # running nascg (211003202052: Size m)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         191.434 seconds

   iteration           ||r||                 zeta
        1       0.21274498209534E-11   999.9996857982868
        2       0.13449968484270E-14    57.8675766650496
        3       0.13442798575389E-14    58.2941206350796
        4       0.13463865381843E-14    58.6783531949450
        5       0.13425297015172E-14    59.0239484906583
        6       0.13347408010222E-14    59.3343718346763
        7       0.13233181060039E-14    59.6128667120510
        8       0.13138717767465E-14    59.8624490951617
        9       0.13008147170912E-14    60.0859075092872
       10       0.12901618369758E-14    60.2858074951204
       11       0.12794284639505E-14    60.4644993187783
       12       0.12692737977994E-14    60.6241279736674
       13       0.12626576467614E-14    60.7666446959979
       14       0.12538484250665E-14    60.8938193721787
       15       0.12484274579886E-14    61.0072533517975
       16       0.12427006222199E-14    61.1083922942584
       17       0.12373784853588E-14    61.1985387720225
       18       0.12334758837819E-14    61.2788644311380
       19       0.12277345753476E-14    61.3504215716458
       20       0.12242955796587E-14    61.4141540599921
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                  2092.82
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   830.44
 Mop/s/thread    =                    25.95
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


cycles: 7175615749966
real time(ms): 3119803, user time(ms): 72592600, system time(ms): 366802, virtual cpu time(ms): 72959402
min_flt: 10172348, maj_flt: 65, maxrss: 40691588 KB
child arg: npb.bin/cg.m.x
child pid: 2621
===================================
# started on Sun Oct  3 22:54:45 2021


 Performance counter stats for 'system wide':

         74,695.32 Joules power/energy-ram/                                           
        380,114.29 Joules power/energy-pkg/                                           

    3119.847055032 seconds time elapsed

===================================
WaitBasePageMigration_ms 28
WaitHugePageMigration_ms 0
ExchangePages_nr_exchanges 1
ExchangePagesBase_nr_base_pages 3082
ExchangePagesHuge_nr_base_pages 0
Fast2Slow_nr_migrations 1
Fast2SlowBasePageMigrations_nr_base_pages 18446744073709551615
Fast2SlowHugePageMigrations_nr_base_pages 0
Slow2Fast_nr_migrations 14
Slow2FastBasePageMigrations_nr_base_pages 186370
Slow2FastHugePageMigrations_nr_base_pages 0
== END ============================
