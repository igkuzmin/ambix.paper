== BEGIN ==========================
Mon 04 Oct 2021 02:01:32 PM WEST
 # setting up vm
vm.migration_batch_size = 8
vm.sysctl_enable_thp_migration = 0
vm.limit_mt_num = 4
never
never
 # setup memory (/sys/fs/cgroup/nimble -> 26Gb)
 # note: 
 # launcher args: --exchange_pages --prefer_memnode --cpunode  0 --fast_mem 0 --slow_mem 2 --dumpstats --dumpstats_period 5 --memory_manage
 # tag: nascg.l.nimble[nTHP]
 # running nascg (211003202052: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     9000000
 Iterations:                      5
 Number of available threads:    32

 Initialization time =        3288.335 seconds

   iteration           ||r||                 zeta
        1       0.44592679407747E-11  1499.9998413806252
        2       0.11029793165709E-14    72.8774756683727
        3       0.11378797694130E-14    73.2915890919608
        4       0.11834910713859E-14    73.6707042243700
        5       0.12190055071253E-14    74.0174332195386
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.7401743321954E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  9000000
 Iterations      =                        5
 Time in seconds =                 10962.88
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   150.92
 Mop/s/thread    =                     4.72
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


cycles: 32797346853750
real time(ms): 14259585, user time(ms): 452162362, system time(ms): 2884667, virtual cpu time(ms): 455047029
min_flt: 38592888, maj_flt: 48, maxrss: 154374528 KB
child arg: npb.bin/cg.l.x
child pid: 62518
===================================
# started on Mon Oct  4 14:01:32 2021


 Performance counter stats for 'system wide':

        337,914.03 Joules power/energy-ram/                                           
      1,815,220.22 Joules power/energy-pkg/                                           

   14259.640535964 seconds time elapsed

===================================
WaitBasePageMigration_ms 20
WaitHugePageMigration_ms 0
ExchangePages_nr_exchanges 2640
ExchangePagesBase_nr_base_pages 2748
ExchangePagesHuge_nr_base_pages 0
Fast2Slow_nr_migrations 2640
Fast2SlowBasePageMigrations_nr_base_pages 18446744073709551613
Fast2SlowHugePageMigrations_nr_base_pages 0
Slow2Fast_nr_migrations 2644
Slow2FastBasePageMigrations_nr_base_pages 99425
Slow2FastHugePageMigrations_nr_base_pages 0
== END ============================
