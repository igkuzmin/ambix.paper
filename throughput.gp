if (!exists('wd')) wd=12
if (!exists('hd')) hd=5
if (!exists('tags')) tags = 'Ambix AutoNUMA MemM'

set terminal postscript eps enhanced color solid size wd,hd

set datafile separator ';'
set grid ytics

set ylabel "Speedup (vs. ADM-default)"
#set xlabel 'Workload'


#set xtics rotate by -25
set style data histograms
set style histogram clustered gap 2
set style fill solid
set boxwidth .95
set key bmargin center  horizontal

#set title "Throughput performance improvement"
#subs(n) = `echo 123 |sed 's/+/ /;'`
unset xrange
plot  1 title "ADM-default (baseline)" lc rgb "#fb3d3d" lw 2, \
     for [i in tags] input_data using 2:xtic(1) index i title i

# vim: ft=gnuplot
