== BEGIN ==========================
Wed 06 Oct 2021 10:25:00 PM WEST
 # note: 
 # tag: nascg.m.autonuma
 # running nascg (211006191255: Size m)
== BEFORE =========================
R_DRAM: 1506319
W_DRAM: 313458
R_OPT: 360094
W_OPT: 127729


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         118.554 seconds

   iteration           ||r||                 zeta
        1       0.21274314662158E-11   999.9996857982868
        2       0.13450188636907E-14    57.8675766650496
        3       0.13442927030260E-14    58.2941206350796
        4       0.13464659942552E-14    58.6783531949446
        5       0.13424532949950E-14    59.0239484906589
        6       0.13347816204308E-14    59.3343718346765
        7       0.13232369423945E-14    59.6128667120510
        8       0.13140089765369E-14    59.8624490951621
        9       0.13007216491975E-14    60.0859075092869
       10       0.12902439913360E-14    60.2858074951204
       11       0.12793339746480E-14    60.4644993187778
       12       0.12693046301308E-14    60.6241279736679
       13       0.12625345050594E-14    60.7666446959979
       14       0.12540419295984E-14    60.8938193721784
       15       0.12484215307283E-14    61.0072533517978
       16       0.12426959718360E-14    61.1083922942576
       17       0.12372701748949E-14    61.1985387720231
       18       0.12334545651357E-14    61.2788644311375
       19       0.12276724400638E-14    61.3504215716462
       20       0.12243050105910E-14    61.4141540599923
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                   507.61
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  3423.81
 Mop/s/thread    =                   106.99
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 1678247
W_DRAM: 319124
R_OPT: 424456
W_OPT: 127975
===================================
# started on Wed Oct  6 22:25:00 2021


 Performance counter stats for 'system wide':

         19,824.93 Joules power/energy-ram/                                           
         89,711.80 Joules power/energy-pkg/                                           

     626.838992906 seconds time elapsed

== END ============================
