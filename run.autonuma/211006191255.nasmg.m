== BEGIN ==========================
Wed 06 Oct 2021 09:06:57 PM WEST
 # note: 
 # tag: nasmg.m.autonuma
 # running nasmg (211006191255: Size m)
== BEFORE =========================
R_DRAM: 1294739
W_DRAM: 273425
R_OPT: 162972
W_OPT: 74697


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1536x1536x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         244.939 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.1746922003355E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           1536x1536x1280
 Iterations      =                       50
 Time in seconds =                  4435.12
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1974.63
 Mop/s/thread    =                    61.71
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 1506319
W_DRAM: 313458
R_OPT: 360094
W_OPT: 127729
===================================
# started on Wed Oct  6 21:06:58 2021


 Performance counter stats for 'system wide':

        115,883.00 Joules power/energy-ram/                                           
        601,679.19 Joules power/energy-pkg/                                           

    4682.185956193 seconds time elapsed

== END ============================
