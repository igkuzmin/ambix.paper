== BEGIN ==========================
Wed 06 Oct 2021 07:36:06 PM WEST
 # note: 
 # tag: nasmg.s.autonuma
 # running nasmg (211006191255: Size s)
== BEFORE =========================
R_DRAM: 608211
W_DRAM: 153334
R_OPT: 37817
W_OPT: 25982


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1024x1024x1024  (class D)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:          11.700 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 VERIFICATION FAILED
 L2 Norm is              0.1590005169528E-09
 The correct L2 Norm is  0.1583275060440E-09


 MG Benchmark Completed.
 Class           =                        D
 Size            =           1024x1024x1024
 Iterations      =                       50
 Time in seconds =                   286.33
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                 10875.16
 Mop/s/thread    =                   339.85
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 737404
W_DRAM: 187563
R_OPT: 37853
W_OPT: 25988
===================================
# started on Wed Oct  6 19:36:06 2021


 Performance counter stats for 'system wide':

         10,229.72 Joules power/energy-ram/                                           
         41,144.98 Joules power/energy-pkg/                                           

     298.218747682 seconds time elapsed

== END ============================
