== BEGIN ==========================
Wed 06 Oct 2021 11:01:50 PM WEST
 # note: 
 # tag: nascg.l.autonuma
 # running nascg (211006191255: Size l)
== BEFORE =========================
R_DRAM: 1725329
W_DRAM: 334265
R_OPT: 443220
W_OPT: 141235


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     9000000
 Iterations:                      5
 Number of available threads:    32

 Initialization time =        3291.357 seconds

   iteration           ||r||                 zeta
        1       0.44592423905584E-11  1499.9998413806252
        2       0.11050483350716E-14    72.8774756683724
        3       0.11378494978656E-14    73.2915890919610
        4       0.11834703608443E-14    73.6707042243700
        5       0.12189940511159E-14    74.0174332195390
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.7401743321954E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  9000000
 Iterations      =                        5
 Time in seconds =                  2870.78
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   576.31
 Mop/s/thread    =                    18.01
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 2249613
W_DRAM: 343660
R_OPT: 819322
W_OPT: 154080
===================================
# started on Wed Oct  6 23:01:51 2021


 Performance counter stats for 'system wide':

        157,917.40 Joules power/energy-ram/                                           
        797,804.34 Joules power/energy-pkg/                                           

    6168.774481809 seconds time elapsed

== END ============================
