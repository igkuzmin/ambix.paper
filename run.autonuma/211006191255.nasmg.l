== BEGIN ==========================
Thu 07 Oct 2021 01:55:17 AM WEST
 # note: 
 # tag: nasmg.l.autonuma
 # running nasmg (211006191255: Size l)
== BEFORE =========================
R_DRAM: 2359743
W_DRAM: 362698
R_OPT: 870110
W_OPT: 192686


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 2048x2048x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         541.651 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.2004924648216E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           2048x2048x1280
 Iterations      =                       50
 Time in seconds =                  9823.02
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1584.98
 Mop/s/thread    =                    49.53
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 2736865
W_DRAM: 414026
R_OPT: 1331354
W_OPT: 309322
===================================
# started on Thu Oct  7 01:55:17 2021


 Performance counter stats for 'system wide':

        253,268.86 Joules power/energy-ram/                                           
      1,341,526.47 Joules power/energy-pkg/                                           

   10369.232600036 seconds time elapsed

== END ============================
