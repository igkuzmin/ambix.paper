== BEGIN ==========================
Wed 29 Sep 2021 08:44:26 PM WEST
 # note: 
 # tag: nascg.m.base
 # running nascg (210929173302: Size m)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         196.126 seconds

   iteration           ||r||                 zeta
        1       0.21274465851313E-11   999.9996857982868
        2       0.13467224025749E-14    57.8675766650496
        3       0.13442237434445E-14    58.2941206350790
        4       0.13464957857992E-14    58.6783531949449
        5       0.13425577701820E-14    59.0239484906583
        6       0.13347494824066E-14    59.3343718346762
        7       0.13234149119768E-14    59.6128667120508
        8       0.13138966240023E-14    59.8624490951621
        9       0.13006913514723E-14    60.0859075092866
       10       0.12903448432751E-14    60.2858074951206
       11       0.12794080970607E-14    60.4644993187784
       12       0.12693718673447E-14    60.6241279736682
       13       0.12625052566600E-14    60.7666446959977
       14       0.12539546157450E-14    60.8938193721784
       15       0.12484046244643E-14    61.0072533517981
       16       0.12426420105030E-14    61.1083922942578
       17       0.12371746059504E-14    61.1985387720231
       18       0.12334930783429E-14    61.2788644311374
       19       0.12277313292890E-14    61.3504215716455
       20       0.12243774053997E-14    61.4141540599919
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                  2234.87
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   777.66
 Mop/s/thread    =                    24.30
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Wed Sep 29 20:44:26 2021


 Performance counter stats for 'system wide':

         60,969.28 Joules power/energy-ram/                                           
        312,167.78 Joules power/energy-pkg/                                           

    2433.743704502 seconds time elapsed

== END ============================
