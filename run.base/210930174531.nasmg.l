== BEGIN ==========================
Thu 30 Sep 2021 05:45:31 PM WEST
 # note: 
 # tag: nasmg.l.base
 # running nasmg (210930174531: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 2048x2048x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         549.492 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.2004924648216E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           2048x2048x1280
 Iterations      =                       50
 Time in seconds =                  9859.87
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1579.05
 Mop/s/thread    =                    49.35
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Thu Sep 30 17:45:31 2021


 Performance counter stats for 'system wide':

        250,510.68 Joules power/energy-ram/                                           
      1,318,150.94 Joules power/energy-pkg/                                           

   10416.408448220 seconds time elapsed

== END ============================
