== BEGIN ==========================
Tue 28 Sep 2021 06:29:23 AM WEST
 # note: 
 # tag: nasmg.s.base
 # running nasmg (210928061314: Size s)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1024x1024x1024  (class D)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:          37.820 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 VERIFICATION FAILED
 L2 Norm is              0.1590005169528E-09
 The correct L2 Norm is  0.1583275060440E-09


 MG Benchmark Completed.
 Class           =                        D
 Size            =           1024x1024x1024
 Iterations      =                       50
 Time in seconds =                   310.74
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                 10020.83
 Mop/s/thread    =                   313.15
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Tue Sep 28 06:29:24 2021


 Performance counter stats for 'system wide':

         11,516.92 Joules power/energy-ram/                                           
         48,107.76 Joules power/energy-pkg/                                           

     348.562562740 seconds time elapsed

== END ============================
