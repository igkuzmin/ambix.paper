== BEGIN ==========================
Sun 07 Nov 2021 08:38:56 PM WET
 # note: 
 # tag: nascg.m.base
 # running nascg (211107154745: Size m)
== BEFORE =========================
R_DRAM: 5505658
W_DRAM: 2160965
R_OPT: 1733290
W_OPT: 402762


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         195.773 seconds

   iteration           ||r||                 zeta
        1       0.21275023813715E-11   999.9996857982868
        2       0.13479554363625E-14    57.8675766650500
        3       0.13442004996132E-14    58.2941206350796
        4       0.13463218637146E-14    58.6783531949446
        5       0.13425127621891E-14    59.0239484906583
        6       0.13347864574700E-14    59.3343718346763
        7       0.13233661662297E-14    59.6128667120514
        8       0.13139489674398E-14    59.8624490951619
        9       0.13008155089231E-14    60.0859075092861
       10       0.12902710280042E-14    60.2858074951207
       11       0.12794846773644E-14    60.4644993187784
       12       0.12692336083737E-14    60.6241279736677
       13       0.12625279038325E-14    60.7666446959977
       14       0.12538987366567E-14    60.8938193721789
       15       0.12482668494154E-14    61.0072533517983
       16       0.12425797798991E-14    61.1083922942570
       17       0.12370854542653E-14    61.1985387720226
       18       0.12335495050883E-14    61.2788644311380
       19       0.12278094929184E-14    61.3504215716455
       20       0.12241852747823E-14    61.4141540599921
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                  2210.74
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   786.14
 Mop/s/thread    =                    24.57
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 5580938
W_DRAM: 2165613
R_OPT: 1929235
W_OPT: 403018
===================================
# started on Sun Nov  7 20:38:56 2021


 Performance counter stats for 'system wide':

         59,606.79 Joules power/energy-ram/                                           
        303,245.20 Joules power/energy-pkg/                                           

    2409.249834800 seconds time elapsed

== END ============================
== BEGIN ==========================
Sun 07 Nov 2021 11:20:47 PM WET
 # note: 
 # tag: nascg.m.base
 # running nascg (211107154745: Size m)
== BEFORE =========================
R_DRAM: 5894443
W_DRAM: 2211494
R_OPT: 2097055
W_OPT: 481997


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         181.831 seconds

   iteration           ||r||                 zeta
        1       0.21274142816809E-11   999.9996857982868
        2       0.13450158965890E-14    57.8675766650498
        3       0.13442708726205E-14    58.2941206350794
        4       0.13465282741998E-14    58.6783531949449
        5       0.13425425024597E-14    59.0239484906585
        6       0.13347440089357E-14    59.3343718346763
        7       0.13233058985796E-14    59.6128667120508
        8       0.13139241189372E-14    59.8624490951617
        9       0.13007335286051E-14    60.0859075092864
       10       0.12902889674850E-14    60.2858074951201
       11       0.12794410142216E-14    60.4644993187781
       12       0.12693555295062E-14    60.6241279736674
       13       0.12625450934856E-14    60.7666446959975
       14       0.12540258267451E-14    60.8938193721790
       15       0.12483760960959E-14    61.0072533517979
       16       0.12426066936254E-14    61.1083922942578
       17       0.12372956460327E-14    61.1985387720223
       18       0.12335194562521E-14    61.2788644311374
       19       0.12277111992849E-14    61.3504215716453
       20       0.12242626140308E-14    61.4141540599921
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                  1933.03
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   899.09
 Mop/s/thread    =                    28.10
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


== AFTER ==========================
R_DRAM: 5968650
W_DRAM: 2216040
R_OPT: 2279526
W_OPT: 482255
===================================
# started on Sun Nov  7 23:20:47 2021


 Performance counter stats for 'system wide':

         52,914.34 Joules power/energy-ram/                                           
        268,719.40 Joules power/energy-pkg/                                           

    2117.179105342 seconds time elapsed

== END ============================
