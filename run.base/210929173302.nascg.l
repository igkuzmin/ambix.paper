== BEGIN ==========================
Wed 29 Sep 2021 09:25:00 PM WEST
 # note: 
 # tag: nascg.l.base
 # running nascg (210929173302: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     9000000
 Iterations:                      5
 Number of available threads:    32

 Initialization time =        3295.866 seconds

   iteration           ||r||                 zeta
        1       0.44593067599068E-11  1499.9998413806252
        2       0.11025285476739E-14    72.8774756683717
        3       0.11378396538218E-14    73.2915890919608
        4       0.11834857612480E-14    73.6707042243702
        5       0.12189210984969E-14    74.0174332195388
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.7401743321954E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  9000000
 Iterations      =                        5
 Time in seconds =                 10991.88
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   150.52
 Mop/s/thread    =                     4.70
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Wed Sep 29 21:25:00 2021


 Performance counter stats for 'system wide':

        336,079.27 Joules power/energy-ram/                                           
      1,797,075.56 Joules power/energy-pkg/                                           

   14287.770906329 seconds time elapsed

== END ============================
