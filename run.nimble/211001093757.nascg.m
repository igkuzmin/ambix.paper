== BEGIN ==========================
Fri 01 Oct 2021 12:31:49 PM WEST
 # setting up vm
vm.migration_batch_size = 8
vm.sysctl_enable_thp_migration = 1
vm.limit_mt_num = 4
always
never
 # setup memory (/sys/fs/cgroup/nimble -> 26Gb)
 # note: 
 # launcher args: --exchange_pages --prefer_memnode --cpunode  0 --fast_mem 0 --slow_mem 2 --dumpstats --dumpstats_period 5 --memory_manage
 # tag: nascg.m.nimble
 # running nascg (211001093757: Size m)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         157.969 seconds

   iteration           ||r||                 zeta
        1       0.21274439084045E-11   999.9996857982868
        2       0.13450074671879E-14    57.8675766650496
        3       0.13442221737552E-14    58.2941206350796
        4       0.13464606251596E-14    58.6783531949446
        5       0.13424592799228E-14    59.0239484906585
        6       0.13347776849569E-14    59.3343718346763
        7       0.13233062883217E-14    59.6128667120508
        8       0.13139549390621E-14    59.8624490951622
        9       0.13008294444083E-14    60.0859075092866
       10       0.12902973295915E-14    60.2858074951204
       11       0.12794762250680E-14    60.4644993187778
       12       0.12693165454211E-14    60.6241279736682
       13       0.12626449261780E-14    60.7666446959973
       14       0.12539329808518E-14    60.8938193721789
       15       0.12483246155599E-14    61.0072533517981
       16       0.12425620905468E-14    61.1083922942576
       17       0.12370982393115E-14    61.1985387720232
       18       0.12334455755398E-14    61.2788644311378
       19       0.12276555781027E-14    61.3504215716456
       20       0.12243803042232E-14    61.4141540599921
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                  1207.84
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1438.90
 Mop/s/thread    =                    44.97
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


cycles: 3141724863280
real time(ms): 1365945, user time(ms): 43149864, system time(ms): 433018, virtual cpu time(ms): 43582882
min_flt: 108062, maj_flt: 1, maxrss: 40694244 KB
child arg: npb.bin/cg.m.x
child pid: 17157
===================================
# started on Fri Oct  1 12:31:49 2021


 Performance counter stats for 'system wide':

         35,527.10 Joules power/energy-ram/                                           
        175,733.30 Joules power/energy-pkg/                                           

    1365.948513080 seconds time elapsed

===================================
WaitBasePageMigration_ms 48
WaitHugePageMigration_ms 24
ExchangePages_nr_exchanges 260
ExchangePagesBase_nr_base_pages 0
ExchangePagesHuge_nr_base_pages 0
Fast2Slow_nr_migrations 260
Fast2SlowBasePageMigrations_nr_base_pages 1582
Fast2SlowHugePageMigrations_nr_base_pages 0
Slow2Fast_nr_migrations 273
Slow2FastBasePageMigrations_nr_base_pages 1695
Slow2FastHugePageMigrations_nr_base_pages 31232
== END ============================
