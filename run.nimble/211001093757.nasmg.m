== BEGIN ==========================
Fri 01 Oct 2021 11:13:09 AM WEST
 # setting up vm
vm.migration_batch_size = 8
vm.sysctl_enable_thp_migration = 1
vm.limit_mt_num = 4
always
never
 # setup memory (/sys/fs/cgroup/nimble -> 26Gb)
 # note: 
 # launcher args: --exchange_pages --prefer_memnode --cpunode  0 --fast_mem 0 --slow_mem 2 --dumpstats --dumpstats_period 5 --memory_manage
 # tag: nasmg.m.nimble
 # running nasmg (211001093757: Size m)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1536x1536x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         289.509 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.1746922003355E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           1536x1536x1280
 Iterations      =                       50
 Time in seconds =                  4429.15
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1977.29
 Mop/s/thread    =                    61.79
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


cycles: 10853834257664
real time(ms): 4719019, user time(ms): 148387153, system time(ms): 1177756, virtual cpu time(ms): 149564909
min_flt: 105132, maj_flt: 19, maxrss: 77891904 KB
child arg: npb.bin/mg.E.x
child pid: 10930
===================================
# started on Fri Oct  1 11:13:10 2021


 Performance counter stats for 'system wide':

        115,255.80 Joules power/energy-ram/                                           
        588,396.49 Joules power/energy-pkg/                                           

    4719.022819636 seconds time elapsed

===================================
WaitBasePageMigration_ms 0
WaitHugePageMigration_ms 0
ExchangePages_nr_exchanges 943
ExchangePagesBase_nr_base_pages 0
ExchangePagesHuge_nr_base_pages 0
Fast2Slow_nr_migrations 943
Fast2SlowBasePageMigrations_nr_base_pages 615
Fast2SlowHugePageMigrations_nr_base_pages 0
Slow2Fast_nr_migrations 943
Slow2FastBasePageMigrations_nr_base_pages 354
Slow2FastHugePageMigrations_nr_base_pages 0
== END ============================
