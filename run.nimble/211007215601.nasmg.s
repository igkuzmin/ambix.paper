== BEGIN ==========================
Thu 07 Oct 2021 10:34:46 PM WEST
 # setting up vm
vm.migration_batch_size = 8
vm.sysctl_enable_thp_migration = 1
vm.limit_mt_num = 4
always
never
 # setup memory (/sys/fs/cgroup/nimble -> 26Gb)
 # note: 
 # launcher args: --exchange_pages --prefer_memnode --cpunode  0 --fast_mem 0 --slow_mem 2 --dumpstats --dumpstats_period 5 --memory_manage
 # tag: nasmg.s.nimble
 # running nasmg (211007215601: Size s)
== BEFORE =========================
R_DRAM: 78366
W_DRAM: 16794
R_OPT: 42598
W_OPT: 25292


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1024x1024x1024  (class D)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:          13.462 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 VERIFICATION FAILED
 L2 Norm is              0.1590005169528E-09
 The correct L2 Norm is  0.1583275060440E-09


 MG Benchmark Completed.
 Class           =                        D
 Size            =           1024x1024x1024
 Iterations      =                       50
 Time in seconds =                   356.14
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  8743.27
 Mop/s/thread    =                   273.23
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


cycles: 850611493430
real time(ms): 369831, user time(ms): 11711114, system time(ms): 44821, virtual cpu time(ms): 11755935
min_flt: 14723, maj_flt: 19, maxrss: 27752220 KB
child arg: npb.bin/mg.E.x
child pid: 6168
== AFTER ==========================
R_DRAM: 204828
W_DRAM: 49830
R_OPT: 45206
W_OPT: 26306
===================================
# started on Thu Oct  7 22:34:46 2021


 Performance counter stats for 'system wide':

         11,842.17 Joules power/energy-ram/                                           
         49,816.61 Joules power/energy-pkg/                                           

     369.834342056 seconds time elapsed

===================================
WaitBasePageMigration_ms 0
WaitHugePageMigration_ms 0
ExchangePages_nr_exchanges 72
ExchangePagesBase_nr_base_pages 0
ExchangePagesHuge_nr_base_pages 0
Fast2Slow_nr_migrations 72
Fast2SlowBasePageMigrations_nr_base_pages 2833
Fast2SlowHugePageMigrations_nr_base_pages 0
Slow2Fast_nr_migrations 73
Slow2FastBasePageMigrations_nr_base_pages 2305
Slow2FastHugePageMigrations_nr_base_pages 0
== END ============================
