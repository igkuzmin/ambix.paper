== BEGIN ==========================
Fri 08 Oct 2021 01:43:12 PM WEST
 # setting up vm
vm.migration_batch_size = 8
vm.sysctl_enable_thp_migration = 1
vm.limit_mt_num = 4
always
never
 # setup memory (/sys/fs/cgroup/nimble -> 26Gb)
 # note: 
 # launcher args: --exchange_pages --prefer_memnode --cpunode  0 --fast_mem 0 --slow_mem 2 --dumpstats --dumpstats_period 5 --memory_manage
 # tag: nasmg.l.nimble
 # running nasmg (211007215601: Size l)
== BEFORE =========================
R_DRAM: 1134151
W_DRAM: 198099
R_OPT: 1595615
W_OPT: 587212


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
