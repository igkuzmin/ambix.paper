== BEGIN ==========================
Sat 09 Oct 2021 11:19:06 AM WEST
 # setting up vm
vm.migration_batch_size = 8
vm.sysctl_enable_thp_migration = 1
vm.limit_mt_num = 4
always
never
 # setup memory (/sys/fs/cgroup/nimble -> 26Gb)
 # note: 
 # launcher args: --exchange_pages --prefer_memnode --cpunode  0 --fast_mem 0 --slow_mem 2 --dumpstats --dumpstats_period 5 --memory_manage
 # tag: nascg.l.nimble
 # running nascg (211009111906: Size l)
== BEFORE =========================
R_DRAM: 638446
W_DRAM: 101449
R_OPT: 2697
W_OPT: 1088


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     9000000
 Iterations:                      5
 Number of available threads:    32

 Initialization time =        3337.064 seconds

   iteration           ||r||                 zeta
        1       0.44593068317733E-11  1499.9998413806252
        2       0.11025896487808E-14    72.8774756683729
        3       0.11379113443538E-14    73.2915890919608
        4       0.11834198499873E-14    73.6707042243697
        5       0.12189506115636E-14    74.0174332195386
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.7401743321954E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  9000000
 Iterations      =                        5
 Time in seconds =                 10718.12
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   154.36
 Mop/s/thread    =                     4.82
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


cycles: 32328090279466
real time(ms): 14055564, user time(ms): 445119433, system time(ms): 3657900, virtual cpu time(ms): 448777333
min_flt: 138120, maj_flt: 1, maxrss: 154377328 KB
child arg: npb.bin/cg.l.x
child pid: 5843
== AFTER ==========================
R_DRAM: 650676
W_DRAM: 107273
R_OPT: 776310
W_OPT: 16298
===================================
# started on Sat Oct  9 11:19:06 2021


 Performance counter stats for 'system wide':

        327,038.72 Joules power/energy-ram/                                           
      1,700,433.29 Joules power/energy-pkg/                                           

   14055.567223885 seconds time elapsed

===================================
WaitBasePageMigration_ms 32
WaitHugePageMigration_ms 0
ExchangePages_nr_exchanges 2807
ExchangePagesBase_nr_base_pages 0
ExchangePagesHuge_nr_base_pages 0
Fast2Slow_nr_migrations 2807
Fast2SlowBasePageMigrations_nr_base_pages 1702
Fast2SlowHugePageMigrations_nr_base_pages 0
Slow2Fast_nr_migrations 2810
Slow2FastBasePageMigrations_nr_base_pages 1547
Slow2FastHugePageMigrations_nr_base_pages 512
== END ============================
