== BEGIN ==========================
Sun 03 Oct 2021 02:07:02 PM WEST
 # setting up vm
vm.migration_batch_size = 8
vm.sysctl_enable_thp_migration = 1
vm.limit_mt_num = 4
always
never
 # setup memory (/sys/fs/cgroup/nimble -> 26Gb)
 # note: 
 # launcher args: --exchange_pages --prefer_memnode --cpunode  0 --fast_mem 0 --slow_mem 2 --dumpstats --dumpstats_period 5 --memory_manage
 # tag: nascg.l.nimble
 # running nascg (211003075412: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     9000000
 Iterations:                      5
 Number of available threads:    32

 Initialization time =        3335.372 seconds

   iteration           ||r||                 zeta
        1       0.44593117698278E-11  1499.9998413806252
        2       0.11031368050744E-14    72.8774756683717
        3       0.11378642024330E-14    73.2915890919601
        4       0.11835054013141E-14    73.6707042243695
        5       0.12188498835127E-14    74.0174332195388
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.7401743321954E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  9000000
 Iterations      =                        5
 Time in seconds =                 10716.07
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   154.39
 Mop/s/thread    =                     4.82
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


cycles: 32319509492770
real time(ms): 14051830, user time(ms): 445050439, system time(ms): 3665258, virtual cpu time(ms): 448715697
min_flt: 122214, maj_flt: 2, maxrss: 154375900 KB
child arg: npb.bin/cg.l.x
child pid: 30922
===================================
# started on Sun Oct  3 14:07:03 2021


 Performance counter stats for 'system wide':

        326,280.01 Joules power/energy-ram/                                           
      1,696,972.38 Joules power/energy-pkg/                                           

   14051.833631788 seconds time elapsed

===================================
WaitBasePageMigration_ms 8
WaitHugePageMigration_ms 0
ExchangePages_nr_exchanges 2807
ExchangePagesBase_nr_base_pages 0
ExchangePagesHuge_nr_base_pages 0
Fast2Slow_nr_migrations 2807
Fast2SlowBasePageMigrations_nr_base_pages 1850
Fast2SlowHugePageMigrations_nr_base_pages 0
Slow2Fast_nr_migrations 2810
Slow2FastBasePageMigrations_nr_base_pages 2474
Slow2FastHugePageMigrations_nr_base_pages 512
== END ============================
