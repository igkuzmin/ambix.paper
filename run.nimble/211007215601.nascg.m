== BEGIN ==========================
Fri 08 Oct 2021 08:17:59 AM WEST
 # setting up vm
vm.migration_batch_size = 8
vm.sysctl_enable_thp_migration = 1
vm.limit_mt_num = 4
always
never
 # setup memory (/sys/fs/cgroup/nimble -> 26Gb)
 # note: 
 # launcher args: --exchange_pages --prefer_memnode --cpunode  0 --fast_mem 0 --slow_mem 2 --dumpstats --dumpstats_period 5 --memory_manage
 # tag: nascg.m.nimble
 # running nascg (211007215601: Size m)
== BEFORE =========================
R_DRAM: 909944
W_DRAM: 156959
R_OPT: 975967
W_OPT: 402569


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         158.227 seconds

   iteration           ||r||                 zeta
        1       0.21274465851313E-11   999.9996857982868
        2       0.13467227429885E-14    57.8675766650496
        3       0.13441464067408E-14    58.2941206350796
        4       0.13463195063009E-14    58.6783531949446
        5       0.13425432335632E-14    59.0239484906588
        6       0.13348029631563E-14    59.3343718346763
        7       0.13233725838987E-14    59.6128667120510
        8       0.13140340683231E-14    59.8624490951615
        9       0.13009029260102E-14    60.0859075092869
       10       0.12902428331459E-14    60.2858074951204
       11       0.12794610004579E-14    60.4644993187783
       12       0.12692056292251E-14    60.6241279736674
       13       0.12626153975455E-14    60.7666446959973
       14       0.12540435535813E-14    60.8938193721783
       15       0.12483546164102E-14    61.0072533517981
       16       0.12426533135365E-14    61.1083922942574
       17       0.12372192714258E-14    61.1985387720226
       18       0.12334630135391E-14    61.2788644311374
       19       0.12276937355404E-14    61.3504215716458
       20       0.12242959196080E-14    61.4141540599923
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                  1216.66
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1428.46
 Mop/s/thread    =                    44.64
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


cycles: 3162550849582
real time(ms): 1375009, user time(ms): 43377759, system time(ms): 421116, virtual cpu time(ms): 43798875
min_flt: 26304, maj_flt: 1, maxrss: 40694200 KB
child arg: npb.bin/cg.m.x
child pid: 53409
== AFTER ==========================
R_DRAM: 972411
W_DRAM: 161831
R_OPT: 1122785
W_OPT: 403044
===================================
# started on Fri Oct  8 08:18:00 2021


 Performance counter stats for 'system wide':

         35,683.81 Joules power/energy-ram/                                           
        177,847.76 Joules power/energy-pkg/                                           

    1375.012523528 seconds time elapsed

===================================
WaitBasePageMigration_ms 92
WaitHugePageMigration_ms 20
ExchangePages_nr_exchanges 261
ExchangePagesBase_nr_base_pages 0
ExchangePagesHuge_nr_base_pages 0
Fast2Slow_nr_migrations 261
Fast2SlowBasePageMigrations_nr_base_pages 1289
Fast2SlowHugePageMigrations_nr_base_pages 0
Slow2Fast_nr_migrations 274
Slow2FastBasePageMigrations_nr_base_pages 2746
Slow2FastHugePageMigrations_nr_base_pages 31232
== END ============================
