== BEGIN ==========================
Fri 08 Oct 2021 06:59:26 AM WEST
 # setting up vm
vm.migration_batch_size = 8
vm.sysctl_enable_thp_migration = 1
vm.limit_mt_num = 4
always
never
 # setup memory (/sys/fs/cgroup/nimble -> 26Gb)
 # note: 
 # launcher args: --exchange_pages --prefer_memnode --cpunode  0 --fast_mem 0 --slow_mem 2 --dumpstats --dumpstats_period 5 --memory_manage
 # tag: nasmg.m.nimble
 # running nasmg (211007215601: Size m)
== BEFORE =========================
R_DRAM: 747683
W_DRAM: 117092
R_OPT: 780634
W_OPT: 349996


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1536x1536x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         289.619 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.1746922003355E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           1536x1536x1280
 Iterations      =                       50
 Time in seconds =                  4422.85
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1980.11
 Mop/s/thread    =                    61.88
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


cycles: 10839161713950
real time(ms): 4712637, user time(ms): 148100252, system time(ms): 1179133, virtual cpu time(ms): 149279385
min_flt: 39962, maj_flt: 0, maxrss: 77890604 KB
child arg: npb.bin/mg.E.x
child pid: 45916
== AFTER ==========================
R_DRAM: 909944
W_DRAM: 156959
R_OPT: 975967
W_OPT: 402569
===================================
# started on Fri Oct  8 06:59:26 2021


 Performance counter stats for 'system wide':

        115,355.67 Joules power/energy-ram/                                           
        593,620.43 Joules power/energy-pkg/                                           

    4712.640709079 seconds time elapsed

===================================
WaitBasePageMigration_ms 0
WaitHugePageMigration_ms 0
ExchangePages_nr_exchanges 942
ExchangePagesBase_nr_base_pages 0
ExchangePagesHuge_nr_base_pages 0
Fast2Slow_nr_migrations 942
Fast2SlowBasePageMigrations_nr_base_pages 1
Fast2SlowHugePageMigrations_nr_base_pages 0
Slow2Fast_nr_migrations 942
Slow2FastBasePageMigrations_nr_base_pages 0
Slow2FastHugePageMigrations_nr_base_pages 0
== END ============================
