== BEGIN ==========================
Thu 07 Oct 2021 10:55:05 PM WEST
 # setting up vm
vm.migration_batch_size = 8
vm.sysctl_enable_thp_migration = 1
vm.limit_mt_num = 4
always
never
 # setup memory (/sys/fs/cgroup/nimble -> 26Gb)
 # note: 
 # launcher args: --exchange_pages --prefer_memnode --cpunode  0 --fast_mem 0 --slow_mem 2 --dumpstats --dumpstats_period 5 --memory_manage
 # tag: nasft.l.nimble
 # running nasft (211007215601: Size l)
== BEFORE =========================
R_DRAM: 615188
W_DRAM: 90511
R_OPT: 45206
W_OPT: 26306


 NAS Parallel Benchmarks (NPB3.4-OMP) - FT Benchmark

 Size                : 2048x1024x1024
 Iterations                  :     25
 Number of available threads :     32

 T =    1     Checksum =    5.122230065252D+02    5.118534037109D+02
 T =    2     Checksum =    5.120463975765D+02    5.117061181082D+02
 T =    3     Checksum =    5.119865766760D+02    5.117096364601D+02
 T =    4     Checksum =    5.119518799488D+02    5.117373863950D+02
 T =    5     Checksum =    5.119269088223D+02    5.117680347632D+02
 T =    6     Checksum =    5.119082416858D+02    5.117967875532D+02
 T =    7     Checksum =    5.118943814638D+02    5.118225281841D+02
 T =    8     Checksum =    5.118842385057D+02    5.118451629348D+02
 T =    9     Checksum =    5.118769435632D+02    5.118649119387D+02
 T =   10     Checksum =    5.118718203448D+02    5.118820803844D+02
 T =   11     Checksum =    5.118683569061D+02    5.118969781011D+02
 T =   12     Checksum =    5.118661708593D+02    5.119098918835D+02
 T =   13     Checksum =    5.118649768950D+02    5.119210777066D+02
 T =   14     Checksum =    5.118645605626D+02    5.119307604484D+02
 T =   15     Checksum =    5.118647586618D+02    5.119391362671D+02
 T =   16     Checksum =    5.118654451572D+02    5.119463757241D+02
 T =   17     Checksum =    5.118665212451D+02    5.119526269238D+02
 T =   18     Checksum =    5.118679083821D+02    5.119580184108D+02
 T =   19     Checksum =    5.118695433664D+02    5.119626617538D+02
 T =   20     Checksum =    5.118713748264D+02    5.119666538138D+02
 T =   21     Checksum =    5.118733606701D+02    5.119700787219D+02
 T =   22     Checksum =    5.118754661974D+02    5.119730095953D+02
 T =   23     Checksum =    5.118776626738D+02    5.119755100241D+02
 T =   24     Checksum =    5.118799262314D+02    5.119776353561D+02
 T =   25     Checksum =    5.118822370068D+02    5.119794338060D+02
 Result verification successful
 class = D


 FT Benchmark Completed.
 Class           =                        D
 Size            =           2048x1024x1024
 Iterations      =                       25
 Time in seconds =                 22427.09
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   399.68
 Mop/s/thread    =                    12.49
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              31 Jan 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


cycles: 53809493327974
real time(ms): 23395195, user time(ms): 734447815, system time(ms): 2820121, virtual cpu time(ms): 737267936
min_flt: 54159, maj_flt: 1, maxrss: 84041340 KB
child arg: npb.bin/ft.l.x
child pid: 8090
== AFTER ==========================
R_DRAM: 659715
W_DRAM: 104529
R_OPT: 673713
W_OPT: 292906
===================================
# started on Thu Oct  7 22:55:05 2021


 Performance counter stats for 'system wide':

        533,295.48 Joules power/energy-ram/                                           
      3,001,609.61 Joules power/energy-pkg/                                           

   23395.199047759 seconds time elapsed

===================================
WaitBasePageMigration_ms 0
WaitHugePageMigration_ms 0
ExchangePages_nr_exchanges 4678
ExchangePagesBase_nr_base_pages 0
ExchangePagesHuge_nr_base_pages 0
Fast2Slow_nr_migrations 4678
Fast2SlowBasePageMigrations_nr_base_pages 2771
Fast2SlowHugePageMigrations_nr_base_pages 0
Slow2Fast_nr_migrations 4678
Slow2FastBasePageMigrations_nr_base_pages 0
Slow2FastHugePageMigrations_nr_base_pages 0
== END ============================
