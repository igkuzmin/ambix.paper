== BEGIN ==========================
Sun 03 Oct 2021 11:09:55 AM WEST
 # setting up vm
vm.migration_batch_size = 8
vm.sysctl_enable_thp_migration = 1
vm.limit_mt_num = 4
always
never
 # setup memory (/sys/fs/cgroup/nimble -> 26Gb)
 # note: 
 # launcher args: --exchange_pages --prefer_memnode --cpunode  0 --fast_mem 0 --slow_mem 2 --dumpstats --dumpstats_period 5 --memory_manage
 # tag: nasmg.l.nimble
 # running nasmg (211003075412: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 2048x2048x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         641.018 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.2004924648216E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           2048x2048x1280
 Iterations      =                       50
 Time in seconds =                  9985.81
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1559.14
 Mop/s/thread    =                    48.72
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              18 Feb 2021

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


cycles: 24443033235658
real time(ms): 10627306, user time(ms): 333000882, system time(ms): 2561573, virtual cpu time(ms): 335562455
min_flt: 101596, maj_flt: 19, maxrss: 138363400 KB
child arg: npb.bin/mg.E.x
child pid: 17998
===================================
# started on Sun Oct  3 11:09:55 2021


 Performance counter stats for 'system wide':

        252,798.05 Joules power/energy-ram/                                           
      1,318,506.74 Joules power/energy-pkg/                                           

   10627.310073814 seconds time elapsed

===================================
WaitBasePageMigration_ms 16
WaitHugePageMigration_ms 0
ExchangePages_nr_exchanges 2125
ExchangePagesBase_nr_base_pages 0
ExchangePagesHuge_nr_base_pages 0
Fast2Slow_nr_migrations 2125
Fast2SlowBasePageMigrations_nr_base_pages 615
Fast2SlowHugePageMigrations_nr_base_pages 0
Slow2Fast_nr_migrations 2125
Slow2FastBasePageMigrations_nr_base_pages 355
Slow2FastHugePageMigrations_nr_base_pages 0
== END ============================
