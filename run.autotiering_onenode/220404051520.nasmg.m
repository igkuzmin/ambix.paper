== BEGIN ==========================
Mon 04 Apr 2022 12:36:14 PM WEST
 # note: 
 # tag: nasmg.m.autotiering_onenode
 # running nasmg (220404051520: Size m)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1536x1536x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         383.031 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.1746922003355E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           1536x1536x1280
 Iterations      =                       50
 Time in seconds =                  7399.82
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1183.50
 Mop/s/thread    =                    36.98
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              09 Feb 2022

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Mon Apr  4 12:36:14 2022


 Performance counter stats for 'system wide':

        183,778.39 Joules power/energy-ram/                                           
      1,023,850.15 Joules power/energy-pkg/                                           

    7782.877938991 seconds time elapsed

== DMESG ==========================
[  681.001923] Policy zone: Normal
== END ============================
