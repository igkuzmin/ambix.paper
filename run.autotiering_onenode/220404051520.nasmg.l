== BEGIN ==========================
Tue 05 Apr 2022 12:49:02 AM WEST
 # note: 
 # tag: nasmg.l.autotiering_onenode
 # running nasmg (220404051520: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 2048x2048x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         700.322 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.2004924648216E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           2048x2048x1280
 Iterations      =                       50
 Time in seconds =                 13723.15
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  1134.52
 Mop/s/thread    =                    35.45
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              09 Feb 2022

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Tue Apr  5 00:49:03 2022


 Performance counter stats for 'system wide':

        341,562.09 Joules power/energy-ram/                                           
      1,923,294.67 Joules power/energy-pkg/                                           

   14423.519184578 seconds time elapsed

== DMESG ==========================
[  681.001923] Policy zone: Normal
== END ============================
