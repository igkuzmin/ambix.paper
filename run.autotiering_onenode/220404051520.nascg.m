== BEGIN ==========================
Mon 04 Apr 2022 09:57:39 AM WEST
 # note: 
 # tag: nascg.m.autotiering_onenode
 # running nascg (220404051520: Size m)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         494.316 seconds

   iteration           ||r||                 zeta
        1       0.21274078794502E-11   999.9996857982868
        2       0.13466689652876E-14    57.8675766650496
        3       0.13441238830703E-14    58.2941206350793
        4       0.13464863573379E-14    58.6783531949444
        5       0.13424122381967E-14    59.0239484906588
        6       0.13347884202417E-14    59.3343718346763
        7       0.13232976151440E-14    59.6128667120514
        8       0.13140702408474E-14    59.8624490951617
        9       0.13007107479719E-14    60.0859075092867
       10       0.12903619333794E-14    60.2858074951201
       11       0.12794559381804E-14    60.4644993187781
       12       0.12692201271041E-14    60.6241279736674
       13       0.12624535270811E-14    60.7666446959979
       14       0.12540148441595E-14    60.8938193721790
       15       0.12484401904395E-14    61.0072533517979
       16       0.12425807235448E-14    61.1083922942576
       17       0.12370673656459E-14    61.1985387720226
       18       0.12335029547508E-14    61.2788644311374
       19       0.12276382978904E-14    61.3504215716461
       20       0.12242308270493E-14    61.4141540599921
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                  2785.81
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   623.86
 Mop/s/thread    =                    19.50
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              09 Feb 2022

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Mon Apr  4 09:57:39 2022


 Performance counter stats for 'system wide':

         79,694.85 Joules power/energy-ram/                                           
        418,318.72 Joules power/energy-pkg/                                           

    3283.894456842 seconds time elapsed

== DMESG ==========================
[  681.001923] Policy zone: Normal
== END ============================
