== BEGIN ==========================
Mon 04 Apr 2022 05:26:45 PM WEST
 # note: 
 # tag: nascg.l.autotiering_onenode
 # running nascg (220404051520: Size l)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     9000000
 Iterations:                      5
 Number of available threads:    32

 Initialization time =        3627.601 seconds

   iteration           ||r||                 zeta
        1       0.39982054586884E-11   999.9998963664279
        2       0.12703457315382E-14    67.8896367477664
        3       0.12695148405047E-14    68.4128917198317
        4       0.12699115842357E-14    68.8745868138875
        5       0.12704277667062E-14    69.2812188534548
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6928121885345E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  9000000
 Iterations      =                        5
 Time in seconds =                 10757.58
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   142.48
 Mop/s/thread    =                     4.45
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              13 Feb 2022

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Mon Apr  4 17:26:45 2022


 Performance counter stats for 'system wide':

        338,264.48 Joules power/energy-ram/                                           
      1,819,516.86 Joules power/energy-pkg/                                           

   14385.188594683 seconds time elapsed

== DMESG ==========================
[  681.001923] Policy zone: Normal
== END ============================
