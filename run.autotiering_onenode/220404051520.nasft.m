== BEGIN ==========================
Mon 04 Apr 2022 10:52:23 AM WEST
 # note: 
 # tag: nasft.m.autotiering_onenode
 # running nasft (220404051520: Size m)


 NAS Parallel Benchmarks (NPB3.4-OMP) - FT Benchmark

 Size                : 1024x1024x1024
 Iterations                  :     25
 Number of available threads :     32

 T =    1     Checksum =    5.116577823033D+02    5.102474711843D+02
 T =    2     Checksum =    5.115693002110D+02    5.108297427588D+02
 T =    3     Checksum =    5.116594781204D+02    5.111677372126D+02
 T =    4     Checksum =    5.117318626123D+02    5.113819725173D+02
 T =    5     Checksum =    5.117854293358D+02    5.115331411069D+02
 T =    6     Checksum =    5.118280856712D+02    5.116469520295D+02
 T =    7     Checksum =    5.118643536980D+02    5.117359230707D+02
 T =    8     Checksum =    5.118962169291D+02    5.118071547100D+02
 T =    9     Checksum =    5.119245069338D+02    5.118651589280D+02
 T =   10     Checksum =    5.119496314520D+02    5.119130181970D+02
 T =   11     Checksum =    5.119718715589D+02    5.119529356180D+02
 T =   12     Checksum =    5.119914819920D+02    5.119865337476D+02
 T =   13     Checksum =    5.120087150402D+02    5.120150341749D+02
 T =   14     Checksum =    5.120238193816D+02    5.120393729642D+02
 T =   15     Checksum =    5.120370334009D+02    5.120602782105D+02
 T =   16     Checksum =    5.120485793930D+02    5.120783237496D+02
 T =   17     Checksum =    5.120586600978D+02    5.120939672150D+02
 T =   18     Checksum =    5.120674573643D+02    5.121075775100D+02
 T =   19     Checksum =    5.120751323386D+02    5.121194549542D+02
 T =   20     Checksum =    5.120818266012D+02    5.121298462676D+02
 T =   21     Checksum =    5.120876638105D+02    5.121389558515D+02
 T =   22     Checksum =    5.120927515566D+02    5.121469543779D+02
 T =   23     Checksum =    5.120971832335D+02    5.121539853959D+02
 T =   24     Checksum =    5.121010398212D+02    5.121601704602D+02
 T =   25     Checksum =    5.121043915204D+02    5.121656131470D+02
 class = U


 FT Benchmark Completed.
 Class           =                        U
 Size            =           1024x1024x1024
 Iterations      =                       25
 Time in seconds =                  5955.74
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   729.10
 Mop/s/thread    =                    22.78
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              10 Feb 2022

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Mon Apr  4 10:52:23 2022


 Performance counter stats for 'system wide':

        145,223.96 Joules power/energy-ram/                                           
        825,462.39 Joules power/energy-pkg/                                           

    6229.965984957 seconds time elapsed

== DMESG ==========================
[  681.001923] Policy zone: Normal
== END ============================
