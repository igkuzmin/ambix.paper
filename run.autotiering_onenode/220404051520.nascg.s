== BEGIN ==========================
Mon 04 Apr 2022 06:02:11 AM WEST
 # note: 
 # tag: nascg.s.autotiering_onenode
 # running nascg (220404051520: Size s)


 NAS Parallel Benchmarks (NPB3.4-OMP) - CG Benchmark

 Size:     3000000
 Iterations:                     20
 Number of available threads:    32

 Initialization time =         489.827 seconds

   iteration           ||r||                 zeta
        1       0.21274337258038E-11   999.9996857982868
        2       0.13450109717976E-14    57.8675766650498
        3       0.13441841246261E-14    58.2941206350796
        4       0.13464154540667E-14    58.6783531949446
        5       0.13425429829304E-14    59.0239484906585
        6       0.13347493844303E-14    59.3343718346763
        7       0.13232976040402E-14    59.6128667120508
        8       0.13140577462372E-14    59.8624490951617
        9       0.13007359098959E-14    60.0859075092869
       10       0.12902668907167E-14    60.2858074951207
       11       0.12794877390113E-14    60.4644993187786
       12       0.12692313265819E-14    60.6241279736679
       13       0.12624716069530E-14    60.7666446959980
       14       0.12540612332994E-14    60.8938193721790
       15       0.12484301403118E-14    61.0072533517978
       16       0.12425013460026E-14    61.1083922942578
       17       0.12371973308170E-14    61.1985387720225
       18       0.12334354654792E-14    61.2788644311375
       19       0.12276355061035E-14    61.3504215716456
       20       0.12243490528514E-14    61.4141540599923
 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 Zeta is     0.6141415405999E+02


 CG Benchmark Completed.
 Class           =                        U
 Size            =                  3000000
 Iterations      =                       20
 Time in seconds =                  2697.38
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                   644.32
 Mop/s/thread    =                    20.13
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              09 Feb 2022

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


===================================
# started on Mon Apr  4 06:02:11 2022


 Performance counter stats for 'system wide':

         77,512.37 Joules power/energy-ram/                                           
        407,768.03 Joules power/energy-pkg/                                           

    3190.945749587 seconds time elapsed

== DMESG ==========================
[  681.001923] Policy zone: Normal
== END ============================
