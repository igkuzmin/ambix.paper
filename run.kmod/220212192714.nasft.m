== BEGIN ==========================
Sat 12 Feb 2022 08:23:34 PM WET
 # note: 
 # tag: nasft.m.kmod
 # running nasft (220212192714: Size m)
 Binding KMOD...


 NAS Parallel Benchmarks (NPB3.4-OMP) - FT Benchmark

 Size                : 1024x1024x1024
 Iterations                  :     25
 Number of available threads :     32

 T =    1     Checksum =    5.116577823033D+02    5.102474711843D+02
 T =    2     Checksum =    5.115693002110D+02    5.108297427588D+02
 T =    3     Checksum =    5.116594781204D+02    5.111677372126D+02
 T =    4     Checksum =    5.117318626123D+02    5.113819725173D+02
 T =    5     Checksum =    5.117854293358D+02    5.115331411069D+02
 T =    6     Checksum =    5.118280856712D+02    5.116469520295D+02
 T =    7     Checksum =    5.118643536980D+02    5.117359230707D+02
 T =    8     Checksum =    5.118962169291D+02    5.118071547100D+02
 T =    9     Checksum =    5.119245069338D+02    5.118651589280D+02
 T =   10     Checksum =    5.119496314520D+02    5.119130181970D+02
 T =   11     Checksum =    5.119718715589D+02    5.119529356180D+02
 T =   12     Checksum =    5.119914819920D+02    5.119865337476D+02
 T =   13     Checksum =    5.120087150402D+02    5.120150341749D+02
 T =   14     Checksum =    5.120238193816D+02    5.120393729642D+02
 T =   15     Checksum =    5.120370334009D+02    5.120602782105D+02
 T =   16     Checksum =    5.120485793930D+02    5.120783237496D+02
 T =   17     Checksum =    5.120586600978D+02    5.120939672150D+02
 T =   18     Checksum =    5.120674573643D+02    5.121075775100D+02
 T =   19     Checksum =    5.120751323386D+02    5.121194549542D+02
 T =   20     Checksum =    5.120818266012D+02    5.121298462676D+02
 T =   21     Checksum =    5.120876638105D+02    5.121389558515D+02
 T =   22     Checksum =    5.120927515566D+02    5.121469543779D+02
 T =   23     Checksum =    5.120971832335D+02    5.121539853959D+02
 T =   24     Checksum =    5.121010398212D+02    5.121601704602D+02
 T =   25     Checksum =    5.121043915204D+02    5.121656131470D+02
 class = U


 FT Benchmark Completed.
 Class           =                        U
 Size            =           1024x1024x1024
 Iterations      =                       25
 Time in seconds =                   761.16
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  5704.91
 Mop/s/thread    =                   178.28
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              10 Feb 2022

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


 Unbinding KMOD...
===================================
# started on Sat Feb 12 20:23:35 2022


 Performance counter stats for 'system wide':

         23,138.03 Joules power/energy-ram/                                           
        114,063.70 Joules power/energy-pkg/                                           

     821.008656002 seconds time elapsed

== DMESG ==========================
[38458.116959] kmod.PLACEMENT: Unbound pid=10973.
[38460.759436] kmod.PLACEMENT: Bound pid=11327.
[38461.380765] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 0 intensive pages out of 131071. (99026us cleanup; 103761us migration) 
[38478.046134] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (10288039us cleanup; 4180242us migration)
[38489.962157] kmod.PLACEMENT: DRAM->NVRAM [U]: Migrated 131071 out of 131071 pages. (11784799us)
[38498.531893] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2840806us cleanup; 4643725us migration)
[38509.774241] kmod.PLACEMENT: DRAM->NVRAM [U]: Migrated 131071 out of 131071 pages. (11118539us)
[38516.790054] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (1654746us cleanup; 4265119us migration)
[38532.549968] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (11658093us cleanup; 2916806us migration) 
[38539.238112] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (3252681us cleanup; 2366322us migration)
[38550.761159] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (3099425us cleanup; 7301395us migration)
[38557.989197] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (1626208us cleanup; 4529833us migration) 
[38578.337770] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (14153527us cleanup; 4974468us migration)
[38585.404238] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (723315us cleanup; 5265350us migration)
[38592.210032] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (3429936us cleanup; 2294751us migration) 
[38600.470867] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2696247us cleanup; 4454006us migration)
[38619.763232] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13687786us cleanup; 4381096us migration)
[38625.784656] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (958469us cleanup; 3977944us migration) 
[38632.414655] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2788485us cleanup; 2758061us migration)
[38640.627435] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2393265us cleanup; 4725532us migration)
[38661.028038] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (13499585us cleanup; 5661569us migration) 
[38671.082246] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2727792us cleanup; 6217192us migration)
[38679.616406] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2457265us cleanup; 4990456us migration)
[38699.860559] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (13123734us cleanup; 5895158us migration) 
[38709.691967] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2709034us cleanup; 5999928us migration)
[38718.088921] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2266295us cleanup; 5031244us migration)
[38737.295331] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (12325508us cleanup; 5671192us migration) 
[38747.161881] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2748496us cleanup; 5989702us migration)
[38754.977148] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2293220us cleanup; 4422677us migration)
[38773.019734] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (12396296us cleanup; 4445287us migration) 
[38780.510907] kmod.PLACEMENT: Can't find separator
[38780.510911] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (664600us cleanup; 5736331us migration)
[38788.649062] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (1335289us cleanup; 5708556us migration)
[38805.972353] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (556000us cleanup; 15581773us migration)
[38810.688711] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (1352542us cleanup; 2296495us migration) 
[38817.022879] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (3069890us cleanup; 2191354us migration)
[38825.036731] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (1988867us cleanup; 4931659us migration)
[38842.054514] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (11092355us cleanup; 4741042us migration) 
[38850.042039] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2784169us cleanup; 4117643us migration)
[38857.062650] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2328415us cleanup; 3604716us migration)
[38875.418125] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (12300451us cleanup; 4855096us migration) 
[38883.511385] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2637053us cleanup; 4356759us migration)
[38889.776186] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2251400us cleanup; 2931305us migration)
[38906.848992] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (10864559us cleanup; 4999865us migration) 
[38914.514798] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2627001us cleanup; 3950569us migration)
[38920.570418] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2181626us cleanup; 2785441us migration)
[38935.789073] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (10659213us cleanup; 3381246us migration) 
[38942.299427] kmod.PLACEMENT: Can't find separator
[38942.299433] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (585308us cleanup; 4861217us migration)
[38948.626676] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 44697 out of 131070 pages. (690063us cleanup; 4557867us migration)
[38964.398440] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (10962911us cleanup; 3616715us migration)
[38971.750754] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (2706663us cleanup; 3573629us migration) 
[38976.707020] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2239840us cleanup; 1663132us migration)
[38991.269498] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (9822705us cleanup; 3572807us migration)
[38998.432865] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (2786597us cleanup; 3297719us migration) 
[39007.742596] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2237915us cleanup; 5964291us migration)
[39018.834906] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (5772976us cleanup; 4187743us migration)
[39024.510964] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (2211457us cleanup; 2383265us migration) 
[39039.080726] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (1534291us cleanup; 11868022us migration)
[39045.656723] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 8120 out of 131070 pages. (1268693us cleanup; 4237472us migration)
[39061.661309] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2567187us cleanup; 12247833us migration)
[39068.034486] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (2226457us cleanup; 3067580us migration) 
[39074.406687] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 43385 out of 131070 pages. (1106640us cleanup; 4191525us migration)
[39089.172184] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (9686943us cleanup; 3911910us migration)
[39094.933773] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2689245us cleanup; 1990622us migration)
[39109.393344] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (1452697us cleanup; 11830881us migration) 
[39116.783524] kmod.PLACEMENT: Can't find separator
[39116.783528] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (1664311us cleanup; 4654915us migration)
[39132.119764] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2488616us cleanup; 11683311us migration)
[39139.454654] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 1030 out of 131070 pages. (1604170us cleanup; 4634833us migration)
[39155.050246] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2480571us cleanup; 11934905us migration)
[39161.678177] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (1789816us cleanup; 3768093us migration) 
[39166.567203] kmod.PLACEMENT: Can't find separator
[39166.567206] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (2486725us cleanup; 1355269us migration)
[39182.154308] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 57829 out of 131070 pages. (9792048us cleanup; 4622881us migration)
[39189.223431] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 11211 out of 131070 pages. (2209573us cleanup; 3784445us migration)
[39204.678003] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 57365 out of 131070 pages. (9561672us cleanup; 4718338us migration)
[39211.839200] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (2216022us cleanup; 3864584us migration) 
[39225.591096] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (8934605us cleanup; 3652971us migration)
[39233.638830] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 41759 out of 131070 pages. (2482349us cleanup; 4460070us migration)
[39247.542359] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (9243300us cleanup; 3505706us migration) 
[39252.985666] kmod.PLACEMENT: Can't find separator
[39252.985668] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (2790726us cleanup; 1575034us migration)
[39266.794820] kmod.PLACEMENT: Can't find separator
[39266.794827] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (2568972us cleanup; 10073727us migration)
[39274.493296] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 128 out of 131070 pages. (2151815us cleanup; 4464329us migration)
[39279.347545] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 26502 out of 131070 pages. (2493694us cleanup; 1296094us migration)
[39279.347592] kmod.PLACEMENT: Unbound pid=11327.
== END ============================
