== BEGIN ==========================
Sat 12 Feb 2022 11:00:49 PM WET
 # note: 
 # tag: nasmg.s.kmod
 # running nasmg (220212192714: Size s)
 Binding KMOD...


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1024x1024x1024  (class D)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:          12.694 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 VERIFICATION FAILED
 L2 Norm is              0.1590005169528E-09
 The correct L2 Norm is  0.1583275060440E-09


 MG Benchmark Completed.
 Class           =                        D
 Size            =           1024x1024x1024
 Iterations      =                       50
 Time in seconds =                   324.36
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  9599.94
 Mop/s/thread    =                   300.00
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              09 Feb 2022

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


 Unbinding KMOD...
===================================
# started on Sat Feb 12 23:00:49 2022


 Performance counter stats for 'system wide':

         11,150.76 Joules power/energy-ram/                                           
         45,881.64 Joules power/energy-pkg/                                           

     338.330312371 seconds time elapsed

== DMESG ==========================
[47893.425146] kmod.PLACEMENT: Unbound pid=12676.
[47894.895378] kmod.PLACEMENT: Bound pid=12798.
[47901.974406] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (456355us cleanup; 1542544us migration)
[47905.637109] kmod.PLACEMENT: DRAM->NVRAM [U]: Migrated 131071 out of 131071 pages. (3622345us)
[47907.058666] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 62 out of 131070 pages. (208828us cleanup; 207931us migration)
[47909.845596] kmod.PLACEMENT: DRAM->NVRAM [U]: Migrated 131071 out of 131071 pages. (2756235us)
[47912.797809] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 72 out of 131070 pages. (832173us cleanup; 1078971us migration)
[47915.637262] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 56 out of 131070 pages. (580601us cleanup; 1231114us migration)
[47919.848578] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (584233us cleanup; 2570321us migration)
[47923.266349] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (578809us cleanup; 1783812us migration)
[47928.268601] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (454837us cleanup; 3500111us migration) 
[47931.926024] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (400657us cleanup; 2198900us migration)
[47936.191822] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (442690us cleanup; 2771766us migration)
[47941.600740] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (596709us cleanup; 3752162us migration) 
[47943.877297] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (171820us cleanup; 1085839us migration)
[47947.974682] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (703664us cleanup; 2359173us migration)
[47951.151311] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (516075us cleanup; 1605891us migration) 
[47955.931701] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (868977us cleanup; 2847593us migration)
[47960.200322] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (434610us cleanup; 2784006us migration)
[47963.777774] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (871373us cleanup; 1648515us migration) 
[47967.439909] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 31531 out of 131070 pages. (791115us cleanup; 1837583us migration)
[47970.508916] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 29676 out of 131070 pages. (707703us cleanup; 1316761us migration)
[47973.755224] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 22332 out of 131070 pages. (287357us cleanup; 1909427us migration)
[47976.622678] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 21876 out of 131070 pages. (890976us cleanup; 945284us migration)
[47981.721700] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (810352us cleanup; 3220390us migration) 
[47983.613456] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 11175 out of 131070 pages. (436265us cleanup; 433529us migration)
[47986.646845] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 1 out of 131070 pages. (659039us cleanup; 1343053us migration)
[47989.499205] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (390666us cleanup; 1422285us migration)
[47994.002525] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (498836us cleanup; 2954385us migration)
[47996.679601] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 11145 out of 131070 pages. (906027us cleanup; 733160us migration)
[47999.656369] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 1 out of 131070 pages. (773709us cleanup; 1150887us migration)
[48002.668715] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (548361us cleanup; 1412317us migration)
[48007.388325] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (496408us cleanup; 3152093us migration)
[48011.472405] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (750442us cleanup; 2285866us migration) 
[48014.656119] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 6418 out of 131070 pages. (915815us cleanup; 1222174us migration)
[48016.891064] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (584701us cleanup; 630491us migration)
[48020.253445] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 14554 out of 131070 pages. (628288us cleanup; 1696854us migration)
[48023.242691] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 16250 out of 131070 pages. (901297us cleanup; 1057177us migration)
[48026.295562] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (636442us cleanup; 1362070us migration)
[48029.120905] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (361701us cleanup; 1428809us migration)
[48033.413949] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 64857 out of 131070 pages. (509196us cleanup; 2741537us migration)
[48037.582813] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (893077us cleanup; 2240313us migration) 
[48040.846296] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (919133us cleanup; 1295918us migration)
[48043.270523] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (738882us cleanup; 645627us migration)
[48046.254525] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (666064us cleanup; 1264306us migration)
[48049.073997] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (463195us cleanup; 1312400us migration)
[48052.234939] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 12373 out of 131070 pages. (391046us cleanup; 1725664us migration)
[48055.242486] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (532704us cleanup; 1425288us migration)
[48059.583097] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 55309 out of 131070 pages. (461927us cleanup; 2812828us migration)
[48062.056553] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 1823 out of 131070 pages. (786698us cleanup; 662932us migration)
[48066.007207] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 32572 out of 131070 pages. (689579us cleanup; 2198627us migration)
[48068.621427] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 30235 out of 131070 pages. (679031us cleanup; 898005us migration)
[48074.284464] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (747636us cleanup; 3834811us migration) 
[48078.367378] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 55309 out of 131070 pages. (458513us cleanup; 2563819us migration)
[48081.421753] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 25120 out of 131070 pages. (575324us cleanup; 1448988us migration)
[48084.533513] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (797622us cleanup; 1265842us migration)
[48087.549509] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 4528 out of 131070 pages. (477712us cleanup; 1498788us migration)
[48092.018723] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 59062 out of 131070 pages. (699492us cleanup; 2721394us migration)
[48096.051846] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (825570us cleanup; 2154003us migration) 
[48099.246378] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (919344us cleanup; 1231959us migration)
[48101.671827] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (786148us cleanup; 595145us migration)
[48104.693826] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (574765us cleanup; 1393907us migration)
[48107.531338] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (493582us cleanup; 1306517us migration)
[48110.508514] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (386534us cleanup; 1541294us migration)
[48113.503921] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (656975us cleanup; 1289989us migration)
[48117.501229] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 51193 out of 131070 pages. (487800us cleanup; 2464902us migration)
[48120.842398] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 43579 out of 131070 pages. (905426us cleanup; 1399992us migration)
[48124.200627] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (929706us cleanup; 1373596us migration)
[48127.092375] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (399454us cleanup; 1441050us migration)
[48131.624066] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 56449 out of 131070 pages. (855497us cleanup; 2613712us migration)
[48135.783659] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (889960us cleanup; 2203796us migration) 
[48139.157899] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 5270 out of 131070 pages. (929178us cleanup; 1387433us migration)
[48141.435988] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (607828us cleanup; 638744us migration)
[48144.538397] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 6417 out of 131070 pages. (503750us cleanup; 1560071us migration)
[48147.186651] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (171747us cleanup; 1445352us migration)
[48151.073767] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 51193 out of 131070 pages. (372965us cleanup; 2460664us migration)
[48154.486615] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 56778 out of 131070 pages. (907952us cleanup; 1468557us migration)
[48159.466089] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (605303us cleanup; 3313502us migration) 
[48161.425126] kmod.PLACEMENT: Can't find separator
[48161.425128] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (499114us cleanup; 420066us migration)
[48165.245082] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 45967 out of 131070 pages. (718337us cleanup; 2048145us migration)
[48167.417031] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 2075 out of 131070 pages. (501735us cleanup; 646737us migration)
[48171.269271] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 48406 out of 131070 pages. (814078us cleanup; 1992134us migration)
[48173.533235] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 14259 out of 131070 pages. (517605us cleanup; 729902us migration)
[48179.143512] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (769697us cleanup; 3775324us migration) 
[48182.378057] kmod.PLACEMENT: Can't find separator
[48182.378064] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (822157us cleanup; 1387360us migration)
[48185.140035] kmod.PLACEMENT: Can't find separator
[48185.140038] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (904593us cleanup; 804480us migration)
[48188.084422] kmod.PLACEMENT: Can't find separator
[48188.084432] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (826867us cleanup; 1076369us migration)
[48190.914809] kmod.PLACEMENT: Can't find separator
[48190.914812] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (908790us cleanup; 882088us migration)
[48193.600685] kmod.PLACEMENT: Can't find separator
[48193.600688] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (771108us cleanup; 891057us migration)
[48196.345102] kmod.PLACEMENT: Can't find separator
[48196.345104] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (372335us cleanup; 1345598us migration)
[48199.438718] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (599940us cleanup; 1455793us migration)
[48202.288002] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (426125us cleanup; 1373753us migration)
[48205.346250] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (566608us cleanup; 1445108us migration)
[48208.188316] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (423894us cleanup; 1392058us migration)
[48211.273441] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (736356us cleanup; 1310090us migration)
[48214.025696] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (240203us cleanup; 1458393us migration)
[48217.056869] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (551953us cleanup; 1426640us migration)
[48219.905130] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (419992us cleanup; 1396651us migration)
[48222.941277] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (547181us cleanup; 1459441us migration)
[48225.744236] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (316410us cleanup; 1455795us migration)
[48228.801483] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (575636us cleanup; 1435083us migration)
[48231.681239] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (552459us cleanup; 1299776us migration)
[48231.954747] kmod.PLACEMENT: Unbound pid=12798.
== END ============================
