== BEGIN ==========================
Sat 12 Feb 2022 08:37:16 PM WET
 # note: 
 # tag: nasmg.m.kmod
 # running nasmg (220212192714: Size m)
 Binding KMOD...


 NAS Parallel Benchmarks (NPB3.4-OMP) - MG Benchmark

 Reading from input file mg.input
 Size: 1536x1536x1280  (class U)
 Iterations:                     50
 Number of available threads:    32

 Initialization time:         246.345 seconds

  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
  iter  25
  iter  30
  iter  35
  iter  40
  iter  45
  iter  50

 Benchmark completed 
 Problem size unknown
 NO VERIFICATION PERFORMED
 L2 Norm is  0.1746922003355E-09


 MG Benchmark Completed.
 Class           =                        U
 Size            =           1536x1536x1280
 Iterations      =                       50
 Time in seconds =                  4107.78
 Total threads   =                       32
 Avail threads   =                       32
 Mop/s total     =                  2131.98
 Mop/s/thread    =                    66.62
 Operation type  =           floating point
 Verification    =             UNSUCCESSFUL
 Version         =                    3.4.1
 Compile date    =              09 Feb 2022

 Compile options:
    FC           = mpif77
    FLINK        = $(FC)
    F_LIB        = (none)
    F_INC        = (none)
    FFLAGS       = -O3 -fopenmp
    FLINKFLAGS   = $(FFLAGS)
    RAND         = randi8


 Please send all errors/feedbacks to:

 NPB Development Team
 npb@nas.nasa.gov


 Unbinding KMOD...
===================================
# started on Sat Feb 12 20:37:17 2022


 Performance counter stats for 'system wide':

        107,526.03 Joules power/energy-ram/                                           
        546,383.05 Joules power/energy-pkg/                                           

    4357.905594121 seconds time elapsed

== DMESG ==========================
[39279.347592] kmod.PLACEMENT: Unbound pid=11327.
[39282.632702] kmod.PLACEMENT: Bound pid=11493.
[39283.583686] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 0 intensive pages out of 131071. (62885us cleanup; 67746us migration) 
[39296.871376] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (4921882us cleanup; 7209706us migration)
[39329.032533] kmod.PLACEMENT: DRAM->NVRAM [U]: Migrated 131071 out of 131071 pages. (31806879us)
[39352.015049] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (17931890us cleanup; 3794990us migration)
[39377.764084] kmod.PLACEMENT: DRAM->NVRAM [U]: Migrated 131071 out of 131071 pages. (25465430us)
[39388.013181] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (5745148us cleanup; 3385389us migration)
[39418.186278] kmod.PLACEMENT: DRAM->NVRAM [U]: Migrated 131071 out of 131071 pages. (29840766us)
[39436.840634] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (10080628us cleanup; 7368209us migration)
[39464.202729] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13331566us cleanup; 12727140us migration)
[39486.229772] rcu: INFO: rcu_sched self-detected stall on CPU
[39486.232710] rcu: 	33-....: (5247 ticks this GP) idle=832/1/0x4000000000000000 softirq=57957/57957 fqs=2624 
[39486.234191] 	(t=5251 jiffies g=532881 q=598)
[39486.234315] NMI backtrace for cpu 33
[39486.234435] CPU: 33 PID: 11179 Comm: kworker/33:1 Tainted: G           OE     5.8.5-patchedRDT #4
[39486.234475] Hardware name: Supermicro SYS-6019P-WT/X11DDW-L, BIOS 3.2 10/16/2019
[39486.235135] Workqueue: events work_queue_routine [kmod]
[39486.235172] Call Trace:
[39486.235354]  <IRQ>
[39486.235926]  dump_stack+0x6d/0x90
[39486.236045]  nmi_cpu_backtrace.cold.8+0x13/0x50
[39486.236406]  ? lapic_can_unplug_cpu.cold.35+0x37/0x37
[39486.236481]  nmi_trigger_cpumask_backtrace+0xf9/0xfb
[39486.237037]  rcu_dump_cpu_stacks+0xab/0xd9
[39486.237088]  rcu_sched_clock_irq.cold.93+0x109/0x36c
[39486.237236]  ? timekeeping_advance+0x372/0x5a0
[39486.237440]  ? tick_sched_do_timer+0x60/0x60
[39486.237466]  update_process_times+0x24/0x60
[39486.237490]  tick_sched_handle.isra.20+0x1f/0x60
[39486.237531]  tick_sched_timer+0x3b/0x80
[39486.237540]  __hrtimer_run_queues+0x100/0x280
[39486.237553]  hrtimer_interrupt+0x100/0x220
[39486.237579]  __sysvec_apic_timer_interrupt+0x5d/0xf0
[39486.237752]  asm_call_on_stack+0xf/0x20
[39486.237760]  </IRQ>
[39486.237957]  sysvec_apic_timer_interrupt+0x75/0x80
[39486.238053]  asm_sysvec_apic_timer_interrupt+0x12/0x20
[39486.238216] RIP: 0010:pte_callback_nvram_clear+0x75/0xc0 [kmod]
[39486.238352] Code: 48 85 c0 74 0f 48 89 c2 48 f7 d2 83 e2 01 48 f7 da 48 31 d0 48 c1 e0 0c 48 c1 e8 18 48 c1 e0 06 48 03 05 86 dc 42 ec 48 8b 00 <48> c1 e8 3a 48 83 f8 02 75 b6 49 8b 7c 24 18 4c 89 ee 48 89 da e8
[39486.238372] RSP: 0000:ffffadac8a1afcb0 EFLAGS: 00000286
[39486.238437] RAX: 017fffc000080034 RBX: ffff9d1236e745f8 RCX: ffffadac8a1afd98
[39486.238491] RDX: 0000000000000000 RSI: 00007fe0b28bf000 RDI: 80000006eaf68867
[39486.238522] RBP: ffffadac8a1afcc8 R08: ffffadac8a1afe00 R09: 00007fe080200000
[39486.238523] R10: 000000000000006a R11: 0000000000000039 R12: ffffadac8a1afd98
[39486.238524] R13: 00007fe0b28bf000 R14: 00007fe0b29ff000 R15: ffffadac8a1afd98
[39486.238866]  __walk_page_range+0x6f9/0x9e0
[39486.238948]  walk_page_range+0x9b/0x120
[39486.238954]  ambix_check_memory+0x2ba/0x580 [kmod]
[39486.238998]  ? do_page_walk+0x3a0/0x3a0 [kmod]
[39486.239044]  work_queue_routine+0xe/0x20 [kmod]
[39486.239386]  process_one_work+0x1ad/0x370
[39486.239396]  ? create_worker+0x1a0/0x1a0
[39486.239401]  worker_thread+0x30/0x390
[39486.239427]  ? create_worker+0x1a0/0x1a0
[39486.239523]  kthread+0x116/0x130
[39486.239554]  ? kthread_park+0x80/0x80
[39486.239750]  ret_from_fork+0x1f/0x30
[39531.977704] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (31149093us cleanup; 34878750us migration)
[39558.789324] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13463898us cleanup; 12050679us migration)
[39582.089753] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (4568737us cleanup; 17468953us migration) 
[39613.533359] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (7863300us cleanup; 22232355us migration)
[39643.328425] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2599757us cleanup; 25853000us migration)
[39669.527148] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (5784690us cleanup; 19109646us migration) 
[39699.133969] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13639027us cleanup; 14647898us migration)
[39729.703229] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (3050903us cleanup; 26167211us migration)
[39754.781270] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (5332029us cleanup; 18459844us migration) 
[39787.079914] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (12358138us cleanup; 18564624us migration)
[39817.445874] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (6857055us cleanup; 22168949us migration)
[39838.492068] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (3620168us cleanup; 16186211us migration) 
[39869.706793] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (10465390us cleanup; 19387946us migration)
[39900.071568] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2853831us cleanup; 26173150us migration)
[39923.176301] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (5487348us cleanup; 16351968us migration) 
[39954.741416] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (10455984us cleanup; 19755189us migration)
[39985.103747] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2549252us cleanup; 26485054us migration)
[40007.430323] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (5116018us cleanup; 15964912us migration) 
[40039.216972] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (9710412us cleanup; 20716793us migration)
[40071.265872] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2505637us cleanup; 28191391us migration)
[40092.617752] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (3369908us cleanup; 16732406us migration) 
[40125.400549] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13479013us cleanup; 17931728us migration)
[40153.698292] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (7782410us cleanup; 19211446us migration)
[40177.807868] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (4800777us cleanup; 18028434us migration) 
[40209.138932] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13544132us cleanup; 16440130us migration)
[40237.270835] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (5064287us cleanup; 21759171us migration)
[40259.287251] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (5041651us cleanup; 15737168us migration) 
[40288.427588] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (9769296us cleanup; 18055353us migration)
[40320.783876] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (4253162us cleanup; 26740276us migration)
[40343.954171] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (4827544us cleanup; 17084757us migration) 
[40373.900745] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13880868us cleanup; 14730747us migration)
[40404.560003] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (3044727us cleanup; 26270680us migration)
[40427.497227] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (5019788us cleanup; 16661446us migration) 
[40459.570007] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13856222us cleanup; 16852447us migration)
[40488.334580] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (4872996us cleanup; 22569034us migration)
[40510.917161] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (4890422us cleanup; 16437820us migration) 
[40543.205169] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13888008us cleanup; 17024812us migration)
[40573.563601] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (3775010us cleanup; 25233651us migration)
[40592.724990] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (3232207us cleanup; 14724433us migration) 
[40623.256913] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (6488708us cleanup; 22702914us migration)
[40654.979063] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (4199936us cleanup; 26176347us migration)
[40677.072521] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (5039357us cleanup; 15792032us migration) 
[40707.976484] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13968140us cleanup; 15586037us migration)
[40738.900878] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2907516us cleanup; 26662730us migration)
[40760.734000] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (4423300us cleanup; 16163730us migration) 
[40790.866099] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (14031697us cleanup; 14771212us migration)
[40823.276487] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2965913us cleanup; 28082708us migration)
[40843.911766] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (3134240us cleanup; 16262976us migration) 
[40875.123416] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13753061us cleanup; 16094718us migration)
[40906.851296] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (1901477us cleanup; 28472636us migration)
[40926.120077] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (2674098us cleanup; 15361812us migration) 
[40957.778030] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13640946us cleanup; 16648377us migration)
[40987.712853] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2361459us cleanup; 26237221us migration)
[41009.662021] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (4541133us cleanup; 16170101us migration) 
[41040.444550] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13848357us cleanup; 15599395us migration)
[41070.848340] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2394765us cleanup; 26677327us migration)
[41092.470869] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (4278057us cleanup; 16112879us migration) 
[41124.496194] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13889371us cleanup; 16775673us migration)
[41155.778098] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (7048211us cleanup; 22879310us migration)
[41175.524589] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (2214448us cleanup; 16322057us migration) 
[41201.599457] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (14147023us cleanup; 10645884us migration)
[41227.147955] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (4608314us cleanup; 19663195us migration)
[41242.584877] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (7797008us cleanup; 6454196us migration) 
[41271.563385] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (9131860us cleanup; 18520034us migration)
[41292.299169] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (11576329us cleanup; 7910540us migration)
[41323.063622] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (14176199us cleanup; 15232252us migration) 
[41358.502752] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (10077302us cleanup; 23961729us migration)
[41373.136372] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (5854956us cleanup; 7623172us migration)
[41404.059005] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (14099624us cleanup; 15465862us migration) 
[41423.423905] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (10271100us cleanup; 7877433us migration)
[41453.123292] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13577480us cleanup; 14797113us migration)
[41482.463734] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (8333219us cleanup; 19689961us migration) 
[41499.512475] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2164632us cleanup; 13694234us migration)
[41529.427028] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2530434us cleanup; 26048982us migration)
[41559.952205] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (4355400us cleanup; 24822678us migration) 
[41581.081752] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (5325167us cleanup; 14557486us migration)
[41609.801074] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (1964635us cleanup; 25433208us migration)
[41642.080888] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (4335371us cleanup; 26599099us migration) 
[41659.766080] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (4739491us cleanup; 11752553us migration)
[41691.822399] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (4432223us cleanup; 26261275us migration)
[41723.823298] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (4165781us cleanup; 26465623us migration) 
[41740.663786] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (4061781us cleanup; 11576592us migration)
[41772.045784] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (4422290us cleanup; 25604986us migration)
[41804.862231] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (4206918us cleanup; 27229698us migration) 
[41823.903502] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (3865734us cleanup; 13963042us migration)
[41853.304441] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2370241us cleanup; 25704243us migration)
[41886.483981] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (4179795us cleanup; 27621757us migration) 
[41905.742357] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2832163us cleanup; 15200541us migration)
[41935.521803] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (7756010us cleanup; 20676355us migration)
[41969.703099] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (2781090us cleanup; 30023585us migration) 
[42007.612339] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (10589254us cleanup; 25906226us migration)
[42018.743817] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (3829386us cleanup; 6172897us migration)
[42050.003211] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (13737901us cleanup; 16165898us migration) 
[42071.240695] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (11004292us cleanup; 8979326us migration)
[42095.753011] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 21235 out of 131070 pages. (13746456us cleanup; 9496944us migration)
[42123.552909] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (3695075us cleanup; 22804110us migration)
[42146.269687] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (4868791us cleanup; 16590869us migration) 
[42177.115012] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13390624us cleanup; 16108171us migration)
[42208.592621] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13480666us cleanup; 16641142us migration)
[42242.276272] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (10829084us cleanup; 21458528us migration) 
[42257.649701] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (7649212us cleanup; 6553499us migration)
[42288.506852] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13630179us cleanup; 15867413us migration)
[42328.103441] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (11020182us cleanup; 27125867us migration) 
[42343.810177] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (4419812us cleanup; 10114788us migration)
[42368.034851] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (14109427us cleanup; 8840223us migration)
[42393.592994] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (868016us cleanup; 23405184us migration) 
[42415.625234] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13520119us cleanup; 7255731us migration)
[42444.038636] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (5888231us cleanup; 21214893us migration)
[42468.922805] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (4997215us cleanup; 18612401us migration) 
[42497.644166] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13867819us cleanup; 13524687us migration)
[42528.367876] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13486579us cleanup; 15899547us migration)
[42554.017420] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (869748us cleanup; 23501203us migration) 
[42575.967429] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13883500us cleanup; 6817868us migration)
[42603.568864] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2094245us cleanup; 24194662us migration)
[42626.876337] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (5531738us cleanup; 16527326us migration) 
[42656.753187] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13612524us cleanup; 14922749us migration)
[42686.525987] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (7705757us cleanup; 20747387us migration)
[42706.920810] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (2640043us cleanup; 16515041us migration) 
[42733.111020] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13535127us cleanup; 11361589us migration)
[42766.827168] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (4385114us cleanup; 27941385us migration)
[42787.001970] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (2846536us cleanup; 16107091us migration) 
[42816.544688] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 47286 out of 131070 pages. (13751919us cleanup; 14448910us migration)
[42844.650085] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (2981743us cleanup; 23804566us migration)
[42865.975253] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (4867983us cleanup; 15221596us migration) 
[42894.648496] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 23655 out of 131070 pages. (10990282us cleanup; 16347793us migration)
[42924.423277] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 39554 out of 131070 pages. (3310404us cleanup; 25114140us migration)
[42943.332695] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (4693149us cleanup; 13003696us migration)
[42974.654076] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (2651454us cleanup; 27317690us migration) 
[42999.379748] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 0 out of 131070 pages. (3156013us cleanup; 20279665us migration)
[43010.085365] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (3389431us cleanup; 6205248us migration)
[43037.925933] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (9585428us cleanup; 16939991us migration)
[43061.705238] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (11648817us cleanup; 10857795us migration) 
[43088.187089] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13822267us cleanup; 11363967us migration)
[43124.031177] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (10563132us cleanup; 23863009us migration)
[43137.602304] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (5724573us cleanup; 6682424us migration) 
[43167.325621] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13727885us cleanup; 14652714us migration)
[43204.209030] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (12806478us cleanup; 22652727us migration)
[43227.182899] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (4141233us cleanup; 17581907us migration) 
[43246.764289] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (11973043us cleanup; 6388776us migration)
[43281.432947] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (9884193us cleanup; 23398118us migration)
[43301.038554] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (5609024us cleanup; 12789890us migration) 
[43325.938791] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (14111077us cleanup; 9513901us migration)
[43355.898383] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (9892173us cleanup; 18738398us migration)
[43374.847188] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (10045496us cleanup; 7704913us migration) 
[43403.276702] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13929533us cleanup; 13166103us migration)
[43429.353272] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (611676us cleanup; 24170402us migration)
[43452.030310] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (12362865us cleanup; 9056984us migration) 
[43477.703400] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (6303753us cleanup; 18068625us migration)
[43495.481310] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (4597958us cleanup; 11974863us migration)
[43527.999281] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (5679169us cleanup; 25484148us migration) 
[43545.926029] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 383 out of 131070 pages. (3690390us cleanup; 13021156us migration)
[43562.573971] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (8832060us cleanup; 6620889us migration)
[43598.008683] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (12171041us cleanup; 21868330us migration)
[43615.430595] kmod.PLACEMENT: NVRAM->DRAM [B]: Migrated 131071 intensive pages out of 131071. (4926639us cleanup; 11309338us migration) 
[43636.882922] kmod.PLACEMENT: DRAM<->NVRAM [B]: Switched 65535 out of 131070 pages. (13274969us cleanup; 6929348us migration)
[43636.882925] kmod.PLACEMENT: Unbound pid=11493.
== END ============================
